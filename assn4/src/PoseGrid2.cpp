#include "PoseGrid2.h"

PoseGrid2::PoseGrid2(int theta_dim, int x_dim, int y_dim) {
  this->x_dim = x_dim;
  this->y_dim = y_dim;
  this->theta_dim = theta_dim;
  ROS_INFO("pose grid init: %d, %d, %d", x_dim, y_dim, theta_dim);
  values = new double[x_dim * y_dim * theta_dim];
}

PoseGrid2::~PoseGrid2() {
  delete[] values;
}

void PoseGrid2::setResolution(double xy_res, double theta_res) {
  this->xy_res = xy_res;
  this->theta_res = theta_res;
}

void PoseGrid2::setCenter(RobotPose pose) {
  center_pose = pose;
}

// i = theta, j = x, k = y
int PoseGrid2::dataIdx(int i, int j, int k) {
  if (i < 0 || i >= theta_dim) {
    ROS_INFO("ERROR: pose grid index is outside dim: i=%d, dim=%d", i, theta_dim);
  }
  if (j < 0 || j >= x_dim) {
    ROS_INFO("ERROR: pose grid index is outside dim: j=%d, dim=%d", j, x_dim);
  }
  if (k < 0 || k >= y_dim) {
    ROS_INFO("ERROR: pose grid index is outside dim: k=%d, dim=%d", k, y_dim);
  }
  return i * (x_dim * y_dim) + j * (y_dim) + k;
}

void PoseGrid2::setCellValue(int i, int j, int k, double value) {
  values[dataIdx(i,j,k)] = value;
}

RobotPose PoseGrid2::getMaxPose() {

  int max_i = 0, max_j = 0, max_k = 0;
  double max_value = getCellValue(0, 0, 0);

  for (int i = 0; i < theta_dim; i++) {
    for (int j = 0; j < x_dim; j++) {
      for (int k = 0; k < y_dim; k++) {
        double value = getCellValue(i, j, k);
        if (value > max_value) {
          max_value = value;
          max_i = i;
          max_j = j;
          max_k = k;
        }
      }
    }
  }
  
  return getCellPose(max_i, max_j, max_k);  
}

double PoseGrid2::getMaxPoseLogScore() {
  double max_value = getCellValue(0, 0, 0);

  for (int i = 0; i < theta_dim; i++) {
    for (int j = 0; j < x_dim; j++) {
      for (int k = 0; k < y_dim; k++) {
        double value = getCellValue(i, j, k);
        if (value > max_value) {
          max_value = value;
        }
      }
    }
  }
  
  return max_value;
}

int PoseGrid2::getMaxThetaSlice() {

  int max_i = 0, max_j = 0, max_k = 0;
  double max_value = getCellValue(0, 0, 0);

  for (int i = 0; i < theta_dim; i++) {
    for (int j = 0; j < x_dim; j++) {
      for (int k = 0; k < y_dim; k++) {
        double value = getCellValue(i, j, k);
        if (value > max_value) {
          max_value = value;
          max_i = i;
          max_j = j;
          max_k = k;
        }
      }
    }
  }
  
  return max_i;
}

double PoseGrid2::getCellValue(int i, int j, int k) {
  return values[dataIdx(i,j,k)];
}

RobotPose PoseGrid2::getCellPose(int i, int j, int k) {
  double dtheta = (i - theta_dim/2) * theta_res;
  double dx = (j - x_dim/2) * xy_res;
  double dy = (k - y_dim/2) * xy_res;
  
  RobotPose pose = center_pose;
  pose.x += dx;
  pose.y += dy;
  pose.theta += dtheta;
  
  return pose;
}
