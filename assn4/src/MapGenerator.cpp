#include "MapGenerator.h"
#include <tf/transform_datatypes.h>
#include <math.h>

#include "MathUtil.h"

MapGenerator::MapGenerator() {
  // size of a cell, in meters
  map_resolution = 0.1;
  
  // fixed frame of map
  map_frame = "/odom";
  
  scan_stride = 1;
}

/* sets the offset between the robot and the laser */
void MapGenerator::setLaserOffset(RobotPose offset) {
  laser_offset = offset;
}

int MapGenerator::mapX(double x) {
  double x_offset = x - x_min;
  return (int) (x_offset / map_resolution);
}

int MapGenerator::mapY(double y) {
  double y_offset = y - y_min;
  return (int) (y_offset / map_resolution);
}

int MapGenerator::dataIdx(int x, int y) {
  return y * map_size_x + x;
}


/*
Header header
  uint32 seq
  time stamp
  string frame_id
nav_msgs/MapMetaData info
  time map_load_time
  float32 resolution
  uint32 width
  uint32 height
  geometry_msgs/Pose origin
    geometry_msgs/Point position
      float64 x
      float64 y
      float64 z
    geometry_msgs/Quaternion orientation
      float64 x
      float64 y
      float64 z
      float64 w
int8[] data
*/  
void MapGenerator::generateMap(PoseGraph *graph, nav_msgs::FancyMap *map) {
  // for convenience
  this->graph = graph;
  this->map = map;
  
  // first compute the map bounds
  computeMapBounds();
  
  // setup map
  map->info.resolution = map_resolution;
  map->info.width = map_size_x;
  map->info.height = map_size_y;
  map->info.origin.position.x = x_min;
  map->info.origin.position.y = y_min;
  map->info.origin.position.z = -0.1;
  tf::quaternionTFToMsg(tf::createQuaternionFromYaw(0), map->info.origin.orientation);
  map->channels.clear();
  
  // default color abgr
  map->default_color = 0x00000000;
  nav_msgs::FancyMapChannel chan;
  chan.name = "rgba";
  map->channels.push_back(chan);
  
  // put in initial data
  log_odds = new double[map_size_x * map_size_y];
  for (int i = 0; i < map_size_x * map_size_y; i++) {
    log_odds[i] = 0;
  }
  
  // process the graph
  for (size_t i = 0; i < graph->getNumNodes(); i++) {
    GraphNode *node = graph->getNode(i);
    processNode(node);
  }
  
  // translate odds
  for (size_t i = 0; i < map_size_x * map_size_y; i++) {
    int value = (int) (255 * sigmoid(log_odds[i]));
    map->channels[0].data.push_back(makeColor(255-value, 255-value, 255-value, 255));
    //map->channels[0].data.push_back(makeColor(value,0,255-value,255));
  }
  
  // clean up
  delete[] log_odds;
  
  // update header information
  map->header.stamp = ros::Time::now();
  map->header.frame_id = map_frame;
}

/* Updates the odds at map grid (x,y), given that the scan starts at (start_x, start_y) 
 * and has a distance of z_dist. */
void MapGenerator::updateOdds(int x, int y, double start_x, double start_y, double z_dist) {
  // compute m_dist
  double cell_x = x * map_resolution + x_min;
  double cell_y = y * map_resolution + y_min;
  double dx = cell_x - start_x;
  double dy = cell_y - start_y;
  double m_dist = sqrt(dx * dx + dy * dy);
  
  // do something with z and m
  double a_hit = 0.006;
  double a_unexp = 0.2;
  double a_rand = 0.3;
  double z_max_ratio = 1.0;
  double sigma = 0.01 + 0.001 * m_dist;
  double lambda = 0.05 + 0.05 * m_dist;
  
  double p_hit = getGaussianDensity(z_dist, m_dist, sigma);
  double p_unexp = lambda * exp(-lambda * z_dist);
  double p_rand = 1.0 / range_max;
  
  double p1 = a_rand * p_rand;
  if (z_dist < range_max) {
    p1 += a_hit * p_hit;
  }
  double p2 = a_rand * p_rand + a_unexp * p_unexp;
  if (z_dist < m_dist) {
    p1 += a_unexp * p_unexp;
  }
  
  double lo = log(p1) - log(p2);
  if (z_dist == range_max) {
    //lo += log(z_max_ratio);
  }
  
  int idx = dataIdx(x,y);
  if (idx >= 0 && idx < map->info.width * map->info.height) {
   log_odds[idx] += lo; 
  }

}

void MapGenerator::processNode(GraphNode *node) {
  // extract some vars from the node
  RobotPose pose = node->pose;
  double angle_min = node->scan.angle_min;
  double angle_inc = node->scan.angle_increment;
  double range_min = node->scan.range_min;
  range_max = node->scan.range_max;
  
  // only works for non-rotational laser offsets
  double laser_dx = laser_offset.x * cos(pose.theta) - laser_offset.y * sin(pose.theta);
  double laser_dy = laser_offset.x * sin(pose.theta) + laser_offset.y * cos(pose.theta);
  
  // position of the laser
  double start_x = pose.x + laser_dx;
  double start_y = pose.y + laser_dy;
  
  for (size_t i = 0; i < node->scan.ranges.size(); i += scan_stride) {
    double range = node->scan.ranges[i];
    
    // TODO: does range_max imply free space?
    if (range < range_min || range > range_max) {
      continue;
    }
    
    // process the scan
    double angle = pose.theta + angle_min + i * angle_inc;
    processScanPoint(start_x, start_y, angle, range);
  }
}



void MapGenerator::processScanPoint(double start_x, double start_y, double angle, double distance) {
  // anti-aliased ray tracing, taken from:
  // http://en.wikipedia.org/wiki/Xiaolin_Wu%27s_line_algorithm
  double x0 = (start_x - x_min) / map_resolution;
  double y0 = (start_y - y_min) / map_resolution;
  
  // account for robot itself
  for (int i = -2; i <= 2; i++) {
    for (int j = -2; j <= 2; j++) {
      updateOdds(x0 + i, y0 + j, start_x, start_y, distance);
    }
  }
  
  double end_x = start_x + range_max * cos(angle);
  double end_y = start_y + range_max * sin(angle);
  double x1 = (end_x - x_min) / map_resolution;
  double y1 = (end_y - y_min) / map_resolution;
  
  double dx = x1 - x0;
  double dy = y1 - y0;
  bool steep = false;
  if (fabs(dx) < fabs(dy)) {
    swap(&x0, &y0);
    swap(&x1, &y1);
    swap(&dx, &dy);
    steep = true;
  }
  if (x1 < x0) {
    swap(&x0, &x1);
    swap(&y0, &y1);
  }
  double gradient = dy / dx;
  
  double xend = round(x0);
  double yend = y0 + gradient * (xend - x0);
  double xgap = rfpart(x0 + 0.5);
  int xpxl1 = (int) xend;
  int ypxl1 = (int) yend;
  
  if (steep) {
    updateOdds(ypxl1, xpxl1, start_x, start_y, distance);
    updateOdds(ypxl1+1, xpxl1, start_x, start_y, distance);
  } else {
    updateOdds(xpxl1, ypxl1, start_x, start_y, distance);
    updateOdds(xpxl1, ypxl1+1, start_x, start_y, distance);
  }
  double intery = yend + gradient;
  
  xend = round(x1);
  yend = y1 + gradient * (xend - x1);
  xgap = fpart(x1 + 0.5);
  int xpxl2 = (int) xend;
  int ypxl2 = (int) yend;
  // plot xpxl2, ypxl2, rfpart(yend) * xgap
  // plot xpxl2, ypxl2 + 1, fpart(yend) * xgap
  
  // main loop
  for (int x = xpxl1 + 1; x <= xpxl2 - 1; x++) {
    // plot x, 
    if (steep) { 
      updateOdds((int) intery, x, start_x, start_y, distance);
      updateOdds((int) (intery+1), x, start_x, start_y, distance);
    } else {
      updateOdds(x, (int) intery, start_x, start_y, distance);
      updateOdds(x, (int) (intery+1), start_x, start_y, distance);
    }
    intery += gradient;
  }
}

/* computes a rectangle bound for the map */
void MapGenerator::computeMapBounds() {
  // initialize (not really necessary)  
  x_min = y_min = x_max = y_max = 0;

  for (size_t i = 0; i < graph->getNumNodes(); i++) {
    GraphNode *node = graph->getNode(i);
    
    // extract some data
    RobotPose pose = node->pose;
    double angle_min = node->scan.angle_min;
    double angle_inc = node->scan.angle_increment;
    double range_min = node->scan.range_min;
    double range_max = node->scan.range_max;   
    
    // only works for non-rotational laser offsets
    double laser_dx = laser_offset.x * cos(pose.theta) - laser_offset.y * sin(pose.theta);
    double laser_dy = laser_offset.x * sin(pose.theta) + laser_offset.y * cos(pose.theta);
    
    // position of the laser
    double start_x = pose.x + laser_dx;
    double start_y = pose.y + laser_dy;
    
    if (i == 0) {
      x_min = x_max = start_x;
      y_min = y_max = start_y;
    } else {
      x_min = fmin(x_min, start_x);
      x_max = fmax(x_max, start_x);
      y_min = fmin(y_min, start_y);
      y_max = fmax(y_max, start_y);
    }
    
    for (size_t j = 0; j < node->scan.ranges.size(); j++) {
      double range = node->scan.ranges[j];
 
      // TODO: does range_max imply free space?
      if (range < range_min || range > range_max) {
        continue;
      }
      
      double angle = pose.theta + angle_min + j * angle_inc;
      double end_x = start_x + range_max * cos(angle);
      double end_y = start_y + range_max * sin(angle);
      x_min = fmin(x_min, end_x);
      x_max = fmax(x_max, end_x);
      y_min = fmin(y_min, end_y);
      y_max = fmax(y_max, end_y);
    }
  }
  
  // compute the size of the map
  map_size_x = (int) ((x_max - x_min) / map_resolution + 1);
  map_size_y = (int) ((y_max - y_min) / map_resolution + 1);
}

