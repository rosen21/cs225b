#include "SensorModel2.h"
#include "MathUtil.h"
#include "Point.h"
#include <queue>
#include <float.h>
#include <sensor_msgs/PointCloud.h>

using namespace std;

/* creates a new sensor model for the reference map and test scan. these values are fixed for the lifetime
 * of the model. */
SensorModel2::SensorModel2(nav_msgs::OccupancyGrid* reference_map, sensor_msgs::LaserScan* test_scan) {
  this->reference_map = reference_map;
  this->test_scan = test_scan;
  num_cells = reference_map->data.size();
  
  likelihood_map = new double[num_cells];
  
  // sensor cache data
  scan_offset_x = new int[test_scan->ranges.size()];
  scan_offset_y = new int[test_scan->ranges.size()];
  cache_empty = true;
  cache_theta = 0;
  
  // parameters
  likelihood_map_std_dev = 0.03; // in meters
  likelihood_map_max_dist = 0.4; // in meters
  
  // precompute likelihood map
  buildLikelihoodMap();
}

/* clean up dynamic memory */
SensorModel2::~SensorModel2() {
  delete[] likelihood_map;
  delete[] scan_offset_x;
  delete[] scan_offset_y;
}

double SensorModel2::getLogScore(int x_idx, int y_idx, double theta) {
  //ROS_INFO("[getLogScore] called!");
  
  if (cache_empty || theta != cache_theta) {
    updateCache(theta);
  }
  double log_total_score = 0;
  double bad_score = -12;
  int scan_count = 0;
  
  for (size_t i = 0; i < test_scan->ranges.size(); i++) {
    double range = test_scan->ranges[i];
    if (range >= test_scan->range_max) {
      continue;
    }
    scan_count++;
    int x_offset = scan_offset_x[i];
    int y_offset = scan_offset_y[i];
    
    int x_grid = x_idx + x_offset;
    int y_grid = y_idx + y_offset;

    if (x_grid < 0 || y_grid < 0 || x_grid >= reference_map->info.width || y_grid >= reference_map->info.height) {
      log_total_score += bad_score;
      continue;
    }
    //ROS_INFO("x_idx=%d, y_idx=%d; x_offset=%d, y_offset=%d", x_idx, y_idx, x_offset, y_offset);
    //ROS_INFO("x_grid=%d (width=%d), y_grid=%d (height=%d)",
    //  x_grid, reference_map->info.width, y_grid, reference_map->info.height);
        
    double score = getVal(likelihood_map, x_grid, y_grid);

    double log_score = log(score);
    
    log_total_score += log_score;
  }
  
  return log_total_score / scan_count;
}

/* returns the (unnormalized) likelihood at a given map index */
double SensorModel2::getLikelihood(int idx) {
  return likelihood_map[idx];
}

/* computes and caches scan offsets for poses with orientation theta. */
void SensorModel2::updateCache(double theta) {
  double angle_min = test_scan->angle_min;
  double angle_inc = test_scan->angle_increment;
  double range_min = test_scan->range_min;
  double range_max = test_scan->range_max;
  
  for (size_t i = 0; i < test_scan->ranges.size(); i++) {
    double range = test_scan->ranges[i];

    // scan offset relative to fixed node's origin
    double scan_x = range * cos(angle_min + angle_inc * i + theta);
    double scan_y = range * sin(angle_min + angle_inc * i + theta);
    
    // turn into grid cell indices
    int idx_x = (int) ((scan_x) / reference_map->info.resolution); 
    //(int) ((scan_x - 0.5*reference_map->info.resolution) / reference_map->info.resolution);
    int idx_y = (int) ((scan_y) / reference_map->info.resolution);
    
    //ROS_INFO("updateCache: i=%d, idx_x=%d, idx_y=%d", i, idx_x, idx_y);
    //ROS_INFO("    range=%f, scan_x=%f, scan_y=%f", range, scan_x, scan_y);
    
    // cache the offset
    scan_offset_x[i] = idx_x;
    scan_offset_y[i] = idx_y;
  }

  // set cache flags
  cache_empty = false;
  cache_theta = theta;
}

/* builds the likelihood field from the reference map */
void SensorModel2::buildLikelihoodMap() {
  // find occupied cells in reference map
  bool *init_set_map = new bool[num_cells];
  getInitSet(init_set_map);
  
  // initialize map
  for (int i = 0; i < num_cells; i++) {
    likelihood_map[i] = -1;
  }
  
  // run dijkstra's on the seeded map
  double edge_cost = 1.0; // if 1.0, updateMapDijkstra does manhattan distance
  updateMapDijkstra(init_set_map, edge_cost);
  
  // convert grid cell distances into probabilities
  computeDecay();
  
  // clean up
  delete[] init_set_map;
}

void SensorModel2::getInitSet(bool* init_set_map) {
  ROS_DEBUG("[getInitSet]\tentering");
  for (int i=0; i < num_cells; i++) {
    signed char map_cost = reference_map->data[i];
    if (map_cost <= 0) {
      init_set_map[i] = false; // unkown, or known and free; don't consider part of initial set
    } else {
      init_set_map[i] = true;
    }
  }
}

/* runs dijkstra's algorithm */
void SensorModel2::updateMapDijkstra(bool* init_set_map, double edge_cost) {
  bool *visited = new bool[num_cells];
  for (int i=0; i < num_cells; i++) {
    visited[i] = false;
  }
  
  // setup queue
  priority_queue<pair<Point,double>, vector<pair<Point,double> >, PairComparator > pq;
  double init_cost = 0.0;
  for (int i=0; i < num_cells; i++) {
    if (init_set_map[i]) {
      pair<Point,double> init_pair = make_pair(Point(i,reference_map->info.width,reference_map->info.height),init_cost);
      pq.push(init_pair);
    }
  }

  pair<Point,double> curr_pq_elem;
  Point curr_pt;
  double curr_cost;
  Point nbrs[4];
  int num_nbrs;
  
  // main dijkstra's update loop
  while (!pq.empty()) {
    // pop queue top: cell and cost
    curr_pq_elem = pq.top();
    pq.pop();
    curr_pt = curr_pq_elem.first;
    curr_cost = curr_pq_elem.second;
    
    // don't update value of points which have been visited
    if (getVal(visited,curr_pt.x,curr_pt.y)) {
      continue; 
    }
    
    // sanity check
    if (curr_cost < 0) {
      ROS_WARN("found cost less that 0: %f",curr_cost);
    }

    // mark as visited, update likelihood map
    setVal(visited,curr_pt.x,curr_pt.y,true);
    setVal(likelihood_map,curr_pt.x,curr_pt.y, curr_cost);
    
    // stop enqueuing edges when we get sufficiently far away
    if (curr_cost * reference_map->info.resolution >= likelihood_map_max_dist) {
      continue;
    }

    // enqueue neighbors
    num_nbrs = curr_pt.get4Nbrs(nbrs,reference_map->info.width,reference_map->info.height);
    for (int i = 0; i < num_nbrs; i++) {
      if (!getVal(visited,nbrs[i].x,nbrs[i].y)) {
	      double new_cost = curr_cost + edge_cost;
	      pq.push(make_pair(nbrs[i],new_cost));
      }
    }
  }
  
  // clean up
  delete[] visited;
}

/* translates from grid cell distances to likelihood */
void SensorModel2::computeDecay() {
  for (int i = 0; i < num_cells; i++) {
    double obstacle_dist = likelihood_map[i];
    double world_dist;
    
    // didn't compute via dijkstra's, treat as max allowed distance
    if (obstacle_dist < 0) {
      if (obstacle_dist != -1) {
        ROS_WARN("[computeDecay]\tfound unexpected obstacle_dist: %f", obstacle_dist);
      }
      world_dist = likelihood_map_max_dist;
    } else {
      world_dist = obstacle_dist * reference_map->info.resolution;
    }
    
    // compute as a zero-centered gaussian
    likelihood_map[i] = getGaussianDensity(world_dist, 0.0, likelihood_map_std_dev);
  }
}
