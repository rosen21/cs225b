#ifndef _MATH_UTIL_H
#define _MATH_UTIL_H

#include <float.h>
#include <math.h>
#include <stdlib.h>
#include "Eigen/Core"
#include "RobotPose.h"
#define _USE_MATH_DEFINES

#define RED 0
#define GREEN 1
#define BLUE 2
#define ALPHA 3

double absf(double val) {
  return val < 0 ? -val : val;
}

double getUnitRand() {
  return ((double)rand() / (double)RAND_MAX);
}

double getUniformRand(double min, double max) {
  double range = max - min;
  return getUnitRand() * range + min;
}

double getGaussianRand(double mean, double variance) {
  double u1 = getUnitRand();
  double u2 = getUnitRand();
  double gaussian_sample = sqrt(-2*log(u1)) * cos(2*M_PI*u2);
  return gaussian_sample*sqrt(variance) + mean;
  //return partial_res = sqrt(-2*log(u1)) * sin(2*M_PI*u2);
}

double getGaussianDensity(RobotPose pose, Eigen::Matrix3d cov) {
  Eigen::Matrix3d inv = cov.inverse();
  Eigen::Vector3d vec;
  vec[0] = pose.x;
  vec[1] = pose.y;
  vec[2] = pose.theta;
  Eigen::Vector3d tmp = inv * vec;
  double exponent = -0.5 * tmp.dot(vec);
  
  double value = pow(2 * M_PI, -1.5) * pow(cov.determinant(), -0.5) * exp(exponent);  
  return value;
}

double getGaussianDensity(double val, double mu, double std_dev) {
    double var = pow(std_dev, 2);
    return exp(-pow(val - mu,2)/(2 * var)) / (sqrt(2*M_PI*var)); 
}

double getGaussianLogDensity(double val, double mu, double std_dev) {
  //ROS_INFO("[getGaussianDensity]\tval=%f, mu=%f, std_dev=%f",val,mu,std_dev);
  double var = pow(std_dev, 2);
  return -pow(val - mu,2)/(2 * var) - log(sqrt(2*M_PI*var));
}

double sigmoid(double x) {
  return 1.0 / (1 + exp(-x));
}

double logAdd(double log_p1, double log_p2) {
  double max_log = max(log_p1, log_p2);
  double p1_hat = exp(log_p1 - max_log);
  double p2_hat = exp(log_p2 - max_log);
  double log_p3_hat = log(p1_hat + p2_hat);
  return log_p3_hat + max_log;
}

double fmax(double a, double b) {
  return a > b ? a : b;
}

double fmin(double a, double b) {
  return a < b ? a : b;
}

double fabs(double a) {
  return a < 0 ? -a : a;
}

double fpart(double a) {
  return a - floor(a);
}

double rfpart(double a) {
  return 1 - fpart(a);
}

/* returns the difference between two headings normalized to the range [0,PI] */
double headingChange(double theta1, double theta2) {
  // get some 0 <= d <= 2*M_PI
  double d = theta1 - theta2;
  if (d < 0) {
    d = -d;
  }
  
  // smaller change if we rotate in the opposite direction
  if (d > M_PI) {
    return 2 * M_PI - d;
  }
  return d;
}

/* binary searches the array for value, assuming arr[lb] < value && arr[ub] >= value.
 * Returns the upper bound index. */
int binarySearch(double *arr, double value, int lb, int ub) {
  // base case
  if (ub - lb <= 1) {
    return ub;
  }
  
  // middle index
  int mid = (lb + ub) / 2;
  
  // mid is too small
  if (arr[mid] < value) {
    return binarySearch(arr, value, mid, ub);
  }
  
  // mid is at least big enough
  return binarySearch(arr, value, lb, mid);
}

void swap(int *a, int *b) {
  int tmp = *a;
  *a = *b;
  *b = tmp;
}

void swap(double *a, double *b) {
  double tmp = *a;
  *a = *b;
  *b = tmp;
}

unsigned int makeColor(int r, int g, int b, int a) {
  return
    ((a & 0xff) << 24) |
    ((b & 0xff) << 16) |
    ((g & 0xff) << 8) |
    ((r & 0xff) << 0);
}

int overlayColors(unsigned int top_color, unsigned int bottom_color) {
  
  unsigned char *top = (unsigned char *) &top_color;
  unsigned char *bot = (unsigned char *) &bottom_color;

  
  double top_scale = (1.0 * top[ALPHA]) / 255;
  double bot_scale = (1-top_scale) * (1.0 * bot[ALPHA]) / 255;
  
  int r = (int)(top[RED] * top_scale + bot[RED] * bot_scale);
  int g = (int)(top[GREEN] * top_scale + bot[GREEN] * bot_scale);
  int b = (int)(top[BLUE] * top_scale + bot[BLUE] * bot_scale);
  int a = (int)(top[ALPHA] * top_scale + bot[ALPHA] * bot_scale);
  
  return makeColor(r,g,b,a);
}

#endif

