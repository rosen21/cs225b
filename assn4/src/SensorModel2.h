#ifndef SENSOR_MODEL2_H
#define SENSOR_MODEL2_H

#include <nav_msgs/OccupancyGrid.h>
#include <sensor_msgs/LaserScan.h>
#include <ros/ros.h>
#include "Point.h"

using namespace std;

class PairComparator {
public:
  PairComparator() {}
  bool operator() (const pair<Point,double> &lhs, const pair<Point,double> &rhs) const {
    return (lhs.second > rhs.second);
  }
};

class SensorModel2 {
  public:
    SensorModel2(nav_msgs::OccupancyGrid* reference_map, sensor_msgs::LaserScan* test_scan);
    double getLikelihood(int idx);
    ~SensorModel2();
    void visualizeLikelihoodMap();
    double getLogScore(int i, int j, double theta);
  private:
    nav_msgs::OccupancyGrid* reference_map;
    sensor_msgs::LaserScan* test_scan;
    double* likelihood_map;
    int* scan_offset_x;
    int* scan_offset_y;
    double cache_theta;
    bool cache_empty;
    int num_cells;
    double likelihood_map_max_dist;
    double likelihood_map_std_dev;

    ros::Publisher* map_pub;
    template <class T>
    T getVal(T* cost_map, int i,int j) {return cost_map[reference_map->info.width*j + i];};
  
    template <class T>
    void setVal(T* cost_map, int i,int j, T val) {cost_map[reference_map->info.width*j + i] = val;}

    void updateCache(double theta);
    void buildLikelihoodMap();
    void getInitSet(bool* init_set_map);
    void computeDecay();
    void updateMapDijkstra(bool* init_set_map, double edge_cost);
};

#endif
