#ifndef _ROBOT_POSE_H
#define _ROBOT_POSE_H

#include <ros/ros.h>
#include <math.h>
#include <Eigen/Geometry>
#include <Eigen/Dense>

using namespace std;
using namespace Eigen;

/* Pose class representing a single (x,y,theta) triple. */
class RobotPose {
  public:
    double x;
    double y;
    double theta;

    RobotPose() {};
    RobotPose(double _x, double _y, double _theta) {x=_x; y=_y; theta=_theta;}

    /* Eigen::Matrix2d rotMat() {return Eigen::Rotation2D<double>(theta).toRotationMatrix();} */
    /* Eigen::Vector2d trans() { */
    /*   Eigen::Vector2d t; */
    /*   t[0] = x; */
    /*   t[1] = y; */
    /*   return t; */
    /* } */

    /* RobotPose inverse() const { */
    /*   RobotPose new_pose; */
    /*   new_pose.x = -1.0*x; */
    /*   new_pose.y = -1.0*y; */
    /*   new_pose.theta = fmod(-1.0*theta, M_PI); */
    /*   return new_pose; */
    /* } */

    /* RobotPose oplus(RobotPose up) { */
    /*   RobotPose new_pose; */
    /*   Eigen::Vector2d t_new = trans() + (rotMat() * up.trans()); */
    /*   new_pose.x = t_new[0]; */
    /*   new_pose.y = t_new[1]; */
    /*   new_pose.theta = fmod(theta + up.theta, 2*M_PI); */
    /*   return new_pose; */
    /* } */

    /* RobotPose ominus(RobotPose other) { */
    /*   RobotPose new_pose; */
    /*   Eigen::Vector2d t_new = rotMat().inverse() * (trans() - other.trans()); */
    /*   //Eigen::Vector2d t_new = rotMat().inverse() * (other.trans() - trans()); */
    /*   new_pose.x = t_new[0]; */
    /*   new_pose.y = t_new[1]; */
    /*   new_pose.theta = fmod(theta - other.theta, 2*M_PI); */
    /*   // alternately: */
    /*   RobotPose check = inverse().oplus(other); */
    /*   if (check.x != new_pose.x || check.y != new_pose.y || check.theta != new_pose.theta) { */
    /* 	ROS_WARN("[RobotPose::ominus]\treturns different values for check and new_pose methods"); */
    /*   } */
    /*   return new_pose; */
    /* } */

    /* void setToOrigin() {x=0; y=0; theta=0;} */

    void setPose(RobotPose pose) {x = pose.x; y = pose.y; theta = pose.theta;}

    RobotPose operator-(RobotPose other_pose) {
      RobotPose new_pose;
      new_pose.x = x - other_pose.x;
      new_pose.y = y - other_pose.y;
      new_pose.theta = theta - other_pose.theta;
      if (new_pose.theta < -M_PI) {
        new_pose.theta += 2 * M_PI;
      }
      if (new_pose.theta > M_PI) {
        new_pose.theta -= 2 * M_PI;
      }
      return new_pose;
    };

    RobotPose operator+(RobotPose other_pose) {
      RobotPose new_pose;
      new_pose.x = x + other_pose.x;
      new_pose.y = y + other_pose.y;
      new_pose.theta = theta + other_pose.theta;
      return new_pose;
    };

    RobotPose operator*(double mult) {
      RobotPose new_pose;
      new_pose.x = x * mult;
      new_pose.y = y * mult;
      new_pose.theta = theta * mult;
      return new_pose;
    };

    Vector3d toEigenVector(){
      return Vector3d(x, y, theta);
    } 

    double dist(RobotPose other_pose){
      return pow( pow((other_pose.x -x ), 2) + pow((other_pose.y -y), 2) , 0.5);
    }

    // sets res to an outer product (scatter) matrix in row major order
    void scatter(double* res) {
      res[0] = x * x;
      res[1] = x * y;
      res[2] = x * theta;
      
      res[3] = y * x;
      res[4] = y * y;
      res[5] = y * theta;
      
      res[6] = theta * x;
      res[7] = theta * y;
      res[8] = theta * theta;
    };
};

#endif
