#include "SensorModel.h"
#include "MathUtil.h"

#include <geometry_msgs/Point32.h>
#include <visualization_msgs/Marker.h>
#include <ros/ros.h>
#include <sensor_msgs/PointCloud.h>
#include <sensor_msgs/ChannelFloat32.h>
#include <tf/transform_datatypes.h>

#include "Point.h"
#include "VizUtil.h"
#include <ros/ros.h>

#include <queue>
#include <float.h>

using namespace std;

class PairComparator {
public:
  PairComparator() {}
  bool operator() (const pair<Point,double> &lhs, const pair<Point,double> &rhs) const {
    return (lhs.second > rhs.second);
  }
};

SensorModel::SensorModel(nav_msgs::OccupancyGrid msg) {
  MAP_SIZE_X = msg.info.width;
  MAP_SIZE_Y = msg.info.height;
  NUM_CELLS = MAP_SIZE_X * MAP_SIZE_Y;
  //ROS_INFO("[SensorModel::SensorModel]\tMAP_SIZE_X=%d, MAP_SIZE_Y=%d", MAP_SIZE_X, MAP_SIZE_Y);
  MAP_RESOLUTION = msg.info.resolution;

  LIKELIHOOD_MAP_MAX_DIST = MAP_SIZE_X + MAP_SIZE_Y;
  LIKELIHOOD_MAP_STD_DEV = 1.0;
  
  max_scan_pts = 128;
  laser_offset_x = 0.12;
  laser_offset_y = 0;
  
  //TODO - get the right values for the constants!!!!
  zhit = 0.9;
  zrandom = 0.05;
  zmax = 0.05;

  // build likelihood map
  likelihood_map = new double[NUM_CELLS];
  //ROS_DEBUG("likelihood_map addr = %p", likelihood_map);
  buildLikelihoodMap(msg, likelihood_map);
}

/*
 * gets the probability P(z|p), i.e. prob of scan given pose p
 * its assumed the pose p is in the map frame, in normal double map coordinates 
 * (i.e. not integer map grid coordinates)
 */
double SensorModel::getScanProb(RobotPose p){

    double prob = 0.0;
    int numPts = cur_scan.ranges.size();
    int stride = 1;
    if (numPts > max_scan_pts) {
      stride = numPts / max_scan_pts;
    }

    int count = 0;
    
    // loop through scan range
    for (int i = 0; i < numPts; i += stride) {
      
      double distance = cur_scan.ranges[i]; 
      double angle = cur_scan.angle_min + i * cur_scan.angle_increment;
      count++;  
      
      // get offset from current pose
      double x = distance * cos(angle + p.theta) + laser_offset_x * cos(p.theta);
      double y = distance * sin(angle + p.theta) + laser_offset_x * sin(p.theta);
      
      //get the closest point in the map frame
      int mapX = floor((x + p.x)/MAP_RESOLUTION );
      int mapY = floor((y + p.y)/MAP_RESOLUTION );
      
      //lookup the point in the likelihood map 
      int mapInd = mapY*MAP_SIZE_X + mapX;
      double likelihood;
      
        if(mapInd < NUM_CELLS && mapInd >= 0){
            likelihood = likelihood_map[mapInd]; 
        }
        else{
            //ROS_INFO("an out of bounds reading?");
            likelihood = 0.0;
        }

        double isMax = 0.0;
        if(distance >= cur_scan.range_max) isMax = 1.0;


        //get the probability of this specific laser scan
        //ROS_INFO("likelihood = %f", likelihood);
        double point_prob = zhit*likelihood + zrandom/cur_scan.range_max + zmax*isMax;

        //add it into prob
        prob = prob + log(point_prob);

        //ROS_INFO("point_prob: %f \t prob: %f", point_prob, prob);
    }

    return prob / count * 4;
}

/*
 * Method to calculate the probability of all points in the grid corresponding to this model's occupancy map
 */
void SensorModel::calcGridProbs(PoseGrid* grid, double xoff, double yoff, int grid_sample_rate, int scan_sample_rate){

  //ROS_INFO("[SensorModel::calcGridProbs]\tentering with xoff=%f, yoff=%f and grid_sample_rate=%d, scan_sample_rate=%d",
  //	   xoff,yoff,grid_sample_rate,scan_sample_rate);

    //calculate the angle step
    double theta_step = 2 * M_PI / grid->theta_dim;

    //init necessary structures
    int numScanPts = cur_scan.ranges.size();
    double* scan_zero_xoff = new double[numScanPts];
    double* scan_zero_yoff = new double[numScanPts];

    //pre-calculate stuff
    double uniform_logp = log(zrandom/cur_scan.range_max);
    double uniform_p = zrandom/cur_scan.range_max;

    //loop through grids
    for(int theta_index = 0; theta_index < grid->theta_dim; theta_index += grid_sample_rate){
        double theta = theta_index * theta_step;
        int theta_arr_offset = theta_index * grid->x_dim * grid->y_dim;

        //calculate where our scan is for a robot position of 0,0 on the map grid
        //remember, the scan is in laser frame scale, not map grid scale
        for(int s = 0; s < numScanPts; s += scan_sample_rate){
            double distance = cur_scan.ranges[s]; 
            double angle = cur_scan.angle_min + s * cur_scan.angle_increment;
            scan_zero_xoff[s] = distance * cos(angle + theta) / MAP_RESOLUTION;
            scan_zero_yoff[s] = distance * sin(angle + theta) / MAP_RESOLUTION;
        }

        //loop through x and y coordinates in this grid
        for(int y = 0; y < grid->y_dim; y += grid_sample_rate){
            int y_offset = y * grid->x_dim;

            for(int x = 0; x < grid->x_dim; x += grid_sample_rate){
            
                //get grids array index for this entry
                int grid_arr_index = theta_arr_offset + y_offset + x;

                //loop through the scans now!
                double logp = 0.0;
                for(int i = 0; i < numScanPts; i += scan_sample_rate){
                    //compute y and x map positions for each point in the scan
                    int mapX = floor(scan_zero_xoff[i] + xoff) + x;
                    int mapY = floor(scan_zero_yoff[i] + yoff) + y;
                    if(mapX < 0 || mapX >= MAP_SIZE_X || mapY < 0 || mapY >= MAP_SIZE_Y){
                        //if this point is out of bounds, increment it by the baseline uniform probability
                        logp = logp + uniform_logp;
                        ROS_WARN("point (%d %d) outside of map coordinates!", mapX, mapY);
                        continue;
                    }

                    //compute index and increment prob with it!
                    int occ_index = mapY*MAP_SIZE_X + mapX;
                    logp = logp + log(likelihood_map[occ_index]*zhit + uniform_p);
                }

                //now, update the grid with the new probability!
                //well, its sort of a probability.....
                //grid->grid[grid_arr_index] = (logp / numScanPts * 4);
                grid->grid[grid_arr_index] = logp;

            }
        }
    }

    //free stuff
    delete[] scan_zero_xoff;
    delete[] scan_zero_yoff;
}

//updates the current laser scan
void SensorModel::updateScan(const sensor_msgs::LaserScan::ConstPtr& msg){

  //ROS_INFO("[SensorModel::updateScan] scan is updating");
    cur_scan = *msg;  
}

void SensorModel::updateScan(const sensor_msgs::LaserScan msg){

  //ROS_INFO("[SensorModel::updateScan] scan is updating");
    cur_scan = msg;  
}

void SensorModel::visualizeScanLikelihood(ros::Publisher marker_pub, int map_size_x, int map_size_y, double map_resolution){

  //ROS_INFO("[SensorModel::visualizeScanLikelihood]\tentering");

    double maxP = -10000000;
    int mx = 0, my = 0;
    double* scanProbs = new double[NUM_CELLS]; 

    //loop through, create a new map of p(z|x) for all points X and the orientation of facing up
    for(int i = 0; i < MAP_SIZE_X; i ++){
        for(int j = 0; j < MAP_SIZE_Y; j ++){

            //ROS_INFO("i = %d \t j=%d", i, j);
            RobotPose p;
            p.x = i * MAP_RESOLUTION;
            p.y = j * MAP_RESOLUTION;
            p.theta = M_PI/4;

            double scanProb = getScanProb(p);
            //ROS_INFO("hello %f", scanProb);

            if(scanProb > maxP){
                maxP = scanProb;
                mx = i;
                my = j;    
            }

            scanProbs[j*MAP_SIZE_X + i] = scanProb;

        }
        ROS_INFO("ending processing for line %d", i);
    }

    //ROS_INFO("[SensorModel::visualizeScanLikelihood]\tfinished computing, visualizing");
    visualizeHeatMap(marker_pub, scanProbs, "scan_likelihoods",map_size_x, map_size_y, map_resolution);

    //ROS_INFO("max prob %f at coordinate (%d, %d)", maxP, mx, my);
}

void SensorModel::visualizeLikelihoodMap(ros::Publisher map_pub) {
  //ROS_INFO("[SensorModel::visualizeLikelihoodMap]\tconstructing point cloud");
  
  sensor_msgs::PointCloud cloud;
  cloud.header.stamp = ros::Time::now();
  cloud.header.frame_id = "/map";
  
  geometry_msgs::Point32 point;
  sensor_msgs::ChannelFloat32 channel;
  channel.name = "intensity";
  cloud.channels.push_back(channel);
  
  for (int x = 0; x < MAP_SIZE_X; x++) {
    for (int y = 0; y < MAP_SIZE_Y; y++) {
      point.x = x * MAP_RESOLUTION;
      point.y = y * MAP_RESOLUTION;
      point.z = -0.2;
      cloud.points.push_back(point);
      int idx = y * MAP_SIZE_X + x;
      cloud.channels[0].values.push_back(pow(likelihood_map[idx], 0.03));
    }
  }
  map_pub.publish(cloud);
}

void SensorModel::buildLikelihoodMap(nav_msgs::OccupancyGrid map_msg, double* likelihood_map) {
  //ROS_INFO("[SensorModel::buildLikelihoodMap]\tentering");
  bool init_set_map[NUM_CELLS];
  double init_set_vis_map[NUM_CELLS];
  getInitSet(init_set_map,map_msg);
  for (int i = 0; i < NUM_CELLS; i++) {init_set_vis_map[i] = init_set_map[i] ? 1 : 0;}

  double obstacle_dist_map[NUM_CELLS];
  for (int i = 0; i < NUM_CELLS; i++) {obstacle_dist_map[i] = -1;}
  double EDGE_COST = 1.0; // if 1.0, updateMapDijkstra does manhattan distance
  updateMapDijkstra(obstacle_dist_map, init_set_map, EDGE_COST, LIKELIHOOD_MAP_MAX_DIST);
  computeDecay(obstacle_dist_map, LIKELIHOOD_MAP_STD_DEV, likelihood_map);
}

void SensorModel::getInitSet(bool* init_set_map, nav_msgs::OccupancyGrid map_msg) {
  ROS_DEBUG("[getInitSet]\tentering");
  for (int i=0; i < NUM_CELLS; i++) {
    signed char map_cost = map_msg.data[i];
    if (map_cost <= 0) {
      init_set_map[i] = false; // unkown, or known and free; don't consider part of initial set
    } else {
      init_set_map[i] = true;
    }
  }
}

void SensorModel::updateMapDijkstra(double* map, bool* init_set_map, double edge_cost, int max_dist) {
  //ROS_INFO("[SensorModel::updateMapDijkstra]\tentering, edge_cost=%f, max_dist=%d", edge_cost, max_dist);
  bool visited[NUM_CELLS];
  for (int i=0; i < NUM_CELLS; i++) {visited[i] = false;}
  
  priority_queue<pair<Point,double>, vector<pair<Point,double> >, PairComparator > pq;
  
  double init_cost = 0.0;
  for (int i=0; i < NUM_CELLS; i++) {
    if (init_set_map[i]) {
      pair<Point,double> init_pair = make_pair(Point(i,MAP_SIZE_X,MAP_SIZE_Y),init_cost);
      pq.push(init_pair);
      //ROS_INFO("[updateMapDijkstra]\tpushing new pair");
    }
  }

  pair<Point,double> curr_pq_elem;
  Point curr_pt;
  double curr_cost;
  Point nbrs[4];
  int num_nbrs;
  while (!pq.empty()) {
    curr_pq_elem = pq.top();
    pq.pop();

    curr_pt = curr_pq_elem.first;
    curr_cost = curr_pq_elem.second;
    //ROS_INFO("[updateMapDijkstra]\tpopped Point(%d,%d) with cost: %f", curr_pt.x,curr_pt.y,curr_cost);
    if (getVal(visited,curr_pt.x,curr_pt.y)) {continue;} // don't update value of points which have been visited
    if (curr_cost < 0) {ROS_WARN("found cost less that 0: %f",curr_cost);}

    // set info for curr_pt
    setVal(visited,curr_pt.x,curr_pt.y,true);
    setVal(map,curr_pt.x,curr_pt.y, curr_cost);
    
    // this stops enqueuing edges when we get sufficiently far away
    if (curr_cost >= max_dist) {continue;}

    // enqueue neighbors
    num_nbrs = curr_pt.get4Nbrs(nbrs,MAP_SIZE_X,MAP_SIZE_Y);
    for (int i = 0; i < num_nbrs; i++) {
      if (!getVal(visited,nbrs[i].x,nbrs[i].y)) {
	double new_cost = curr_cost + edge_cost;
	pq.push(make_pair(nbrs[i],new_cost));
	//ROS_INFO("[updateMapDijkstra]\tpushing Point(%d,%d) with cost: %f", nbrs[i].x, nbrs[i].y,new_cost);
      }
    }
  }
}

void SensorModel::computeDecay(double* obstacle_dist_map, double std_dev, double* cost_map) {
  ROS_INFO("[SensorModel::computeDecay]\tentering");
  double obstacle_dist;
  for (int i=0; i < NUM_CELLS; i++) {
    obstacle_dist = obstacle_dist_map[i];
    if (obstacle_dist > 20) {
      obstacle_dist = 20;
    }
    if (obstacle_dist >= 0) {
      cost_map[i] = getGaussianDensity(obstacle_dist,0.0,std_dev);
    } else { // something weird happened, so just set to -1 and throw warning
      cost_map[i] = -1;
      ROS_WARN("[computeDecay]\tfound unexpected obstacle_dist: %f", obstacle_dist);
    }
  }
}
