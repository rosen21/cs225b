#include "ScanMatcher.h"
#include "MathUtil.h"
#include "tf/transform_datatypes.h"
#include <visualization_msgs/Marker.h>
#include <geometry_msgs/Point.h>
#include <tf/transform_listener.h>
#include <tf/transform_datatypes.h>
#include "SensorModel2.h"
#include "MotionModel.h"
#include <nav_msgs/FancyMap.h>

ScanMatcher::ScanMatcher() {
  // map params
  map_resolution = 0.02;
  

  grid_dim_x = 40;
  grid_dim_y = 25;
  grid_dim_theta = 30;

  grid_res_xy = map_resolution;
  grid_res_theta = 0.03;
  
  map_margin_buffer = 2.0; // meters
  motion_model_weight = 0.0;
  
  pose_grid = new PoseGrid2(grid_dim_theta, grid_dim_x, grid_dim_y);
  pose_grid->setResolution(grid_res_xy, grid_res_theta);
}

void ScanMatcher::initialize(ros::NodeHandle *nh, tf::TransformListener *listener) {
  this->listener = listener;
  marker_pub = nh->advertise<visualization_msgs::Marker>("visualization_marker", 0);
  pose_grid_pub = nh->advertise<sensor_msgs::PointCloud>("sm_pose_grid", 0);
  likelihood_field_pub = nh->advertise<sensor_msgs::PointCloud>("sm_likelihood_field", 0);
  occ_pub = nh->advertise<nav_msgs::OccupancyGrid>("sm_reference_scan", 0);
  fancy_pub = nh->advertise<nav_msgs::FancyMap>("fancy_likelihood", 0);
}


void ScanMatcher::initMap(vector<GraphNode*> reference_nodes) {
  visualization_msgs::Marker rays;
  rays.header.frame_id = "/odom";
  rays.header.stamp = ros::Time::now();
  rays.ns = "rays";
  rays.type = visualization_msgs::Marker::LINE_LIST;
  rays.scale.x = 0.01;
  rays.scale.y = 0.01;
  rays.color.b = 1.0;
  rays.color.a = 0.2;

  double x_min, y_min, x_max, y_max;
  x_min = y_min = x_max = y_max = 0.0;

  // just create transform from fixed node which is essentially a pose w.r.t odom
  tf::Transform fixed_to_odom;
  fixed_to_odom.setOrigin(tf::Vector3(fixed_node->pose.x, fixed_node->pose.y, 0));
  fixed_to_odom.setRotation(tf::createQuaternionFromYaw(fixed_node->pose.theta));
  tf::Transform odom_to_fixed = fixed_to_odom.inverse();

  tf::StampedTransform laser_to_base_link;
  string base_link_frame = "/base_link";
  string laser_frame = "/base_laser_link";
  listener->lookupTransform(base_link_frame, laser_frame, ros::Time(0), laser_to_base_link);

  for (size_t i = 0; i < reference_nodes.size(); i++) {
    GraphNode *node = reference_nodes[i];

    tf::Transform pose_to_odom; // this is really base_link (at scan recording pose) to odom
    pose_to_odom.setOrigin(tf::Vector3(node->pose.x,node->pose.y,0.0));
    pose_to_odom.setRotation(tf::createQuaternionFromYaw(node->pose.theta));
    tf::Transform laser_to_odom = pose_to_odom * laser_to_base_link;
    
    double angle_min = node->scan.angle_min;
    double angle_inc = node->scan.angle_increment;
    double range_min = node->scan.range_min;
    double range_max = node->scan.range_max;   

    for (size_t j = 0; j < node->scan.ranges.size(); j++) {
      double range = node->scan.ranges[j];
 
      // TODO: does range_max imply free space?
      if (range < range_min || range >= range_max) {
        continue;
      }

      double scan_x = range * cos(angle_min + angle_inc * j);
      double scan_y = range * sin(angle_min + angle_inc * j);

      tf::Point scan_pt_laser(scan_x,scan_y,0.0); // scan point in the frame of the laser in the pose from which it was recorded
      tf::Point scan_pt_odom = laser_to_odom * scan_pt_laser; // in the odom frame
      tf::Point scan_pt_fixed = odom_to_fixed * scan_pt_odom; // in the fixed frame

      if (j % 10 == 0) {
	geometry_msgs::Point pt;
	tf::pointTFToMsg(laser_to_odom.getOrigin(), pt);
	rays.points.push_back(pt);
	
	tf::pointTFToMsg(scan_pt_odom, pt);
	rays.points.push_back(pt);
      }
      
      if (i == 0 && j == 0) {
	      x_min = x_max = scan_pt_fixed.getX();
	      y_min = y_max = scan_pt_fixed.getY();
      } else {
	      x_min = fmin(x_min, scan_pt_fixed.getX());
	      x_max = fmax(x_max, scan_pt_fixed.getX());
	      y_min = fmin(y_min, scan_pt_fixed.getY());
	      y_max = fmax(y_max, scan_pt_fixed.getY());
      }
    }
  }
  
  x_min -= map_margin_buffer;
  x_max += map_margin_buffer;
  y_min -= map_margin_buffer;
  y_max += map_margin_buffer;
  
  // compute the size of the map
  int map_size_x = (int) ((x_max - x_min) / map_resolution + 1);
  int map_size_y = (int) ((y_max - y_min) / map_resolution + 1);
  reference_map.info.width = map_size_x;
  reference_map.info.height = map_size_y;
  reference_map.info.resolution = map_resolution;
  reference_map.data.clear();
  tf::quaternionTFToMsg(tf::createQuaternionFromYaw(fixed_node->pose.theta), reference_map.info.origin.orientation);
  
  tf::Point lower_left = fixed_to_odom * tf::Vector3(x_min, y_min, 0);
  tf::pointTFToMsg(lower_left, reference_map.info.origin.position);

  marker_pub.publish(rays);
}

void ScanMatcher::fillMap(vector<GraphNode*> reference_nodes) {
  // initialize to empty
  for (uint i = 0; i < reference_map.info.width * reference_map.info.height; i++) {
    reference_map.data.push_back(0);
  }

  // just create transform from fixed node which is essentially a pose w.r.t odom
  tf::Transform fixed_to_odom;
  fixed_to_odom.setOrigin(tf::Vector3(fixed_node->pose.x, fixed_node->pose.y, 0));
  fixed_to_odom.setRotation(tf::createQuaternionFromYaw(fixed_node->pose.theta));
  tf::Transform odom_to_fixed = fixed_to_odom.inverse();

  tf::Point lower_left_odom;
  tf::pointMsgToTF(reference_map.info.origin.position,lower_left_odom); // get the map origin in odom frame
  tf::Point lower_left_fixed = fixed_to_odom.inverse() * lower_left_odom; // translate into fixed frame

  tf::StampedTransform laser_to_base_link;
  string base_link_frame = "/base_link";
  string laser_frame = "/base_laser_link";
  listener->lookupTransform(base_link_frame, laser_frame, ros::Time(0), laser_to_base_link);

  for (size_t i = 0; i < reference_nodes.size(); i++) {
    GraphNode *node = reference_nodes[i];

    tf::Transform pose_to_odom; // this is really base_link (at scan recording pose) to odom
    pose_to_odom.setOrigin(tf::Vector3(node->pose.x,node->pose.y,0.0));
    pose_to_odom.setRotation(tf::createQuaternionFromYaw(node->pose.theta));
    tf::Transform laser_to_odom =  pose_to_odom * laser_to_base_link;
    
    double angle_min = node->scan.angle_min;
    double angle_inc = node->scan.angle_increment;
    double range_min = node->scan.range_min;
    double range_max = node->scan.range_max;   

    for (size_t j = 0; j < node->scan.ranges.size(); j++) {
      double range = node->scan.ranges[j];
 
      // TODO: does range_max imply free space?
      if (range < range_min || range >= range_max) {
        continue;
      }

      double scan_x = range * cos(angle_min + angle_inc * j);
      double scan_y = range * sin(angle_min + angle_inc * j);

      tf::Point scan_pt_laser(scan_x,scan_y,0.0); // scan point in the frame of the pose from which it was recorded
      tf::Point scan_pt_odom = laser_to_odom * scan_pt_laser; // in the odom frame
      tf::Point scan_pt_fixed = odom_to_fixed * scan_pt_odom; // in the fixed frame

      tf::Point scan_pt_map = scan_pt_fixed;
      scan_pt_map -= lower_left_fixed; // subtract origin from scan point in fixed frame
      
      int x_map = (int) (scan_pt_map.getX() / reference_map.info.resolution); // turn into index
      int y_map = (int) (scan_pt_map.getY() / reference_map.info.resolution);
      if (x_map >= (int) reference_map.info.width || x_map < 0 || y_map >= (int) reference_map.info.height || y_map < 0) {
	ROS_WARN("[ScanMatcher::fillMap]\tinvalid map coordinates: (%d,%d)",x_map,y_map);
      } else {
	reference_map.data[x_map + y_map*reference_map.info.width] = 1; // row-major
      }
    }
  }
}

RobotPose ScanMatcher::getMaxPose() {
  return max_pose;
}

void ScanMatcher::setReferenceNodes(vector<GraphNode*> reference_nodes, GraphNode* fixed_node) {
  this->fixed_node = fixed_node;
  initMap(reference_nodes);  
  fillMap(reference_nodes);
  
  tf::Transform fixed_to_odom;
  fixed_to_odom.setOrigin(tf::Vector3(fixed_node->pose.x, fixed_node->pose.y, 0));
  fixed_to_odom.setRotation(tf::createQuaternionFromYaw(fixed_node->pose.theta));
  tf::Transform odom_to_fixed = fixed_to_odom.inverse();

  tf::Point lower_left_odom;
  tf::pointMsgToTF(reference_map.info.origin.position,lower_left_odom); // get the map origin in odom frame
  tf::Point lower_left_fixed = odom_to_fixed * lower_left_odom; // translate into fixed frame

  fixed_i = (int) (-1*lower_left_fixed.getX() / reference_map.info.resolution);
  fixed_j = (int) (-1*lower_left_fixed.getY() / reference_map.info.resolution);

  ROS_INFO("[ScanMatcher::setReferenceNodes]\tfixed_i=%d (width=%d), fixed_j=%d (height=%d)",
	   fixed_i, reference_map.info.width, fixed_j, reference_map.info.height);

  reference_map.header.frame_id = "/odom";
  reference_map.header.stamp = ros::Time::now();
  occ_pub.publish(reference_map);
}

double ScanMatcher::scanMatch(GraphNode* test_node, bool use_odom, bool visualize) { // TODO: return mean and covariance between test_node and fixed_node
  this->test_node = test_node;
  setupPoseGrid();
  
  SensorModel2 *sensor_model = new SensorModel2(&reference_map, &(test_node->scan));
  MotionModel *motion_model = new MotionModel(1, 1, 2);
  motion_model->setOdometry(fixed_node->pose, test_node->pose);
  
  
  tf::StampedTransform laser_to_base_link;
  string base_link_frame = "/base_link";
  string laser_frame = "/base_laser_link";
  listener->lookupTransform(base_link_frame, laser_frame, ros::Time(0), laser_to_base_link);
  
  RobotPose origin;
  origin.x = origin.y = origin.theta = 0;
  
  for (int i = 0; i < grid_dim_theta; i++) {
    // compute slice
    for (int j = 0; j < grid_dim_x; j++) {
      for (int k = 0; k < grid_dim_y; k++) {
	      RobotPose pose_fixed = pose_grid->getCellPose(i,j,k);
	      
	      tf::Transform pose_to_fixed; // this is really base_link (at scan recording pose) to odom
        pose_to_fixed.setOrigin(tf::Vector3(pose_fixed.x,pose_fixed.y,0.0));
        pose_to_fixed.setRotation(tf::createQuaternionFromYaw(pose_fixed.theta));
        tf::Transform laser_to_fixed =  pose_to_fixed * laser_to_base_link;
        
        RobotPose laser_pose;
        laser_pose.x = laser_to_fixed.getOrigin().getX();
        laser_pose.y = laser_to_fixed.getOrigin().getY();
        laser_pose.theta = tf::getYaw(laser_to_fixed.getRotation());
	      
	      int di = (int)(laser_pose.x / reference_map.info.resolution);
	      int dj = (int)(laser_pose.y / reference_map.info.resolution);
	      int map_i = fixed_i + di;
	      int map_j = fixed_j + dj;
	      // log prob

	      double log_motion_prob= 0.0;
	      if (use_odom) {
		log_motion_prob = motion_model->getMotionProb(origin, laser_pose); // motion model
	      }
	      double log_match_score = sensor_model->getLogScore(map_i, map_j, laser_pose.theta); // sensor model
	      double total_score = motion_model_weight * log_motion_prob + log_match_score;
	      pose_grid->setCellValue(i, j, k, total_score);
      }
    }
  }
  
  max_pose = pose_grid->getMaxPose();
  double max_pose_score = pose_grid->getMaxPoseLogScore();

  if (visualize) {
    visualizeLikelihoodField(sensor_model);
    visualizeBestScan();
  }
  
  delete sensor_model;
  delete motion_model;
  return max_pose_score;
}

void ScanMatcher::visualizeLikelihoodField(SensorModel2 *sensor_model) {

  nav_msgs::FancyMap map;
  map.info = reference_map.info;
  map.info.origin.position.z += 0.05;
  map.header.frame_id = "/odom";
  map.header.stamp = ros::Time::now();
  
  nav_msgs::FancyMapChannel chan;
  chan.name = "rgba";
  map.channels.push_back(chan);

  double min_val = 0, max_val = 0;
  for (size_t i = 0; i < reference_map.info.width * reference_map.info.height; i++) {
    double prob = sensor_model->getLikelihood(i);
    if (i == 0) {
      min_val = max_val = prob;
    } else {
      min_val = fmin(min_val, prob);
      max_val = fmax(max_val, prob);
    }
  }
  
  ROS_INFO("[ScanMatcher::visualizeLikelihoodField]\tlikelihood: min=%f, max=%f", min_val, max_val);
  for (size_t i = 0; i < reference_map.info.width * reference_map.info.height; i++) {
    double prob = sensor_model->getLikelihood(i);
    double scaled = (prob - min_val) / (max_val - min_val);
    int value = (int) (scaled * 255);
    map.channels[0].data.push_back(makeColor(0, 0, 255, value));
  }
  
  //visualizeCovariance(&map);
  // ======================
  // visualize center slice
  int i = pose_grid->getMaxThetaSlice();// grid_dim_theta / 2;
  double min_score, max_score;
  
  for (int j = 0; j < grid_dim_x; j++) {
    for (int k = 0; k < grid_dim_y; k++) {
      double log_score = pose_grid->getCellValue(i, j, k);
      double score = exp(log_score);
      if (j == 0 && k == 0) {
        min_score = max_score = score;
      } else {
        min_score = fmin(min_score, score);
        max_score = fmax(max_score, score);
      }  
    }
  }
  
  ROS_INFO("slice: min=%f, max=%f", min_score, max_score);
  
  for (int j = 0; j < grid_dim_x; j++) {
    for (int k = 0; k < grid_dim_y; k++) {
      double log_score = pose_grid->getCellValue(i, j, k);
      double score = exp(log_score);
      double scaled_score = 1;
      if (min_score != max_score) {
        scaled_score = (score - min_score) / (max_score - min_score);
      }
      
      // make a little more visible
      scaled_score = sqrt(scaled_score);
      
    	RobotPose pose_fixed = pose_grid->getCellPose(i,j,k);
      int di = (int) (pose_fixed.x / reference_map.info.resolution);
      int dj = (int) (pose_fixed.y / reference_map.info.resolution);
      int map_i = fixed_i + di;
      int map_j = fixed_j + dj;
      
      int data_idx = map_j * reference_map.info.width + map_i;

      if (data_idx < 0 || data_idx >= reference_map.info.width * reference_map.info.height) {
        ROS_INFO("???? j=%d, k=%d, data_idx=%d", j, k, data_idx);
      } else {
        int alpha = (int) (255 * scaled_score);
        
        // TODO: how to layer and not just overwrite?
        unsigned int prev_color = map.channels[0].data[data_idx];
        map.channels[0].data[data_idx] = overlayColors(prev_color, makeColor(255, 0, 0, alpha));
        
      }
    }
  }
  
  ROS_INFO("[ScanMatcher::visualizeLikelihoodField]\tvisualizing reference cells");
  
  // set fixed to magenta
  int fixed_idx = fixed_j * reference_map.info.width + fixed_i;
  map.channels[0].data[fixed_idx] = makeColor(255, 0, 255, 255);

  // max_pose, relative to fixed node
  int di = (int) (max_pose.x / reference_map.info.resolution);
  int dj = (int) (max_pose.y / reference_map.info.resolution);
  int map_i = fixed_i + di;
  int map_j = fixed_j + dj;
  int data_idx = map_j * reference_map.info.width + map_i;
  
  
  
  //ROS_INFO("[ScanMatcher::visualizeLikelihoodField]\tslice: min=%f, max=%f", min_score, max_score);
  // max pose is cyan
  map.channels[0].data[data_idx] = makeColor(0, 255, 255, 255);
  fancy_pub.publish(map);

  tf::Transform fixed_to_odom;
  fixed_to_odom.setOrigin(tf::Vector3(fixed_node->pose.x, fixed_node->pose.y, 0));
  fixed_to_odom.setRotation(tf::createQuaternionFromYaw(fixed_node->pose.theta));
  
  tf::Transform pose_to_fixed; // this is really base_link (at scan recording pose) to odom
  pose_to_fixed.setOrigin(tf::Vector3(max_pose.x,max_pose.y,0.0));
  pose_to_fixed.setRotation(tf::createQuaternionFromYaw(max_pose.theta));
  tf::Transform pose_to_odom = fixed_to_odom * pose_to_fixed;
  
  visualization_msgs::Marker rays;
  rays.header.frame_id = "/odom";
  rays.header.stamp = ros::Time::now();
  rays.ns = "best_match_position";
  rays.type = visualization_msgs::Marker::ARROW;
  rays.scale.x = 1.0;
  rays.scale.y = 1.0;
  rays.scale.z = 1.0;
  rays.color.r = 1.0;
  rays.color.g = 1.0;
  rays.color.b = 1.0;
  rays.color.a = 1.0;
  
  tf::pointTFToMsg(pose_to_odom.getOrigin(), rays.pose.position);
  tf::quaternionTFToMsg(pose_to_odom.getRotation(), rays.pose.orientation);
  marker_pub.publish(rays);
}


void ScanMatcher::visualizeCovariance(nav_msgs::FancyMap *map) {
  double min_score = 0, max_score = 0;
  double cov_tmp[9];
  Eigen::Matrix3d cov;
  getCovariance(cov_tmp);
  for (int i = 0; i < 9; i++) {
    cov(i/3,i%3) = cov_tmp[i];
  }
  int i = pose_grid->getMaxThetaSlice();
  for (int j = 0; j < grid_dim_x; j++) {
    for (int k = 0; k < grid_dim_y; k++) {
      RobotPose pose = pose_grid->getCellPose(i, j, k);
      double score = getGaussianDensity(pose - max_pose, cov);
      if (j == 0 && k == 0) {
        min_score = max_score = score;
      } else {
        min_score = fmin(min_score, score);
        max_score = fmax(max_score, score);
      }  
    }
  }
  
  ROS_INFO("slice: min=%f, max=%f", min_score, max_score);
  
  for (int j = 0; j < grid_dim_x; j++) {
    for (int k = 0; k < grid_dim_y; k++) {
      RobotPose pose_fixed = pose_grid->getCellPose(i, j, k);
      double score = getGaussianDensity(pose_fixed - max_pose, cov);
      double scaled_score = 1;
      if (min_score != max_score) {
        scaled_score = (score - min_score) / (max_score - min_score);
      }
      
      // make a little more visible
      scaled_score = sqrt(scaled_score);
      
      int di = (int) (pose_fixed.x / reference_map.info.resolution);
      int dj = (int) (pose_fixed.y / reference_map.info.resolution);
      int map_i = fixed_i + di;
      int map_j = fixed_j + dj;
      
      int data_idx = map_j * reference_map.info.width + map_i;

      if (data_idx < 0 || data_idx >= (int) (reference_map.info.width * reference_map.info.height)) {
        ROS_INFO("???? j=%d, k=%d, data_idx=%d", j, k, data_idx);
      } else {
        int alpha = (int) (255 * scaled_score);
        // TODO: how to layer and not just overwrite?
        unsigned int prev_color = map->channels[0].data[data_idx];
        map->channels[0].data[data_idx] = overlayColors(prev_color, makeColor(255, 0, 0, alpha));
      }
    }
  }
}

void ScanMatcher::visualizeBestScan() {

  visualization_msgs::Marker rays;
  rays.header.frame_id = "/odom";
  rays.header.stamp = ros::Time::now();
  rays.ns = "best_scan_projected";
  rays.type = visualization_msgs::Marker::POINTS;
  rays.scale.x = 0.05;
  rays.scale.y = 0.05;
  rays.color.r = 1.0;
  rays.color.g = 1.0;
  rays.color.b = 1.0;
  rays.color.a = 1.0;

  tf::StampedTransform laser_to_base_link;
  string base_link_frame = "/base_link";
  string laser_frame = "/base_laser_link";
  listener->lookupTransform(base_link_frame, laser_frame, ros::Time(0), laser_to_base_link);
  
  tf::Transform fixed_to_odom;
  fixed_to_odom.setOrigin(tf::Vector3(fixed_node->pose.x, fixed_node->pose.y, 0));
  fixed_to_odom.setRotation(tf::createQuaternionFromYaw(fixed_node->pose.theta));
  
  //RobotPose max_pose = pose_grid->getCellPose(grid_dim_theta/2, grid_dim_x/2, grid_dim_y/2);
  tf::Transform pose_to_fixed; // this is really base_link (at scan recording pose) to odom
  pose_to_fixed.setOrigin(tf::Vector3(max_pose.x,max_pose.y,0.0));
  pose_to_fixed.setRotation(tf::createQuaternionFromYaw(max_pose.theta));
  tf::Transform laser_to_odom = fixed_to_odom * pose_to_fixed * laser_to_base_link;
  
  GraphNode *node = test_node;
  double angle_min = node->scan.angle_min;
  double angle_inc = node->scan.angle_increment;
  double range_min = node->scan.range_min;
  double range_max = node->scan.range_max;   

  for (size_t j = 0; j < node->scan.ranges.size(); j++) {
    double range = node->scan.ranges[j];

    // TODO: does range_max imply free space?
    if (range < range_min || range >= range_max) {
      continue;
    }

    double scan_x = range * cos(angle_min + angle_inc * j);
    double scan_y = range * sin(angle_min + angle_inc * j);

    tf::Point scan_pt_laser(scan_x,scan_y,0.0); // scan point in the frame of the pose from which it was recorded
    tf::Point scan_pt_odom = laser_to_odom * scan_pt_laser; // in the odom frame
     
    if (j % 1 == 0) {
      geometry_msgs::Point pt;
      tf::pointTFToMsg(scan_pt_odom, pt);
      rays.points.push_back(pt);
    }
  }
  marker_pub.publish(rays);
}


void ScanMatcher::getMean(RobotPose* mean) {

}

void ScanMatcher::getCovariance(double* cov) {

  for (int i = 0; i < 9; i++) {
    cov[i] = 0;
  }
  double scatter[9];
  double total_score = 0;
  
  // max_pose, pose_grid
  for (int i = 0; i < grid_dim_theta; i++) {
    // compute slice
    for (int j = 0; j < grid_dim_x; j++) {
      for (int k = 0; k < grid_dim_y; k++) {
	      RobotPose pose_fixed = pose_grid->getCellPose(i, j, k);
	      double score = exp(pose_grid->getCellValue(i, j, k));
	      RobotPose offset = pose_fixed - max_pose;
	      offset.scatter(scatter);
	      
	      // update cov
	      for (int l = 0; l < 9; l++) {
	        cov[l] += score * scatter[l];
	      }
	      total_score += score;
      }
    }
  }
  
  // normalize
  for (int i = 0; i < 9; i++) {
    cov[i] /= total_score;
  }
}

void ScanMatcher::setupPoseGrid() {
  // compute odom to fixed frame transform
  tf::Transform fixed_to_odom;
  fixed_to_odom.setOrigin(tf::Vector3(fixed_node->pose.x, fixed_node->pose.y, 0));
  fixed_to_odom.setRotation(tf::createQuaternionFromYaw(fixed_node->pose.theta));
  tf::Transform odom_to_fixed = fixed_to_odom.inverse();
  
  // compute test frame to odom transform
  tf::Transform test_to_odom;
  test_to_odom.setOrigin(tf::Vector3(test_node->pose.x, test_node->pose.y, 0));
  test_to_odom.setRotation(tf::createQuaternionFromYaw(test_node->pose.theta));
  tf::Transform test_to_fixed = odom_to_fixed * test_to_odom;
 
  // compute the test pose, relative to fixed frame
  RobotPose center;
  center.x = test_to_fixed.getOrigin().getX();
  center.y = test_to_fixed.getOrigin().getY();
  center.theta = tf::getYaw(test_to_fixed.getRotation());
  
  pose_grid->setCenter(center);
}

void ScanMatcher::fillPoseGrid() {
  
}

void ScanMatcher::extractMatch() {

}

void ScanMatcher::estimateCovariance() {

}
