#ifndef VIZ_UTIL_H
#define VIZ_UTIL_H

#include <ros/ros.h>
#include "visualization_msgs/Marker.h"
#include "Point.h"

void initMarker(visualization_msgs::Marker &marker, string name, int type) {
  marker.header.frame_id = "/map";
  marker.header.stamp = ros::Time();
  marker.ns = name;
  marker.id = 0;
  marker.type = type;
  marker.action = visualization_msgs::Marker::ADD;
  marker.pose.orientation.w = 1.0;
  marker.scale.x = .1;
  marker.scale.y = .1;
  marker.scale.z = .1;
  marker.color.a = 1.0;
  marker.color.r = 0.0;
  marker.color.g = 1.0;
  marker.color.b = 0.0;
  marker.lifetime = ros::Duration(0);
}

void visualizeHeatMap(ros::Publisher marker_pub, 
		      double* map, 
		      string name, 
		      int MAP_SIZE_X, 
		      int MAP_SIZE_Y, 
		      double MAP_RESOLUTION) {
  // find range
  int NUM_CELLS = MAP_SIZE_X * MAP_SIZE_Y;

  double min = map[0];
  double max = map[0];
  for (int i=0; i < NUM_CELLS; i++) {
    if (map[i] != -1) {
      if (map[i] < min) min = map[i];
      if (map[i] > max) max = map[i];
    }
  }
  double range = max - min;

  visualization_msgs::Marker marker;
  initMarker(marker,name,visualization_msgs::Marker::POINTS);
  geometry_msgs::Point p;
  std_msgs::ColorRGBA c;
  for (int i =0; i < NUM_CELLS; i++) {
    if (map[i] != -1) {
      Point convert_pt(i,MAP_SIZE_X,MAP_SIZE_Y);
      p.x = convert_pt.x * MAP_RESOLUTION;
      p.y = convert_pt.y * MAP_RESOLUTION;
      p.z = 0;
      
      // higher the value, the bluer the color
      c.b = (map[i] - min)/range;;  // should be between 0 and 1
      c.r = 1 - c.b;
      c.g = 0;
      c.a = 1.0;
      
      marker.points.push_back(p);
      marker.colors.push_back(c);
    }
  }
  marker_pub.publish(marker);
  ROS_INFO("[SensorModel::visualizeHeatMap]\t%s, min=%f, range=%f, points.size()=%d",
	   name.c_str(),min,range,marker.points.size());
}

#endif
