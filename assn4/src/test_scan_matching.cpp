#include <ros/ros.h>
#include <visualization_msgs/Marker.h>
#include <geometry_msgs/PoseArray.h>
#include <nav_msgs/OccupancyGrid.h>
#include <nav_msgs/GetMap.h>
#include <sensor_msgs/LaserScan.h>
#include <sensor_msgs/PointCloud.h>
#include <tf/transform_listener.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_datatypes.h>
#include <geometry_msgs/Twist.h>

//#include "MathUtil.h"
#include "MotionModelScanMatcher.h"

using namespace std;

sensor_msgs::LaserScan prev_scan;
sensor_msgs::LaserScan cur_scan;

void scanCallback(const sensor_msgs::LaserScan::ConstPtr& msg) {
 prev_scan = cur_scan;
 cur_scan = *msg;
}


int main(int argc, char** argv) {
  // initialize ros node
  ros::init(argc, argv, "test_scan_matching");    
  ros::NodeHandle nh;
  tf::TransformListener listener;
  // wait for odom-robot transform
  listener.waitForTransform("/odom", "/base_link", ros::Time(), ros::Duration(1.5));
  
  // setup motion model
  ROS_INFO("[ParticleFilter::main]\tinit motion model");
  //MotionModelScanMatcher motion_model(0.01, 0.01, 0.01);
  MotionModelScanMatcher motion_model(1, 1, 2);
  
  // visualization publishers
  ROS_INFO("[ParticleFilter::main]\tinit viz pubs");
  ros::Publisher marker_pub = nh.advertise<visualization_msgs::Marker>("visualization_marker", 0);
  ros::Publisher pose_grid_pub = nh.advertise<sensor_msgs::PointCloud>("sm_pose_grid", 0);
  ros::Publisher fine_pose_grid_pub = nh.advertise<sensor_msgs::PointCloud>("sm_fine_pose_grid", 0);
  ros::Publisher occ_pub = nh.advertise<nav_msgs::OccupancyGrid>("sm_reference_scan", 0);

  motion_model.setMarkerPublisher(&marker_pub);
  motion_model.setPoseGridPublisher(&pose_grid_pub);
  motion_model.setFinePoseGridPublisher(&fine_pose_grid_pub);
  motion_model.setOccPublisher(&occ_pub);

  //subscribe to necessary things, get laser trans
  ros::Subscriber scan_sub = nh.subscribe("scan", 2, scanCallback);
  tf::StampedTransform laserTrans;
  try{ 
    listener.lookupTransform("/odom", "/laser", ros::Time(), laserTrans);
  } catch (tf::TransformException ex){ ROS_ERROR("%s",ex.what()); }

  //init reference stores
  list<sensor_msgs::LaserScan> ref_scans;
  list<RobotPose> ref_poses;
  tf::Quaternion zero_angle = tf::createQuaternionFromYaw(0.0);

  //robot starts at origin
  RobotPose prev(laserTrans.getOrigin().getX(), laserTrans.getOrigin().getY(), laserTrans.getRotation().getAngle());

  //get reference scan
  prev_scan = *ros::topic::waitForMessage<sensor_msgs::LaserScan>("/scan");
  ROS_INFO("[test scan matching]\t prev_scan.ranges.size()=%d", prev_scan.ranges.size());
  
  int loop_rate = 1;
  ros::Rate rate(loop_rate);
  ROS_INFO("entering main loop...");
  while (ros::ok()) {
    //ros::spinOnce();

    try {
      listener.lookupTransform("/odom", "/base_link", ros::Time(), laserTrans);
    } catch (tf::TransformException ex) {
      ROS_ERROR("%s",ex.what());
    }

    RobotPose cur( laserTrans.getOrigin().getX(), laserTrans.getOrigin().getY(), laserTrans.getRotation().getAngle());

    //quick hack
    if(prev.x == 0){
      prev = cur;
      continue;
    }

    if(cur.dist(prev) > 0.3){
      ROS_INFO("\n\n\n =================== BEGIN UPDATE, dist moved = %f ================ \n\n\n", cur.dist(prev));
      ROS_INFO("laserTrans dst (%f %f) angle %f", laserTrans.getOrigin().getX(), laserTrans.getOrigin().getY(), laserTrans.getRotation().getAngle());

      ROS_INFO(" angle represented = %f", zero_angle.angle(laserTrans.getRotation()));

      //get new scan
      cur_scan = *ros::topic::waitForMessage<sensor_msgs::LaserScan>("/scan");

      //add previous scan and pose to refs, manage size
      ref_scans.push_back(prev_scan);
      ref_poses.push_back(prev);
      if(ref_scans.size() >= 5){
        ref_scans.pop_front();
        ref_poses.pop_front();
      }

      //run the motion model scan matching
      motion_model.setOdometry(prev, cur);
      //motion_model.setScans(prev_scan, cur_scan);
      motion_model.setMultipleScans(ref_scans, ref_poses, cur_scan);
      RobotPose cur_hat = motion_model.sampleMotion(prev);

      ROS_INFO("\n\n============== TEST CURHAT AND CUR POSES!!!! =================");
      ROS_INFO("curhat = \t(%f %f %f)", cur_hat.x, cur_hat.y, cur_hat.theta);
      ROS_INFO("cur    = \t(%f %f %f)", cur.x, cur.y, cur.theta);
      ROS_INFO("prev   = \t(%f %f %f)", prev.x, prev.y, prev.theta);

      //update prev stuff
      prev = cur;
      prev_scan = cur_scan;
    }
	
    rate.sleep();
  }

  return 0;
}


/*
  //move the robot!
  ros::Publisher move_pub = nh.advertise<geometry_msgs::Twist>("/turtlebot_node/cmd_vel", 1);	

  geometry_msgs::Twist vel;
  vel.angular.z = 0.0;
  vel.linear.x = 1.0;
  move_pub.publish(vel);

  sleep(1.0);
  vel.linear.x = 0.0;
  move_pub.publish(vel);
*/
