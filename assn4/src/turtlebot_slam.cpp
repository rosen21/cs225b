#include <ros/ros.h>
#include <visualization_msgs/Marker.h>
#include <geometry_msgs/PoseArray.h>
#include <nav_msgs/FancyMap.h>
#include <nav_msgs/GetMap.h>
#include <sensor_msgs/LaserScan.h>
#include <sensor_msgs/PointCloud.h>
#include <tf/transform_listener.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_datatypes.h>

#include "GraphSLAM.h"

using namespace std;

int main(int argc, char** argv) {
  // initialize ros node
  ros::init(argc, argv, "turtlebot_slam");    
  ros::NodeHandle nh;
  
  tf::TransformListener listener;
  tf::TransformBroadcaster br;
  
  // wait for the clock
  ROS_INFO("[turtlebot_slam::main]\twaiting for clock");
  while (ros::Time::now().isZero()) {
    ROS_INFO("... waiting for clock");
    ros::Duration(1.0).sleep();
  }
  
  // wait for tf
  ROS_INFO("[turtlebot_slam::main]\twaiting for tf");
  listener.waitForTransform("base_link", "odom", ros::Time::now(), ros::Duration(1.0));
  listener.waitForTransform("base_link", "laser", ros::Time::now(), ros::Duration(1.0));
  listener.waitForTransform("base_link_laser", "base_link", ros::Time::now(), ros::Duration(1.0));
  ROS_INFO("[turtlebot_slam::main]\tgot init tf");

  GraphSLAM slam;
  slam.initialize(&nh, &listener);
  
  ros::Subscriber scan_sub = nh.subscribe("scan", 1, &GraphSLAM::processLaserReading, &slam);
  
  ros::Publisher marker_pub = nh.advertise<visualization_msgs::Marker>("visualization_marker", 0);
  ros::Publisher pose_pub = nh.advertise<geometry_msgs::PoseArray>("pose_cloud", 0);
  ros::Publisher map_pub = nh.advertise<nav_msgs::FancyMap>("final_map", 0);
  ros::Publisher pose_grid_pub = nh.advertise<sensor_msgs::PointCloud>("sm_pose_grid", 0);
  ros::Publisher fine_pose_grid_pub = nh.advertise<sensor_msgs::PointCloud>("sm_fine_pose_grid", 0);
  ros::Publisher occ_pub = nh.advertise<nav_msgs::OccupancyGrid>("sm_reference_scan", 0);
  
  // final output map
  nav_msgs::FancyMap slam_map;

  int loop_rate = 2; // iterations per second
  int map_update_interval = 10; // iterations between map updates
  
  int it = 0;
  ros::Rate rate(loop_rate);
  
  ROS_INFO("... entering main loop");
  while (ros::ok()) {
    it++;
    // listen for incoming messages. SLAM updates happen here
    ros::spinOnce();

    // visualize the pose graph
    slam.visualizeGraph();
    
    // occasionally generate a new map
    if (it % map_update_interval == 0) {
      slam.generateMap(&slam_map);
      map_pub.publish(slam_map);
    }
    
    rate.sleep();
  }

  return 0;
}
