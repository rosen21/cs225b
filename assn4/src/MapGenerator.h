#ifndef MAP_GENERATOR_H
#define MAP_GENERATOR_H

#include <nav_msgs/FancyMap.h>
#include "PoseGraph.h"

class MapGenerator {
  public:
    MapGenerator();
    
    /* sets the offset between the robot and the laser */
    void setLaserOffset(RobotPose offset);
    
    /* creates an occupancy grid from the pose graph, stores the result in "map" */
    void generateMap(PoseGraph *graph, nav_msgs::FancyMap *map);
   
  private:
    std::string map_frame;
    double map_resolution;
    int scan_stride;
  
    PoseGraph *graph;
    double *log_odds;
    nav_msgs::FancyMap *map;
    RobotPose laser_offset;
    double x_min, x_max, y_min, y_max;
    int map_size_x, map_size_y;
    double range_max;
    
    void processNode(GraphNode *node);
    
    /* returns the array index of the map cell at this position */
    int mapX(double x);
    int mapY(double y);
    int dataIdx(int x, int y);

    
    /* computes a rectangle bound for the map */
    void computeMapBounds();
    
    void updateOdds(int x, int y, double start_x, double start_y, double z_dist);
    
    /* processes a single scan point */
    void processScanPoint(double start_x, double start_y, double angle, double distance);
};

#endif

