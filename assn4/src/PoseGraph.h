#ifndef POSE_GRAPH_H
#define POSE_GRAPH_H

#include <sensor_msgs/LaserScan.h>
#include "RobotPose.h"
#include "se2.h"
#include "Eigen/Core"

/* Node in the pose graph. */
class GraphNode {
  public:
    int idx;
    RobotPose pose;
    sensor_msgs::LaserScan scan;
    
    GraphNode(int _idx) {
      idx = _idx;
    }
};

/* Edge connecting two nodes in the pose graph */
class GraphEdge {
  public:
    int idx1, idx2;
    SE2 mean;
    // necessary for objects with embedded eigen objects
    // see http://eigen.tuxfamily.org/dox-devel/TopicStructHavingEigenMembers.html
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    Matrix3d cov;
    GraphEdge(int a, int b) {
      idx1 = a;
      idx2 = b;
    }
};

class PoseGraph {
  public:
    ~PoseGraph() {
      // clean up dynamic allocations
      clear();
    }
    
    void clear() {
      for (size_t i = 0; i < edges.size(); i++) {
        delete edges[i];
      }
      for (size_t i = 0; i < nodes.size(); i++) {
        delete nodes[i];
      }
      nodes.clear();
      edges.clear();
    }
    
    GraphNode *getNode(int idx) {
      return nodes[idx];
    }
    
    GraphNode *createNode() {
      int idx = nodes.size();
      GraphNode *node = new GraphNode(idx);
      nodes.push_back(node);
      return node;
    }
    
    GraphNode *getLastNode() {
      return nodes[nodes.size() - 1];
    }
    
    GraphEdge *getEdge(int idx) {
      return edges[idx];
    }
    
    size_t getNumNodes() {
      return nodes.size();
    }
    
    size_t getNumEdges() {
      return edges.size();
    }
    
    bool empty() {
      return getNumNodes() == 0;
    }
    
    GraphEdge* addEdge(GraphNode *node1, GraphNode *node2) {
      GraphEdge *edge = new GraphEdge(node1->idx, node2->idx);
      edges.push_back(edge);
      return edge;
    }
    
  private:
    std::vector<GraphNode *> nodes;
    std::vector<GraphEdge *> edges;
};

#endif

