#ifndef _MOTION_MODEL_H
#define _MOTION_MODEL_H

#include "RobotPose.h"
#include <ros/ros.h>
#include "sensor_msgs/LaserScan.h"
#include <tf/transform_datatypes.h>

struct PoseOffset {
  double trans;
  double rot1;
  double rot2;
  RobotPose asPose() {
    RobotPose p;
    p.x = cos(rot1)*trans;
    p.y = sin(rot1)*trans;
    p.theta = rot1+rot2;
    return p;
  }
};


class MotionModel {
 public:
  MotionModel() {}
  MotionModel(double _a1, double _a2, double _a3) { 
    alpha1 = _a1; 
    alpha2 = _a2; 
    alpha3 = _a3; 
    trans_epsilon = 0.01; 
  }

  /* sets odometry data for motion sampling */
  void setOdometry(RobotPose _odom_start, RobotPose _odom_end);
  double getMotionProb(RobotPose poseOrigin, RobotPose p);
  
  /* uses the current odometry data to sample a new pose from the given pose */
  virtual RobotPose sampleMotion(RobotPose pose);
  virtual void setScans(const sensor_msgs::LaserScan reference_scan, 
			const sensor_msgs::LaserScan curr_scan) {};
  virtual void setMultipleScans(list<sensor_msgs::LaserScan> ref_scans, 
		list<RobotPose> ref_poses, const sensor_msgs::LaserScan curr_scan) {};
		
  virtual void setMarkerPublisher(ros::Publisher *_marker_pub) { marker_pub = _marker_pub; }
  virtual void setPoseGridPublisher(ros::Publisher *_pose_grid_pub) { pose_grid_pub = _pose_grid_pub; }
  virtual void setFinePoseGridPublisher(ros::Publisher *_fine_pose_grid_pub) { fine_pose_grid_pub = _fine_pose_grid_pub; }
  virtual void setOccPublisher(ros::Publisher *_occ_pub) { occ_pub = _occ_pub; }

  
 protected:
  void getPoseOffset(RobotPose start, RobotPose end, PoseOffset &offset);
  ros::Publisher *marker_pub;
  ros::Publisher *pose_grid_pub;
  ros::Publisher *fine_pose_grid_pub;
  ros::Publisher *occ_pub;
  PoseOffset odom_offset;

  // error characteristics
  double alpha1;
  double alpha2;
  double alpha3;


 private:
  double trans_epsilon;
  // odometry data
  double rot1_var, rot2_var, trans_var;
};

#endif

