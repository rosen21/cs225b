#ifndef GRAPH_SLAM_H
#define GRAPH_SLAM_H

#include <vector>
#include <sensor_msgs/LaserScan.h>
#include <tf/transform_listener.h>
#include <tf/transform_datatypes.h>
#include <nav_msgs/FancyMap.h>
#include "MapGenerator.h"
#include "PoseGraph.h"
#include "RobotPose.h"
#include <ros/ros.h>
#include "MotionModelScanMatcher.h"
#include "ScanMatcher.h"

class GraphSLAM {
  public:
    GraphSLAM();
    ~GraphSLAM();
    
    void initialize(ros::NodeHandle *nh, tf::TransformListener *listener);
  
    /* main callback for processing a new laser scan */
    void processLaserReading(const sensor_msgs::LaserScan::ConstPtr &msg);
    
    /* publishes a visualization of the pose graph */
    void visualizeGraph();
    
    /* generates an occupancy grid from the pose graph */
    void generateMap(nav_msgs::FancyMap *map);
  private:
    tf::TransformListener *listener;
    ros::Publisher pose_pub, marker_pub;
    
    ScanMatcher* scan_matcher;
  
    // basic configuration
    std::string odom_frame, robot_frame, laser_frame;
    MapGenerator map_generator;
    
    // SLAM configuration
    double min_distance, min_rotation;
    int sequential_window;
    int reference_window;
    double global_reg_dist_thresh;
    double global_reg_conf_thresh;

    // private vars
    RobotPose laser_offset;
    PoseGraph pose_graph;
    
    bool robotHasMoved(RobotPose start_pose, RobotPose end_pose);
    RobotPose lookupRobotPose(ros::Time stamp);
    RobotPose lookupLaserOffset(ros::Time stamp);
    RobotPose lookupFramePose(std::string frame, std::string fixed_frame, ros::Time stamp);
    void addNode(GraphNode *new_node); // these serve as interfaces to both the pose_graph and the optimizer
    void addLocalRegConstraints(GraphNode* new_node, int reference_index);
    void addGlobalRegConstraints(GraphNode *new_node);
    void visualizePoses();
    void visualizeMeasurements();
    void visualizeConstraints();
    void visualizeScans();
    void robotPoseToSE2(const RobotPose &robot_pose, SE2& se2);
    void optimize();
    double scoreEdge(GraphEdge* edge);
};


#endif

