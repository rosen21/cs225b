#ifndef SCANMATCHER_H
#define SCANMATCHER_H

#include "RobotPose.h"
#include "PoseGraph.h"
#include "PoseGrid2.h"
#include "SensorModel2.h"
#include <nav_msgs/OccupancyGrid.h>
#include <nav_msgs/FancyMap.h>
#include <ros/ros.h>
#include <tf/transform_listener.h>

class ScanMatcher{

 public:
  ScanMatcher();
  void initialize(ros::NodeHandle *nh, tf::TransformListener *listener);
  void setReferenceNodes(vector<GraphNode*> reference_nodes, GraphNode* fixed_node);
  double scanMatch(GraphNode* test_node, bool use_odom=true, bool visualize=true); // returns score of max_pose
  void getMean(RobotPose* mean);
  void getCovariance(double* cov);
  RobotPose getMaxPose();

 private:
  tf::TransformListener *listener;
  ros::Publisher marker_pub;
  ros::Publisher pose_grid_pub;
  ros::Publisher likelihood_field_pub;
  ros::Publisher occ_pub;
  ros::Publisher fancy_pub;
  
  // private vars
  GraphNode* fixed_node;
  int fixed_i;
  int fixed_j;
  GraphNode* test_node;
  PoseGrid2* pose_grid;
  nav_msgs::OccupancyGrid reference_map;
  RobotPose max_pose;

  double grid_dim_x, grid_dim_y, grid_dim_theta;
  double grid_res_xy, grid_res_theta;

  // map / occupancy grid params
  double map_resolution;
  double map_margin_buffer;
  double motion_model_weight;

  void initMap(vector<GraphNode*> reference_nodes);
  void fillMap(vector<GraphNode*> reference_nodes);
  void setupPoseGrid();
  void fillPoseGrid();
  void extractMatch();
  void estimateCovariance();
  void visualizeLikelihoodField(SensorModel2 *sensor_model);
  void visualizeBestScan();
  void visualizeCovariance(nav_msgs::FancyMap *map);
};

#endif
