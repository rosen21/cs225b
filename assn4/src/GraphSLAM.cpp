#include "GraphSLAM.h"

#include <geometry_msgs/Pose.h>
#include <geometry_msgs/PoseArray.h>
#include <geometry_msgs/PoseStamped.h>
#include <visualization_msgs/Marker.h>
#include <std_msgs/ColorRGBA.h>

#include "MathUtil.h"

#include "g2o/core/graph_optimizer_sparse.h"
#include "g2o/core/block_solver.h"
#include "g2o/solvers/csparse/linear_solver_csparse.h"
#include "g2o/core/base_vertex.h"
#include "g2o/core/base_binary_edge.h"
#include <Eigen/Core>
#include "se2.h"
#include "vertex_se2.h"
#include "edge_se2.h"
#include "MotionModelScanMatcher.h"

typedef BlockSolver< BlockSolverTraits<-1, -1> >  SlamBlockSolver;
typedef LinearSolverCSparse<SlamBlockSolver::PoseMatrixType> SlamLinearSolver;

/* basic initialization */
GraphSLAM::GraphSLAM() {
  // tf topics
  odom_frame = "odom";
  robot_frame = "base_link";
  laser_frame = "base_laser_link";
  
  // minimum odom change before update
  min_distance = 0.35; // meters
  min_rotation = 1.0; // radians
  
  // SLAM constraint parameters

  reference_window = 2; // number of scans to include in reference scan with sequential scan matching
  sequential_window = 2; // previous neighbors to check
  global_reg_dist_thresh = 0.6; // distance between poses in meters to consider as candidates
  global_reg_conf_thresh = -30.0; // minimum log score of a scan match to create a global edge

  scan_matcher = new ScanMatcher();
}

GraphSLAM::~GraphSLAM() {
  delete scan_matcher;
}

void GraphSLAM::initialize(ros::NodeHandle *nh, tf::TransformListener *listener) {
  this->listener = listener;
  marker_pub = nh->advertise<visualization_msgs::Marker>("visualization_marker", 0);
  pose_pub = nh->advertise<geometry_msgs::PoseArray>("pose_cloud", 0);
  scan_matcher->initialize(nh, listener);
}

void GraphSLAM::robotPoseToSE2(const RobotPose& robot_pose, SE2& se2) {
  se2.set(robot_pose.x, robot_pose.y, robot_pose.theta);
}

RobotPose GraphSLAM::lookupFramePose(std::string frame, std::string fixed_frame, ros::Time stamp) {
  // TODO: why does this seem to help?? i have no idea
  listener->waitForTransform(frame, fixed_frame, stamp, ros::Duration(1.0));
  std::string err_msg;
  if (!listener->canTransform(frame, fixed_frame, stamp, &err_msg)) {
    ROS_INFO("!! cannot transform %s to %s, t=%f, now=%f",
      frame.c_str(), fixed_frame.c_str(), stamp.toSec(), ros::Time::now().toSec());
  }
  
  // "identity" pose of robot in its own frame
  tf::Pose pose;
	pose.setOrigin(tf::Vector3(0, 0, 0));
	pose.setRotation(tf::createQuaternionFromYaw(0));
	
	// setup message
	geometry_msgs::PoseStamped pose_stamped;
	pose_stamped.header.frame_id = frame;
	pose_stamped.header.stamp = stamp;
	tf::poseTFToMsg(pose, pose_stamped.pose);
  
  // transform the message
  geometry_msgs::PoseStamped result_msg;
  listener->transformPose(fixed_frame, pose_stamped, result_msg);
  
  // extract pose from result
  tf::Stamped<tf::Pose> result_pose;
	tf::poseStampedMsgToTF(result_msg, result_pose);
	
	// turn into a RobotPose
  RobotPose robot_pose;
  robot_pose.x = result_pose.getOrigin().getX();
  robot_pose.y = result_pose.getOrigin().getY();
  robot_pose.theta = tf::getYaw(result_pose.getRotation());
  
  return robot_pose;
}

/* returns the current robot pose, relative to odom */
RobotPose GraphSLAM::lookupRobotPose(ros::Time stamp) {
  return lookupFramePose(robot_frame, odom_frame, stamp);
}

RobotPose GraphSLAM::lookupLaserOffset(ros::Time stamp) {
  return lookupFramePose(laser_frame, robot_frame, stamp);
}

/* returns true if the robot has moved at least the min amount needed for update */
bool GraphSLAM::robotHasMoved(RobotPose start_pose, RobotPose end_pose) {
  double dx = end_pose.x - start_pose.x;
  double dy = end_pose.y - start_pose.y;
  double rotation = headingChange(end_pose.theta, start_pose.theta);
  double distance = sqrt(dx * dx + dy * dy);
  return distance >= min_distance || rotation >= min_rotation;
}

/* draws the poses in the graph */
void GraphSLAM::visualizePoses() {
  visualization_msgs::Marker pose_pts;
  pose_pts.header.stamp = ros::Time::now();
  pose_pts.header.frame_id = odom_frame;
  pose_pts.type = visualization_msgs::Marker::POINTS;
  pose_pts.scale.x = 0.1;
  pose_pts.scale.y = 0.1;
  pose_pts.color.g = 1.0;
  pose_pts.color.a = 1.0;
  
  geometry_msgs::PoseArray cloud;
  cloud.header.frame_id = odom_frame;
  cloud.header.stamp = ros::Time();
  
  // add each node's pose to the visualization
  for (size_t i = 0; i < pose_graph.getNumNodes(); i++) {
    GraphNode *node = pose_graph.getNode(i);
  
    geometry_msgs::Pose pose;  
    pose.position.x = node->pose.x;
    pose.position.y = node->pose.y;
    pose.position.z = 0.05; // elevate slightly to improve visibility
    tf::quaternionTFToMsg(tf::createQuaternionFromYaw(node->pose.theta), pose.orientation);
    pose_pts.points.push_back(pose.position);
    cloud.poses.push_back(pose);
  }
  marker_pub.publish(pose_pts);
  pose_pub.publish(cloud);
}

void GraphSLAM::visualizeMeasurements() {
  //ROS_INFO("[GraphSLAM::visualizeMeasurements]\tentering");
  visualization_msgs::Marker lines;
  lines.header.frame_id = odom_frame;
  lines.header.stamp = ros::Time();
  lines.ns = "scan_matches_estimates";
  lines.type = visualization_msgs::Marker::LINE_LIST;
  
  lines.scale.x = 0.014;
  lines.color.r = 0.0f;
  lines.color.g = 1.0f;
  lines.color.b = 0.0f;
  lines.color.a = 1;
  
  // add a line for each edge in graph, starting at node1 and going to where
  // its scan match expects node2 to lie
  for (size_t i = 0; i < pose_graph.getNumEdges(); i++) {
    GraphEdge *edge = pose_graph.getEdge(i);
    GraphNode *node1 = pose_graph.getNode(edge->idx1);
    //GraphNode *node2 = pose_graph.getNode(edge->idx2);
    // TODO: consider drawing line between node2 and estimate
    
    geometry_msgs::Point pt1;
    pt1.x = node1->pose.x;
    pt1.y = node1->pose.y;
    pt1.z = 0;
    
    // pt1 in odom or pt_to_odom transform
    SE2 pt1_to_odom;
    robotPoseToSE2(node1->pose,pt1_to_odom);

    // pt2 in pt1 or pt2 to pt1
    SE2 pt2_to_pt1 = edge->mean;
    SE2 pt2_to_odom = pt1_to_odom * pt2_to_pt1;
    Vector2d pt2_odom = pt2_to_odom.translation();

    geometry_msgs::Point pt2;
    pt2.x = pt2_odom[0];
    pt2.y = pt2_odom[1];
    pt2.z = 0;
    
    lines.points.push_back(pt1);
    lines.points.push_back(pt2);
  }
  
  marker_pub.publish(lines);
}

double GraphSLAM::scoreEdge(GraphEdge* e) {
  //ROS_INFO("[GraphSLAM::scoreEdge]\tscoring edge: (%d,%d)", e->idx1, e->idx2);

  
  // ODOM distance, not useful; use scan match instead
  // // note: edge data not set for odom_pose_graph
  // RobotPose rp1 = odom_nodes[e->idx1].pose;
  // RobotPose rp2 = odom_nodes[e->idx2].pose;
  // SE2 p1;
  // SE2 p2;
  // robotPoseToSE2(rp1,p1);
  // robotPoseToSE2(rp2,p2);
  // SE2 odom_diff = p1.inverse() * p2;

  // pt2 in pt1 or pt2 to pt1
  SE2 pt2_to_pt1 = e->mean; // expected offset by scan matching
  
  RobotPose opt_rp1 = pose_graph.getNode(e->idx1)->pose;
  RobotPose opt_rp2 = pose_graph.getNode(e->idx2)->pose;
  SE2 opt_p1;
  SE2 opt_p2;
  robotPoseToSE2(opt_rp1,opt_p1);
  robotPoseToSE2(opt_rp2,opt_p2);
  SE2 opt_diff = opt_p1.inverse() * opt_p2; // offset after optimization

  RobotPose opt_diff_rp;
  opt_diff_rp.x = pt2_to_pt1[0] - opt_diff[0];
  opt_diff_rp.y = pt2_to_pt1[1] - opt_diff[1];
  opt_diff_rp.theta = pt2_to_pt1[2] - opt_diff[2];
  double cost = 1 - pow(exp(log(getGaussianDensity(opt_diff_rp, e->cov))),0.25);
  // the larger the cost, the more violated the constraint

  return cost;
}

/* draws the constraints between nodes in the graph */
void GraphSLAM::visualizeConstraints() {
  // constraint edges
  visualization_msgs::Marker lines;
  lines.header.frame_id = odom_frame;
  lines.header.stamp = ros::Time();
  lines.ns = "graph_constraints";
  lines.type = visualization_msgs::Marker::LINE_LIST;
  
  lines.scale.x = 0.02;
  lines.color.r = 0.0f;
  lines.color.g = 0.0f;
  lines.color.b = 1.0f;
  lines.color.a = 1;
  
  // add a line for each edge in the graph
  for (size_t i = 0; i < pose_graph.getNumEdges(); i++) {
    GraphEdge *edge = pose_graph.getEdge(i);
    GraphNode *node1 = pose_graph.getNode(edge->idx1);
    GraphNode *node2 = pose_graph.getNode(edge->idx2);
    
    geometry_msgs::Point pt1;
    pt1.x = node1->pose.x;
    pt1.y = node1->pose.y;
    pt1.z = 0;
    
    geometry_msgs::Point pt2;
    pt2.x = node2->pose.x;
    pt2.y = node2->pose.y;
    pt2.z = 0;
    
    lines.points.push_back(pt1);
    lines.points.push_back(pt2);

    double edge_score = scoreEdge(edge);
    std_msgs::ColorRGBA color;
    color.r = edge_score; // more red means more unlikely
    color.g = 0;
    color.b = 1;
    color.a = 0;
    lines.colors.push_back(color);
    lines.colors.push_back(color);
  }
  
  marker_pub.publish(lines);
}



/* draws the constraints between nodes in the graph */
void GraphSLAM::visualizeScans() {

  if (pose_graph.empty()) {
    return;
  }

  visualization_msgs::Marker scans;
  scans.header.frame_id = "/odom";
  scans.header.stamp = ros::Time::now();
  scans.ns = "scans";
  scans.type = visualization_msgs::Marker::POINTS;
  scans.scale.x = 0.05;
  scans.scale.y = 0.05;
  scans.color.r = 1.0;
  scans.color.b = 1.0;
  scans.color.a = 1.0;

  tf::StampedTransform laser_to_base_link;
  string base_link_frame = "/base_link";
  string laser_frame = "/base_laser_link";
  listener->lookupTransform(base_link_frame, laser_frame, ros::Time(0), laser_to_base_link);

  //size_t start_idx = max(0, (int)pose_graph.getNumNodes() - sequential_window);
  size_t start_idx = 0;

  for (size_t i = start_idx; i < pose_graph.getNumNodes(); i++) {
    GraphNode *node = pose_graph.getNode(i);

    tf::Transform pose_to_odom; // this is really base_link (at scan recording pose) to odom
    pose_to_odom.setOrigin(tf::Vector3(node->pose.x,node->pose.y,0.0));
    pose_to_odom.setRotation(tf::createQuaternionFromYaw(node->pose.theta));
    tf::Transform laser_to_odom = pose_to_odom * laser_to_base_link;
    
    double angle_min = node->scan.angle_min;
    double angle_inc = node->scan.angle_increment;
    double range_min = node->scan.range_min;
    double range_max = node->scan.range_max;   

    for (size_t j = 0; j < node->scan.ranges.size(); j++) {
      double range = node->scan.ranges[j];
 
      // TODO: does range_max imply free space?
      if (range < range_min || range >= range_max) {
        continue;
      }

      double scan_x = range * cos(angle_min + angle_inc * j);
      double scan_y = range * sin(angle_min + angle_inc * j);

      tf::Point scan_pt_laser(scan_x,scan_y,0.0); // scan point in the frame of the pose from which it was recorded
      tf::Point scan_pt_odom = laser_to_odom * scan_pt_laser; // in the odom frame
       
      if (j % 5 == 0) {
	      geometry_msgs::Point pt;
	      tf::pointTFToMsg(scan_pt_odom, pt);
	      scans.points.push_back(pt);
      }
    }
  }
  marker_pub.publish(scans);
}

/* creates a visualization of the graph structure */
void GraphSLAM::visualizeGraph() {
  visualizePoses();
  visualizeMeasurements();
  visualizeConstraints();
  visualizeScans();
}

void GraphSLAM::generateMap(nav_msgs::FancyMap *map) {
  map_generator.setLaserOffset(laser_offset);
  map_generator.generateMap(&pose_graph, map);
}

/* finds and adds constraints to the new node */
void GraphSLAM::addGlobalRegConstraints(GraphNode* new_node) {

  double min_dist = DBL_MAX;
  GraphNode* closest_node = NULL;

  for (int i = 0; i < new_node->idx - sequential_window; i++) {
    GraphNode* curr_node = pose_graph.getNode(i);
    
    double delta_x = new_node->pose.x - curr_node->pose.x;
    double delta_y = new_node->pose.y - curr_node->pose.y;
    double global_dist = pow(pow(delta_x,2) + pow(delta_y,2), 0.5);

    if(global_dist < min_dist){
      min_dist = global_dist;
      closest_node = curr_node;
    }
  }

  if (min_dist < global_reg_dist_thresh) {
    ROS_INFO("\n==================================!!!!!!!!!!!!!!!!!!!!!!\n\nGraphSLAM::addGlobalRegConstraints  -  adding constraint between new node and node %d, distance %f\n\n", closest_node->idx, min_dist);

     addLocalRegConstraints(new_node, closest_node->idx); 
  }
}

/* finds and adds constraints to the new node */
void GraphSLAM::addLocalRegConstraints(GraphNode* new_node, int reference_index) {
  ROS_INFO("[GraphSLAM::addLocalRegConstraints]\tchecking ref node: %d, new_node: %d", reference_index, new_node->idx);
  // sequential constraints

  int end_index = min(new_node->idx, reference_index + reference_window / 2);
  int start_index = max(0, end_index - reference_window);
  vector<GraphNode*> ref_nodes;
  for (int i = start_index; i < end_index; i++) {
    GraphNode *prev_node = pose_graph.getNode(i);
    ref_nodes.push_back(prev_node);
  }
  GraphNode* ref_node = pose_graph.getNode(reference_index);

  scan_matcher->setReferenceNodes(ref_nodes,ref_node);
  bool use_odom = true;
  bool visualize = false;
  double confidence = scan_matcher->scanMatch(new_node, use_odom, visualize);
  ROS_INFO("[GraphSLAM::addLocalRegConstraints]\tscan match for edge(%d,%d) confidence = %f", reference_index, new_node->idx, confidence);

  //if its a local constraint, or its global and has high enough confidence
  if (confidence > global_reg_conf_thresh || end_index == new_node->idx) {
  //if (true) {
    
    // create edge object
    GraphEdge *e = pose_graph.addEdge(ref_node, new_node);
    
    Matrix3d& cov = e->cov;
    double cov_tmp[9];
    scan_matcher->getCovariance(cov_tmp);
    for (int i=0; i < 9; i++) {
      cov(i/3,i%3) = cov_tmp[i];
    }
    
    cov(0,2) = 0; // zero out weird covariances
    cov(1,2) = 0;
    cov(2,0) = 0;
    cov(2,1) = 0;  
    
    RobotPose max_pose = scan_matcher->getMaxPose();
    e->mean.set(max_pose.x, max_pose.y, max_pose.theta);
    
    ROS_INFO("[GraphSLAM::addLocalRegConstraints]\tcov : [%e\t%e\t%e]",cov(0,0), cov(0,1), cov(0,2));
    ROS_INFO("[GraphSLAM::addLocalRegConstraints]\t      [%e\t%e\t%e]",cov(1,0), cov(1,1), cov(1,2));
    ROS_INFO("[GraphSLAM::addLocalRegConstraints]\t      [%e\t%e\t%e]",cov(2,0), cov(2,1), cov(2,2));
    
    ROS_INFO("[GraphSLAM::addLocalRegConstraints]\tadding edge (%d, %d)",e->idx1,e->idx2);
    ROS_INFO("[GraphSLAM::addLocalRegConstraints]\tmean: [%e, %e, %e]",e->mean[0],e->mean[1],e->mean[2]);
  }
}

void GraphSLAM::optimize() {

  SparseOptimizer optimizer;
  SlamLinearSolver* linearSolver = new SlamLinearSolver();
  linearSolver->setBlockOrdering(false);
  SlamBlockSolver* solver = new SlamBlockSolver(&optimizer, linearSolver);
  optimizer.setSolver(solver);

  //ROS_INFO("[GraphSLAM::optimize]\tadding %d nodes", pose_graph.getNumNodes());
  for(size_t i=0; i < pose_graph.getNumNodes(); i++) {
    GraphNode *pose_node = pose_graph.getNode(i);
    // create and add vertices
    //ROS_INFO("[GraphSLAM::optimize]\toriginal node %d: (%f,%f,%f)",i,pose_node->pose.x,pose_node->pose.y,pose_node->pose.theta);
    SE2 pose_se2;
    robotPoseToSE2(pose_node->pose,pose_se2);
    VertexSE2* v =  new VertexSE2;
    v->setId(pose_node->idx);
    v->setEstimate(pose_se2);
    optimizer.addVertex(v);
  }

  //ROS_INFO("[GraphSLAM::optimize]\tadding %d edges", pose_graph.getNumEdges());
  for(size_t i=0; i < pose_graph.getNumEdges(); i++) {
    GraphEdge *e = pose_graph.getEdge(i);
    //ROS_INFO("[GraphSLAM::optimize]\tadding edge: (%d,%d)", e->idx1, e->idx2);

    RobotPose rp1 = pose_graph.getNode(e->idx1)->pose;
    RobotPose rp2 = pose_graph.getNode(e->idx2)->pose;
    SE2 p1;
    SE2 p2;
    robotPoseToSE2(rp1,p1);
    robotPoseToSE2(rp2,p2);
    SE2 diff_se2 = p1.inverse() * p2;

    EdgeSE2* odom_edge = new EdgeSE2;
    odom_edge->vertices()[0] = optimizer.vertex(e->idx1);
    odom_edge->vertices()[1] = optimizer.vertex(e->idx2);

    odom_edge->setMeasurement(e->mean);
    odom_edge->setInverseMeasurement(e->mean.inverse());

    odom_edge->setInformation(e->cov.inverse());

    optimizer.addEdge(odom_edge);
  }

  //ROS_INFO("[GraphSLAM::optimize]\tpre-optimization");
  //ros::Duration(10.0).sleep();

  optimizer.vertex(pose_graph.getNumNodes() - 1)->setFixed(true); // set pose which will not move (seems arbitrary)

  optimizer.setVerbose(true);
  //optimizer.setVerbose(false);
  //ROS_INFO("[GraphSLAM::optimize]\toptimizing");
  optimizer.initializeOptimization();
  optimizer.optimize(10);

  // copy updated poses to pose_graph nodes
  for (size_t i=0; i < pose_graph.getNumNodes(); i++) {
    RobotPose& pose = pose_graph.getNode(i)->pose;
    SE2 opt_pose = ((VertexSE2*)optimizer.vertex(i))->estimate();
    pose.x = opt_pose[0];
    pose.y = opt_pose[1];
    pose.theta = opt_pose[2];
    //ROS_INFO("[GraphSLAM::optimize]\tupdated node %d: (%f,%f,%f)",i,pose.x,pose.y,pose.theta);
  }

  //ROS_INFO("[GraphSLAM::optimize]\texiting optimize");
}

void GraphSLAM::processLaserReading(const sensor_msgs::LaserScan::ConstPtr &msg) {
  // get odometry for this scan
  RobotPose curr_pose = lookupRobotPose(msg->header.stamp);
  laser_offset = lookupLaserOffset(msg->header.stamp);
  
  if (pose_graph.empty()) {
    // create the first node
    GraphNode *node = pose_graph.createNode();
    node->scan = *msg;
    node->pose = curr_pose;
    
    return;
  }
  
  GraphNode *last_node = pose_graph.getLastNode();
  
  // moved sufficiently?
  if (!robotHasMoved(last_node->pose, curr_pose)) {
    return;
  }
  
  
  GraphNode *new_node = pose_graph.createNode();
  new_node->pose = curr_pose;
  new_node->scan = *msg;

  vector<GraphNode *> ref_nodes;
  ref_nodes.push_back(last_node);
  scan_matcher->setReferenceNodes(ref_nodes,last_node);
  scan_matcher->scanMatch(new_node);
  RobotPose max_pose = scan_matcher->getMaxPose();
  
  tf::Transform fixed_to_odom;
  fixed_to_odom.setOrigin(tf::Vector3(last_node->pose.x, last_node->pose.y, 0));
  fixed_to_odom.setRotation(tf::createQuaternionFromYaw(last_node->pose.theta));
  
  
  tf::Transform max_to_fixed;
  max_to_fixed.setOrigin(tf::Vector3(max_pose.x, max_pose.y, 0));
  max_to_fixed.setRotation(tf::createQuaternionFromYaw(max_pose.theta));
  
  tf::Transform max_to_odom = fixed_to_odom * max_to_fixed;
  RobotPose pose_odom;
  pose_odom.x = max_to_odom.getOrigin().getX();
  pose_odom.y = max_to_odom.getOrigin().getY();
  pose_odom.theta = tf::getYaw(max_to_odom.getRotation());
 // new_node->pose = pose_odom;
 
  int start_index = max(0, new_node->idx - sequential_window);
  for (int i = start_index; i < new_node->idx; i++) {
    addLocalRegConstraints(new_node, i);
  }
  addGlobalRegConstraints(new_node);
  optimize();
}
