#ifndef POSE_GRID2_H
#define POSE_GRID2_h

#include "RobotPose.h"

class PoseGrid2 {
  public:
  
    PoseGrid2(int x_dim, int y_dim, int theta_dim);
    ~PoseGrid2();
    
    void setResolution(double xy_res, double theta_res);
    void setCenter(RobotPose pose);
    
    void setCellValue(int i, int j, int k, double value);
    double getCellValue(int i, int j, int k);
    RobotPose getCellPose(int i, int j, int k);
    
    RobotPose getMaxPose();
    RobotPose getMaxPoseIndex();
    double getMaxPoseLogScore();
    int getMaxThetaSlice();
    
  private:
    double *values;
    int x_dim, y_dim, theta_dim;
    double xy_res, theta_res;
    RobotPose center_pose;
    
    int dataIdx(int i, int j, int k);
};


#endif
