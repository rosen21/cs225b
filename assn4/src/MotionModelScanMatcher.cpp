#include "MotionModelScanMatcher.h"
#include <visualization_msgs/Marker.h>
#include <sensor_msgs/PointCloud.h>
#include "VizUtil.h"
#include "MathUtil.h"
#include "SensorModel.h"
#include <tf/transform_datatypes.h>
#include <cfloat>
#include <Eigen/Dense>

using namespace Eigen;

MotionModelScanMatcher::MotionModelScanMatcher(double _alpha1, double _alpha2, double _alpha3)
  : MotionModel(_alpha1, _alpha2, _alpha3) {
  cov = new double[9];

  // set occupancy grid dimensions
  occ_xy_grid_dim = 400;
  occ_x_dim = occ_xy_grid_dim;
  occ_y_dim = occ_xy_grid_dim;

  // set pose grid dimensions
  pose_theta_dim = 20;
  pose_x_dim = 50;
  pose_y_dim = 50;

  //set fine grain dimensions
  fine_pose_theta_dim = pose_theta_dim; //TODO - change?
  fine_pose_x_dim = 8;
  fine_pose_y_dim = 8;

  theta_step = 2 * M_PI / pose_theta_dim;
  xy_step = 0.04; // meters per grid cell (occupancy grid and pose grid have same resolution)
  x_step = xy_step;
  y_step = xy_step; // 2m divided by 100

  motion_model_fudge_factor = 10000;

}

MotionModelScanMatcher::~MotionModelScanMatcher() {
  delete[] cov;
}

void MotionModelScanMatcher::computeMotionModelGrid(PoseGrid &grid, int use_fine_grid) {
  int posegrid_x_dim = pose_x_dim;
  int posegrid_y_dim = pose_y_dim;
  if(use_fine_grid) posegrid_x_dim = fine_pose_x_dim;
  if(use_fine_grid) posegrid_y_dim = fine_pose_y_dim;

  RobotPose p(0,0,0);
  double px;
  for (int theta_ind = 0; theta_ind < pose_theta_dim; theta_ind++) {
    p.theta = theta_ind*theta_step;

    for (int x_ind = 0; x_ind < posegrid_x_dim; x_ind++) {
      p.x =  (pose_grid_offset_x + x_ind - occ_x_dim/2) * x_step;

      for (int y_ind = 0; y_ind < posegrid_y_dim; y_ind++) {
        p.y = (pose_grid_offset_y + y_ind - occ_y_dim/2) * y_step;

        px = getMotionProb(RobotPose(0,0,0), p);
        grid.setVal(theta_ind,y_ind,x_ind,px);
      }
    }
  }
}

void MotionModelScanMatcher::computeSensorModelGrid(PoseGrid &grid, SensorModel &sensor_model) {
  RobotPose p(0,0,0);
  double pz;
  for (int theta_ind = 0; theta_ind < pose_theta_dim; theta_ind++) {
    p.theta = theta_ind*theta_step;
    for (int x_ind = 0; x_ind < pose_x_dim; x_ind++) {
      p.x =  (pose_grid_offset_x + x_ind) * x_step;
      for (int y_ind = 0; y_ind < pose_y_dim; y_ind++) {
        p.y = (pose_grid_offset_y + y_ind) * y_step;
        pz = sensor_model.getScanProb(p);
        grid.setVal(theta_ind,y_ind,x_ind,pz);
            }
        }
    }  
}



// reference_scan_map has size twice that of scan.range_max with robot in the middle
void MotionModelScanMatcher::laserScanToOccupancyGrid(sensor_msgs::LaserScan reference_scan,
						      nav_msgs::OccupancyGrid& reference_scan_map) {
  ROS_INFO("[MotionModelScanMatcher::laserScanToOccupancyGrid]\tentering");
  ROS_INFO("[MotionModelScanMatcher::laserScanToOccupancyGrid]\tmax_range=%f, num_scans=%d",reference_scan.range_max, reference_scan.ranges.size());
  ROS_INFO("[MotionModelScanMatcher::laserScanToOccupancyGrid]\tocc_xy_grid_dim=%d",occ_xy_grid_dim);

  // this ensures that all of the scan points fall in the grid we have created
  reference_scan_map.info.resolution = xy_step;
  reference_scan_map.info.width = occ_x_dim;
  reference_scan_map.info.height = occ_y_dim;
  // init map to zeros
  for (int i = 0; i < occ_xy_grid_dim*occ_xy_grid_dim; i++) {reference_scan_map.data.push_back(0);}

  int num_scan_pts = reference_scan.ranges.size();
  // loop through scan range
  for (int i = 0; i < num_scan_pts; i++) {

      double distance = reference_scan.ranges[i];
      double angle = reference_scan.angle_min + i * reference_scan.angle_increment;
      // bounds check - disregard points outside of lasers range for now
      if (distance < reference_scan.range_min || distance >= reference_scan.range_max) {
          if (distance != reference_scan.range_max) {
              ROS_WARN("[MotionModelScanMatcher::laserScanToOccupancyGrid]\tfound invalid distance: d=%f, min_range=%f, max_range=%f",
                      distance,reference_scan.range_min,reference_scan.range_max);
          }
          continue;
      }

      // get coordinates of point in laser frame
      double x = distance * cos(angle);	
      double y = distance * sin(angle);   

      //get the closest point in the new map frame
      int x_grid = occ_xy_grid_dim/2 + floor(x/x_step + 0.5);
      int y_grid = occ_xy_grid_dim/2 + floor(y/y_step + 0.5);

      reference_scan_map.data[y_grid*occ_xy_grid_dim + x_grid] = 1;
      //ROS_INFO("[MotionModelScanMatcher::laserScanToOccupancyGrid]\tset map[%d,%d]=1",x_grid,y_grid);
  }
  //ROS_INFO("[MotionModelScanMatcher::laserScanToOccupancyGrid]\texiting");
}

/*
 * Helper method to make a occupancy grid out of multiple laser scans and pose offsets
 * The last pose (which has to be the most recent pose) 
 * will be taken as the center of the grid, and all others will be judged relative to the last pose
 */
void MotionModelScanMatcher::laserScansToOccupancyGrid(list<sensor_msgs::LaserScan> ref_scans,
						      list<RobotPose> ref_poses, nav_msgs::OccupancyGrid& reference_scan_map) {

  ROS_INFO("[MotionModelScanMatcher::laserScansToOccupancyGrid]\tentering");
  ROS_INFO("[MotionModelScanMatcher::laserScansToOccupancyGrid]\tocc_xy_grid_dim=%d",occ_xy_grid_dim);
  ROS_INFO("[MotionModelScanMatcher::laserScansToOccupancyGrid]\tnum scans=%d", ref_scans.size());

  // init ref scan map to have the right dimensions
  // this ensures that all of the scan points fall in the grid we have created
  reference_scan_map.info.resolution = xy_step;
  reference_scan_map.info.width = occ_x_dim;
  reference_scan_map.info.height = occ_y_dim;
  // init map to zeros
  for (int i = 0; i < occ_xy_grid_dim*occ_xy_grid_dim; i++) {reference_scan_map.data.push_back(0);}

  // init the base robot pose and transform
  RobotPose origin = ref_poses.back();
  tf::Transform base_to_origin;
  base_to_origin.setOrigin(tf::Vector3(origin.x, origin.y, 0));
  base_to_origin.setRotation(tf::createQuaternionFromYaw(origin.theta));
  base_to_origin = base_to_origin.inverse();

  /*
  //TODO _ TESTING _ REMOVE
  tf::Vector3 ta(0, 0, 0.0);
  tf::Vector3 resa = base_to_origin * ta;
  ROS_INFO("[MotionModelScanMatcher::laserScansToOccupancyGrid]\t ORIGIN TEST pose: (%f %f, %f)\n\n", resa.getX(), resa.getY(), resa.getZ());
  int xt_grid = occ_xy_grid_dim/2 + floor(resa.getX()/x_step + 0.5);
  int yt_grid = occ_xy_grid_dim/2 + floor(resa.getY()/y_step + 0.5);
  reference_scan_map.data[yt_grid*occ_xy_grid_dim + xt_grid] = 1;
  */

  int num_scans = ref_scans.size();

  //make sure that the scans/poses are valid
  assert(ref_scans.size() == ref_poses.size());

  for(int s = 0; s < num_scans; s ++){ //loop through poses/scans
    RobotPose cur_pose = ref_poses.back();
    RobotPose cur_offset = cur_pose - origin;
    //ROS_INFO("[MotionModelScanMatcher::laserScansToOccupancyGrid]\t Cur offset: (%f %f, %f)", cur_offset.x, cur_offset.y, cur_offset.theta);
    //ROS_INFO("[MotionModelScanMatcher::laserScansToOccupancyGrid]\t origin: (%f %f, %f)", origin.x, origin.y, origin.theta);
    //ROS_INFO("[MotionModelScanMatcher::laserScansToOccupancyGrid]\t Cur pose: (%f %f, %f)\n\n", cur_pose.x, cur_pose.y, cur_pose.theta);
    //make transform from this pose frame to the base frame
    tf::Transform pose_to_base;
    pose_to_base.setOrigin(tf::Vector3(cur_pose.x, cur_pose.y, 0));
    pose_to_base.setRotation(tf::createQuaternionFromYaw(cur_pose.theta));
    tf::Transform pose_to_origin = base_to_origin * pose_to_base;

    /*
    tf::Vector3 t(0, 0, 0.0);
    tf::Vector3 res = pose_to_base * t;
    tf::Vector3 res2 = base_to_origin * res;
    tf::Vector3 res3 = pose_to_origin * t;
    tf::Vector3 base_trans(origin.x, origin.y, 0.0);
    tf::Vector3 res4 = res - base_trans;
    ROS_INFO("[MotionModelScanMatcher::laserScansToOccupancyGrid]\t TEST pose: (%f %f, %f)\n\n", res.getX(), res.getY(), res.getZ());
    ROS_INFO("[MotionModelScanMatcher::laserScansToOccupancyGrid]\t TEST pose: (%f %f, %f)\n\n", res2.getX(), res2.getY(), res2.getZ());
    ROS_INFO("[MotionModelScanMatcher::laserScansToOccupancyGrid]\t TEST pose: (%f %f, %f)\n\n", res3.getX(), res3.getY(), res3.getZ());
    int x_gridt = occ_xy_grid_dim/2 + floor(res3.getX()/x_step + 0.5);
    int y_gridt = occ_xy_grid_dim/2 + floor(res3.getY()/y_step + 0.5);
    reference_scan_map.data[y_gridt*occ_xy_grid_dim + x_gridt] = 1;
    */

    sensor_msgs::LaserScan reference_scan = ref_scans.back();

    int num_scan_pts = reference_scan.ranges.size();
    // loop through scan range
    for (int i = 0; i < num_scan_pts; i++) {

      double distance = reference_scan.ranges[i];
      double angle = reference_scan.angle_min + i * reference_scan.angle_increment;

      // bounds check - disregard points outside of lasers range for now
      if (distance < reference_scan.range_min || distance >= reference_scan.range_max) {
        if (distance != reference_scan.range_max) {
          ROS_WARN("[MotionModelScanMatcher::laserScanToOccupancyGrid]\tfound invalid distance: d=%f, min_range=%f, max_range=%f",
              distance, reference_scan.range_min, reference_scan.range_max);
        }
        continue;
      }

      /*
      double x = distance * cos(angle + cur_pose.theta) + cur_offset.x;	
      double y = distance * sin(angle + cur_pose.theta) + cur_offset.y;   
      int x_grid = occ_xy_grid_dim/2 + floor(x/x_step + 0.5);
      int y_grid = occ_xy_grid_dim/2 + floor(y/y_step + 0.5);
      */

      //get coordinates of point in the current pose's frame
      double x = distance * cos(angle);	
      double y = distance * sin(angle);   
      tf::Vector3 p(x,y,0.0);

      //convert from the current pose's frame to the base frame, then from the base frame to origin frame
      tf::Vector3 tp = pose_to_origin * p;

      //get the closest point in the new map frame
      int x_grid = occ_xy_grid_dim/2 + floor(tp.getX()/x_step + 0.5);
      int y_grid = occ_xy_grid_dim/2 + floor(tp.getY()/y_step + 0.5);

      if (x_grid > reference_scan_map.info.width || x_grid < 0 || y_grid > reference_scan_map.info.height || y_grid < 0) {
	ROS_INFO("[MotionModelScanMatcher::laserScanToOccupancyGrid]\treference_scan_map.info.(width,height) = (%d,%d)",
		 reference_scan_map.info.width, reference_scan_map.info.height);
	ROS_INFO("[MotionModelScanMatcher::laserScanToOccupancyGrid]\tset map[%d,%d]=1",x_grid,y_grid);
	ROS_INFO("[MotionModelScanMatcher::laserScanToOccupancyGrid]\ttp.getX() = %f, tp.getY() = %f",tp.getX(), tp.getY());
	fflush(stderr);
      } else {
	reference_scan_map.data[y_grid*occ_xy_grid_dim + x_grid] = 1;
      }
    }

    //advance poses/scans
    ref_poses.pop_back();
    ref_scans.pop_back();
  }
  //ROS_INFO("[MotionModelScanMatcher::laserScanToOccupancyGrid]\texiting");
}
void MotionModelScanMatcher::visualizeOdomOffset() {
  RobotPose pose_grid_center = odom_offset.asPose();  

  // visualize pose grid center
  visualization_msgs::Marker marker;
  marker.header.frame_id = "/base_link"; // publish point in the base_link frame as distance from the 
  marker.header.stamp = ros::Time();
  marker.type = visualization_msgs::Marker::LINE_LIST;
  marker.action = visualization_msgs::Marker::ADD;
  marker.ns = "odom_offset";
  marker.id = 0;
  marker.pose.orientation.w = 1.0;
  marker.scale.x = 0.1;
  marker.scale.y = 0.1;
  marker.scale.z = .1;
  marker.color.a = 1.0;
  marker.color.r = 0.0;
  marker.color.g = 1.0;
  marker.color.b = 0.0;
  marker.lifetime = ros::Duration(0);
  geometry_msgs::Point p;
  p.x = 0.0;
  p.y = 0.0;
  p.z = 0.1;
  marker.points.push_back(p);
  p.x = pose_grid_center.x*x_step;
  p.y = pose_grid_center.y*y_step;
  p.z = 0.1;
  marker.points.push_back(p);
  marker_pub->publish(marker);
}

void MotionModelScanMatcher::visualizeMaxPose() {
  RobotPose pose_grid_center = odom_offset.asPose();  

  //get transform from max_pose to the origin of the map frame (which is 
  //the previous pose) and draw an arrow from the previous pose to base link.
  //if the arrow points in the right direction, our estimate is close
  tf::Transform max_pose_to_map_frame;
  max_pose_to_map_frame.setOrigin(tf::Vector3(max_pose.x, max_pose.y, 0));
  max_pose_to_map_frame.setRotation(tf::createQuaternionFromYaw(max_pose.theta));
  tf::Transform map_frame_to_max_pose = max_pose_to_map_frame.inverse();
  tf::Vector3 prev_pose(0.0, 0.0, 0.0);
  prev_pose = map_frame_to_max_pose * prev_pose;

  // visualize pose grid center
  visualization_msgs::Marker marker;
  //marker.header.frame_id = "/base_link";  
  marker.header.frame_id = "/laser";  
  marker.header.stamp = ros::Time();
  marker.type = visualization_msgs::Marker::ARROW;
  marker.action = visualization_msgs::Marker::ADD;
  marker.ns = "max_pose";
  marker.id = 0;
  marker.pose.orientation.w = 1.0;
  marker.scale.x = 0.1;
  marker.scale.y = 0.1;
  marker.scale.z = .1;
  marker.color.a = 1.0;
  marker.color.r = 0.0;
  marker.color.g = 1.0;
  marker.color.b = 1.0;
  marker.lifetime = ros::Duration(0);

  geometry_msgs::Point p;
  p.x = 0.0;
  p.y = 0.0;
  p.z = 0.1;
  marker.points.push_back(p);
  p.x = prev_pose.getX();
  p.y = prev_pose.getY();
  p.z = 0.1;
  marker.points.push_back(p);

  ROS_INFO("MotionModelScanMatcher::visualizeMaxPose - max pose = (%f %f) theta = %f", max_pose.x, max_pose.y, max_pose.theta);
  marker_pub->publish(marker);
}


void MotionModelScanMatcher::visualizeReferenceScanMap(nav_msgs::OccupancyGrid &reference_scan_map) {
  reference_scan_map.header.stamp = ros::Time::now();

  /* ----------------------old code to publish in base_link frame------------------
  reference_scan_map.header.frame_id = "/base_link";
  reference_scan_map.info.map_load_time = ros::Time::now();
  tf::Point origin(-reference_scan_map.info.resolution * reference_scan_map.info.width/2,
                   -reference_scan_map.info.resolution * reference_scan_map.info.height/2,
                   0.01);
  tf::pointTFToMsg(origin, reference_scan_map.info.origin.position);
  tf::quaternionTFToMsg(tf::createQuaternionFromYaw(0), reference_scan_map.info.origin.orientation);
  */

  //// new code to publish in base link frame assuming max pose is our best estimate of base link
  tf::Transform max_pose_to_map_frame;
  max_pose_to_map_frame.setOrigin(tf::Vector3(max_pose.x, max_pose.y, 0));
  max_pose_to_map_frame.setRotation(tf::createQuaternionFromYaw(max_pose.theta));
  tf::Transform map_frame_to_max_pose = max_pose_to_map_frame.inverse();

  //get transform from max pose (which we're assuming is base_link)
  //to the origin of the map, which is the previous reference frame
  tf::Vector3 map_origin(-x_step*occ_x_dim/2, -y_step*occ_y_dim/2, 0);
  map_origin = map_frame_to_max_pose * map_origin;

  //get angle of transform
  //double map_angle = map_frame_to_max_pose.getRotation().getAngle();
  double map_angle = -max_pose.theta;

  ROS_INFO("[MotionModelScanMatcher::visualizeReferenceScanMap] - map pose - %f %f %f", map_origin.getX(), map_origin.getY(), map_angle);

  //reference_scan_map.header.frame_id = "/base_link";
  reference_scan_map.header.frame_id = "/laser";
  reference_scan_map.info.map_load_time = ros::Time::now();
  tf::Point origin(map_origin.getX(), map_origin.getY(), 0.01);
  tf::pointTFToMsg(origin, reference_scan_map.info.origin.position);
  tf::quaternionTFToMsg(tf::createQuaternionFromYaw(map_angle), reference_scan_map.info.origin.orientation);

  occ_pub->publish(reference_scan_map);
}

//int fine is a flag to indicate which gridsize to use 
void MotionModelScanMatcher::visualizePoseGrid(PoseGrid &grid, ros::Time pub_time, int grid_sample_rate, int use_fine_grid) {
  int posegrid_x_dim = pose_x_dim;
  int posegrid_y_dim = pose_y_dim;
  if(use_fine_grid) posegrid_x_dim = fine_pose_x_dim;
  if(use_fine_grid) posegrid_y_dim = fine_pose_y_dim;

  //set up transform from the map frame to max pose (base link) frame
  tf::Transform max_pose_to_map_frame;
  max_pose_to_map_frame.setOrigin(tf::Vector3(max_pose.x, max_pose.y, 0));
  max_pose_to_map_frame.setRotation(tf::createQuaternionFromYaw(max_pose.theta));
  tf::Transform map_frame_to_max_pose = max_pose_to_map_frame.inverse();

  ROS_INFO("[visualizePoseGrid]\tentering");
  sensor_msgs::PointCloud cloud;
  cloud.header.stamp = pub_time;
  //cloud.header.frame_id = "/base_link";
  cloud.header.frame_id = "/laser";
  
  geometry_msgs::Point32 point;
  sensor_msgs::ChannelFloat32 channel;
  channel.name = "intensity";
  cloud.channels.push_back(channel);
  
  double grid_val;
  for (int x = 0; x < posegrid_x_dim; x += grid_sample_rate) {
    for (int y = 0; y < posegrid_y_dim; y += grid_sample_rate) {

      tf::Vector3 pose( (( -occ_x_dim/2 + pose_grid_offset_x + x) * x_step), (( -occ_y_dim/2 + pose_grid_offset_y + y) * y_step), 0.0);
      tf::Vector3 bl_pose = map_frame_to_max_pose * pose;

      point.x = bl_pose.getX();
      point.y = bl_pose.getY();
      point.z = 0.2;

      cloud.points.push_back(point);
      grid_val = grid.grid[0];
      for (int theta = 0; theta < pose_theta_dim; theta += grid_sample_rate) {
          if (grid.getVal(theta,y,x) > grid_val) {grid_val = grid.getVal(theta,y,x);}
      }
      //ROS_INFO("[MotionModelScanMatcher::visualizePoseGrid]\tmax(grid[%d,%d,:] = %f", x, y, grid_val);
      cloud.channels[0].values.push_back(grid_val);
    }
  }

  if(!use_fine_grid) pose_grid_pub->publish(cloud);
  else fine_pose_grid_pub->publish(cloud);
  // TODO also publish to pose_pub to show orientation
}


void MotionModelScanMatcher::setMultipleScans(list<sensor_msgs::LaserScan> ref_scans, 
		list<RobotPose> ref_poses, const sensor_msgs::LaserScan curr_scan){

  ROS_INFO("[MotionModelScanMatcher::setMultipleScans]\t entering"); 

  //make occupancy map
  nav_msgs::OccupancyGrid reference_scan_map;
  ROS_INFO("[MotionModelScanMatcher::setMultipleScans]\tocc_xy_grid_dim=%d",occ_xy_grid_dim);
  laserScansToOccupancyGrid(ref_scans, ref_poses, reference_scan_map);
  ROS_INFO("[MotionModelScanMatcher::setMultipleScans]\tnum scans=%d", ref_scans.size());
    
  //update scans
  updateModel(reference_scan_map, curr_scan);

  //visualize map with the new max pose estimate
  visualizeReferenceScanMap(reference_scan_map);
}

// we do all the heavy lifting from here, sampleMotion just uses instance varialbes cov and mu
void MotionModelScanMatcher::setScans(const sensor_msgs::LaserScan reference_scan, 
				      const sensor_msgs::LaserScan curr_scan) {
  //  check for bad ref scan
  if(reference_scan.ranges.size() <= 0){
    ROS_WARN("[MotionModelScanMatcher::setScans]\t bad ref scan! returning");
    return;
  }
  else{
    ROS_INFO("[MotionModel::MotionModelScanMatcher]\tentering setScans: reference_scan.ranges.size()=%d",reference_scan.ranges.size());
  }

  // construct a sensor model from reference_scan
  // first turn reference scan into occupancy grid for compatibility
  nav_msgs::OccupancyGrid reference_scan_map;
  ROS_INFO("[MotionModelScanMatcher::setScans]\tocc_xy_grid_dim=%d",occ_xy_grid_dim);
  laserScanToOccupancyGrid(reference_scan, reference_scan_map);
  visualizeReferenceScanMap(reference_scan_map);

  updateModel(reference_scan_map, curr_scan);
}

void MotionModelScanMatcher::updateModel(const nav_msgs::OccupancyGrid reference_scan_map, 
				      const sensor_msgs::LaserScan curr_scan) {

  //init sensor model
  SensorModel sensor_model(reference_scan_map);
  //sensor_model->visualizeLikelihoodMap(marker_pub,occ_xy_grid_dim, occ_xy_grid_dim,xy_grid_resolution);
  sensor_model.updateScan(curr_scan);
  ros::Time curr_time = ros::Time::now();

  // translation constants
  // these describe the translation of the pose_grid ORIGIN wrt to the occupancy map ORIGIN
  ROS_INFO("[MotionModelScanMatcher::updateModel]\todom_offset: trans=%f rot1=%f rot2=%f)",odom_offset.trans, odom_offset.rot1, odom_offset.rot2);
  RobotPose pose_grid_center = odom_offset.asPose();  
  ROS_INFO("[MotionModelScanMatcher::updateModel]\tpose_grid_center: (%f %f %f)",pose_grid_center.x, pose_grid_center.y,pose_grid_center.theta);

  //set grid center to be the odom offset
  pose_grid_offset_x = (pose_grid_center.x / x_step) + (occ_x_dim - pose_x_dim) / 2;
  pose_grid_offset_y = (pose_grid_center.y / y_step) + (occ_y_dim - pose_y_dim) / 2;
  ROS_INFO("[MotionModelScanMatcher::updateModel]\tpose_grid_offset_x=%f, pose_grid_offset_y=%f",pose_grid_offset_x, pose_grid_offset_y);

  // construct grid datastructure, init cells to 1.0
  PoseGrid sensor_model_grid(pose_theta_dim, pose_y_dim, pose_x_dim);
  for(int i = 0; i < sensor_model_grid.num_cells; i++) sensor_model_grid.grid[i] = -DBL_MAX;

  int grid_sample_rate = 4;
  int scan_sample_rate = 1; // these are actually 1/rate, i.e number of scans/pixels to skip
  sensor_model.calcGridProbs(&sensor_model_grid, pose_grid_offset_x, pose_grid_offset_y, grid_sample_rate, scan_sample_rate);
  //computeSensorModelGrid(sensor_model_grid,sensor_model);
  PoseGrid motion_model_grid(pose_theta_dim, pose_y_dim, pose_x_dim);
  computeMotionModelGrid(motion_model_grid);
  PoseGrid combined_grid(pose_theta_dim, pose_y_dim, pose_x_dim);
  
  double max_val = -DBL_MAX;
  bool use_motion_model = true;
  int max_ind = -1;
  double curr;
  for (int i = 0; i < combined_grid.num_cells; i++) {
    //ROS_INFO("[MotionModelScanMatcher::setScans]\tsensor_model:%f, motion_model:%f",sensor_model_grid.grid[i], motion_model_grid.grid[i]);

    if(sensor_model_grid.grid[i] == -DBL_MAX){
      combined_grid.grid[i] = -DBL_MAX;
      continue;
    } 
      
    //funny story - our motion model was seriously messed up, hence its returned probabilities were on the same magnitude
    //as our sensor model probs (~ -1000), when they shouldn't have gone below -10 or -15 in the first place. we were doing indexing wrong.
    //anyways, a fudge factor of about 100 fixes things
    //may want to lower it if the odom is really crappy
    if (use_motion_model) {
      curr = sensor_model_grid.grid[i] + motion_model_fudge_factor*motion_model_grid.grid[i];
    } else {
      curr = sensor_model_grid.grid[i];
    }
    if (curr == 0) {
      ROS_WARN("[MotionModelScanMatcher::updateModel]\tsetting grid[%d] = 0.0",i);
    }
    combined_grid.grid[i] = curr;
    if (curr > max_val) {
      max_val = curr;
      max_ind = i;
    }
  }
  
  ROS_INFO("[MotionModelScanMatcher::updateModel] - max prob for scan %f \t for motion %f", sensor_model_grid.grid[max_ind], motion_model_grid.grid[max_ind]);

  visualizePoseGrid(combined_grid, curr_time, grid_sample_rate);

  //Max pose will be in regular coordinates, as the offset from the center of the occupancy grid (NOT the pose grid)
  //, i.e. the base link frame
  max_pose_ind = combined_grid.getIndexPose(max_ind);
  max_pose.x = (pose_grid_offset_x + max_pose_ind.x - occ_x_dim/2) * x_step;
  max_pose.y = (pose_grid_offset_y + max_pose_ind.y - occ_y_dim/2) * y_step;
  max_pose.theta = max_pose_ind.theta * theta_step;

  // extract mode (mu) and covariance matrix
  //estimateCovariance(max_pose,combined_grid);

  //use fine grained scan
  bool use_fine_grain_grid = true;

  if(use_fine_grain_grid){

    //let max_pose be the new center of this new grid - use the old offset, just increase by maxpose - center of grid
    pose_grid_offset_x = pose_grid_offset_x + max_pose_ind.x - fine_pose_x_dim/2;
    pose_grid_offset_y = pose_grid_offset_y + max_pose_ind.y - fine_pose_y_dim/2;

    ROS_INFO("[MotionModelScanMatcher::updateModel]\tpose_grid_offset_x=%f, pose_grid_offset_y=%f",pose_grid_offset_x, pose_grid_offset_y);

    // construct grid datastructure, init cells to 1.0
    PoseGrid sensor_model_fine_grid(fine_pose_theta_dim, fine_pose_y_dim, fine_pose_x_dim);
    for(int i = 0; i < sensor_model_fine_grid.num_cells; i++) sensor_model_fine_grid.grid[i] = -DBL_MAX;

    //run sensor model fine grid with this
    sensor_model.calcGridProbs(&sensor_model_fine_grid, pose_grid_offset_x, pose_grid_offset_y, 1, 1);

    //do motion model again
    PoseGrid motion_model_fine_grid(fine_pose_theta_dim, fine_pose_y_dim, fine_pose_x_dim);
    computeMotionModelGrid(motion_model_fine_grid, 1);
    PoseGrid combined_fine_grid(fine_pose_theta_dim, fine_pose_y_dim, fine_pose_x_dim);

    max_val = -DBL_MAX;
    //max_val = sensor_model_grid.grid[0] + motion_model_grid.grid[0];

    use_motion_model = false;
    max_ind = -1;
    //double curr;
    for (int i = 0; i < combined_fine_grid.num_cells; i++) {
      //ROS_INFO("[MotionModelScanMatcher::setScans]\tsensor_model:%f, motion_model:%f",sensor_model_grid.grid[i], motion_model_grid.grid[i]);

      if(sensor_model_fine_grid.grid[i] == -DBL_MAX){
        combined_fine_grid.grid[i] = -DBL_MAX;
        continue;
      } 

      if (use_motion_model) {
        curr = sensor_model_fine_grid.grid[i] + motion_model_fudge_factor * motion_model_fine_grid.grid[i];
      } else {
        curr = sensor_model_fine_grid.grid[i];
      }
      if (curr == 0) {
        ROS_WARN("[MotionModelScanMatcher::updateModel]\tsetting grid[%d] = 0.0",i);
      }
      combined_fine_grid.grid[i] = curr;
      if (curr > max_val) {
        max_val = curr;
        max_ind = i;
      }
    }

    visualizePoseGrid(combined_fine_grid, curr_time, 1, 1);

    max_pose_ind = combined_fine_grid.getIndexPose(max_ind);
    max_pose.x = (pose_grid_offset_x + max_pose_ind.x - occ_x_dim/2) * x_step;
    max_pose.y = (pose_grid_offset_y + max_pose_ind.y - occ_y_dim/2) * y_step;
    max_pose.theta = max_pose_ind.theta * theta_step;

    //estimate new covariance
    estimateCovariance(max_pose,combined_fine_grid, 1);
  }

  //ROS_INFO("[MotionModelScanMatcher::setScans]\tmax_ind : %d, max_pose : (%f,%f,%f)",max_ind, max_pose.x,max_pose.y,max_pose.theta);
  //visualizeOdomOffset();
  visualizeMaxPose();
}

void MotionModelScanMatcher::estimateCovariance(RobotPose max_pose, PoseGrid& grid, int use_fine_grid){

  int posegrid_x_dim = pose_x_dim;
  int posegrid_y_dim = pose_y_dim;
  if(use_fine_grid) posegrid_x_dim = fine_pose_x_dim;
  if(use_fine_grid) posegrid_y_dim = fine_pose_y_dim;

  Matrix3d cov_temp;
  Matrix3d K = Matrix3d::Constant(0.0);
  Vector3d u(0.0, 0.0, 0.0);

  // first normalize grid
  double sum = 0;
  double offset = grid.getVal(max_pose_ind.theta, max_pose_ind.y, max_pose_ind.x) - 25;
  double phat;
  double logp;

  RobotPose p, norm_p;
  for (int theta_ind = 0; theta_ind < pose_theta_dim; theta_ind++) {
    p.theta = theta_ind*theta_step;
    for (int x_ind = 0; x_ind < posegrid_x_dim; x_ind++) {
      p.x = (x_ind + pose_grid_offset_x - occ_x_dim/2)*x_step;
      for (int y_ind = 0; y_ind < posegrid_y_dim; y_ind++) {
        p.y = (y_ind + pose_grid_offset_y - occ_x_dim/2)*y_step;
        //p is now in the same frame as max_pose

        logp = grid.getVal(theta_ind, y_ind, x_ind);

        if(logp == -DBL_MAX){
          continue;
        }
        if(sum <= -DBL_MAX/2 || sum > DBL_MAX/2){
          ROS_WARN("[MotionModelScanMatcher::estimateCovariance] - SUM NEAR OVERFLOW!!!!!!!!!!!");
        }

        //re-scale the log prob to an actual prob
        //ROS_INFO("[MotionModelScanMatcher::estimateCovariance]\tlogp=%f",logp);
        phat = exp(logp - offset); // legit fudge but overflows still
        //phat = exp((logp / 10) + offset); // fudge

        //calc running sums
        Vector3d pose = p.toEigenVector();
        Matrix3d plus_k = phat * (pose * pose.transpose()); 
        K = K + plus_k;
        u = u + (pose * phat);
        sum += phat;
        /*
        if(phat != 0){
          ROS_INFO("[MotionModelScanMatcher::estimateCovariance] - phat = %f,\t logp = %f, offset = %f", phat, logp, offset);
          ROS_INFO("[MotionModelScanMatcher::estimateCovariance] - u = [%f %f %f]", u(0,0), u(1,0), u(2,0));
        }
        */

      }
    }
  }

  cov_temp = K / sum + (u * u.transpose())/(pow(sum,2));

  //move cov_temp's values to cov
  for(int i = 0; i < 9; i ++){
    int x_ind = i % 3;
    int y_ind = i / 3;
    cov[i] = cov_temp(x_ind, y_ind);
  }

  ROS_INFO("[MotionModelScanMatcher::estimateCovariance]\tSigma=[%e\t%e\t%e]",cov[0],cov[1],cov[2]);
  ROS_INFO("                                            \t      [%e\t%e\t%e]",cov[3],cov[4],cov[5]);
  ROS_INFO("                                            \t      [%e\t%e\t%e]",cov[6],cov[7],cov[8]);
}

RobotPose MotionModelScanMatcher::sampleMotion(RobotPose pose) {
  //max pose is offset in regular meter-scaled coordinates from the origin 
  //(which is taken to be the prev_odom/most recent pose given)
  RobotPose sampled_pose(pose.x + max_pose.x, pose.y + max_pose.y, pose.theta + max_pose.theta);

  //sampled_pose.x += getGaussianRand(0.0, cov[0]);
  //sampled_pose.y += getGaussianRand(0.0, cov[4]);
  //sampled_pose.theta += getGaussianRand(0.0, cov[8]);
  return sampled_pose;
}

void MotionModelScanMatcher::getOffset(RobotPose& _mean, double* _cov) {
  _mean.setPose(max_pose);
  for (int i=0; i < 9; i++) {
    _cov[i] = cov[i];
  }
}
