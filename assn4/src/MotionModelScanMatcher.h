#ifndef _MOTION_MODEL_SCAN_MATCHER_H
#define _MOTION_MODEL_SCAN_MATCHER_H

#include "MotionModel.h"
#include <sensor_msgs/LaserScan.h>
#include "nav_msgs/OccupancyGrid.h"
#include "RobotPose.h"
#include "SensorModel.h"
#include "PoseGrid.h"
#include <ros/ros.h>

class MotionModelScanMatcher : public MotionModel {

 public:
  MotionModelScanMatcher() {};
  MotionModelScanMatcher(double _alpha1, double _alpha2, double _alpha3);
  ~MotionModelScanMatcher();
  virtual void setScans(const sensor_msgs::LaserScan reference_scan, 
		const sensor_msgs::LaserScan curr_scan);
  virtual void setMultipleScans(list<sensor_msgs::LaserScan> ref_scans, 
		list<RobotPose> ref_poses, const sensor_msgs::LaserScan curr_scan);
  virtual RobotPose sampleMotion(RobotPose pose);
  void getOffset(RobotPose& _mean, double* _cov);

 private:

  // estimated motion model
  double* cov;
  RobotPose max_pose;
  RobotPose max_pose_ind;

  // occupancy grid dimensions
  int occ_xy_grid_dim;
  int occ_x_dim;
  int occ_y_dim;

  // pose grid dimensions
  int pose_theta_dim;
  int pose_x_dim;
  int pose_y_dim;

  // fine pose grid dimensions
  int fine_pose_theta_dim;
  int fine_pose_x_dim;
  int fine_pose_y_dim;

  double theta_step;
  double xy_step;
  double x_step;
  double y_step;

  double motion_model_fudge_factor;

  // tranlation constants
  double pose_grid_offset_x;
  double pose_grid_offset_y;

  //visualization methods
  void visualizeOdomOffset();
  void visualizeMaxPose();
  void visualizeReferenceScanMap(nav_msgs::OccupancyGrid &reference_scan_map);
  void visualizePoseGrid(PoseGrid &grid, ros::Time pub_time=ros::Time::now(), int grid_sample_rate=1, int use_fine_grid=0);

  // store motion model grid because it doesn't change
  PoseGrid motion_model_grid;
  void computeMotionModelGrid(PoseGrid &grid, int use_fine_grid=0);
  void computeSensorModelGrid(PoseGrid &sensor_mode_grid, SensorModel &sensor_model);
  void laserScanToOccupancyGrid(sensor_msgs::LaserScan reference_scan, nav_msgs::OccupancyGrid& reference_scan_map);
  void laserScansToOccupancyGrid(list<sensor_msgs::LaserScan> ref_scans,
      list<RobotPose> ref_poses, nav_msgs::OccupancyGrid& reference_scan_map);
  void updateModel(const nav_msgs::OccupancyGrid reference_scan_map, 
				      const sensor_msgs::LaserScan curr_scan);
  void estimateCovariance(RobotPose max_pose, PoseGrid &grid, int use_fine_grid=0);
};

#endif

