#ifndef SENSOR_MODEL_H
#define SENSOR_MODEL_H

#include "nav_msgs/OccupancyGrid.h"
#include <sensor_msgs/LaserScan.h>
#include <visualization_msgs/Marker.h>
#include <ros/ros.h>
#include "RobotPose.h"
#include "PoseGrid.h"

using namespace std;

class SensorModel {
 private:
  int LIKELIHOOD_MAP_MAX_DIST;
  double LIKELIHOOD_MAP_STD_DEV;
  int MAP_SIZE_X;
  int MAP_SIZE_Y;
  int NUM_CELLS;
  double zhit;
  double zrandom;
  double zmax;
  double MAP_RESOLUTION;
  double* likelihood_map;  
  ros::Publisher marker_pub;
  sensor_msgs::LaserScan cur_scan;
  
  int max_scan_pts;
  double laser_offset_x;
  double laser_offset_y;

  template <class T>
    T getVal(T* cost_map, int i,int j) {return cost_map[MAP_SIZE_X*j + i];};
  
  template <class T>
  void setVal(T* cost_map, int i,int j, T val) {cost_map[MAP_SIZE_X*j + i] = val;}
  
  void computeDecay(double* obstacle_dist_map, double std_dev, double* cost_map);
  void updateMapDijkstra(double* map, bool* init_set_map, double edge_cost, int max_dist);
  void getInitSet(bool* init_set_map, nav_msgs::OccupancyGrid map);
  void buildLikelihoodMap(nav_msgs::OccupancyGrid msg, double* likelihood_map);
 public:
  void visualizeLikelihoodMap(ros::Publisher map_pub);
  void visualizeScanLikelihood(ros::Publisher marker_pub, int map_size_x, int map_size_y, double map_resolution);
  double getScanProb(RobotPose p);
  void updateScan(const sensor_msgs::LaserScan::ConstPtr& msg);
  void updateScan(const sensor_msgs::LaserScan msg);
  void calcGridProbs(PoseGrid* grid, double xoff, double yoff, int grid_sample_rate=1, int scan_sample_rate=1);
  SensorModel(nav_msgs::OccupancyGrid msg);
  ~SensorModel() {delete[] likelihood_map;};
};

#endif
