#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <vector>
#include <tf/transform_datatypes.h>
#include <visualization_msgs/MarkerArray.h>
#include <visualization_msgs/Marker.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/LaserScan.h>
#include "tfhelper.h"
#include "assn2/Point2d.h"
#include "assn2/Path.h"
#include "boost/thread/mutex.hpp"

typedef std::pair<double, double> RobotCmd;

typedef struct {
	double v;
	double w;
	tf::Vector3 position;
	double heading;
	int pathIdx;
	double headingError;
	double pathError;
	double navPotential;
	double obstacleDistance;
} RobotState;

class TurtlebotController {

	public:
		TFHelper *tfHelper;
		TurtlebotController();
		void setHelper(TFHelper *helper);
		void setGoal(tf::Pose goalPose);
		void setCmd(RobotCmd cmd);
		RobotCmd nextTimestep();
		void updateRobotState();
		
		// message callbacks
		void updateScan(const sensor_msgs::LaserScan::ConstPtr& msg);
		void updatePath();
		void updatePathMsg(const assn2::Path::ConstPtr& msg);
		std::vector<double> intersectCircles(double r1, tf::Vector3 diff, double r2);
		visualization_msgs::Marker getBestTrajectory();
		visualization_msgs::Marker getPath();
		visualization_msgs::Marker getTrajectories();
		visualization_msgs::Marker getLocalGoal();
		visualization_msgs::Marker getLocalPath();
		visualization_msgs::Marker getGoalViz();
	private:
		
		// internal state
		std::vector<tf::Vector3> scanPts;
		std::vector<tf::Vector3> pathPts;
		std::vector<tf::Vector3> localGoalPts;
		boost::mutex update_mutex;
		
		tf::Pose goalPose;
		tf::Pose robotPose;
		tf::Pose initialPose;
		
		RobotState robot;
		double optV, optW;
		
		// configurable parameters
		double rolloutTime, targetTime;
		
		double vMin, vMax;
		double wMin, wMax;
		double vAccel, wAccel;
		double rotateInPlaceThresh;
		double vStepSize, wStepSize;
		double obstacleDeviation;
		double resolution;
		double horizon;
		
		double robotRadius;
		double goalThreshold;
		double collisionMargin;
		double decayHalfLife;
		bool goingUp, goingDown;
		bool escaping;
		int escapeSgn;
		int followCount;
		
		// weights
		double potentialWeight;
		double pathDistanceWeight;
		double headingWeight;
		double velocityWeight;
		double obstacleWeight;
		
		void updateLocalGoal();
		bool lineOfSight(tf::Vector3 pt1, tf::Vector3 pt2);
		
		// evaluation
		double evaluate(double v, double w, double time);
		RobotState rollout(double v, double w, double time);
		double getObstacleDistance(double v, double w, double time);
		tf::Vector3 getTrajectoryPosition(double v, double w, double time);
		double getTrajectoryHeading(double v, double w, double time);
		double getDistanceToObstacle(tf::Vector3 position);
		
		// path methods
		int getNearestPathPoint(tf::Vector3 position);
		double getPathPotential(int idx);
		double getPathHeading(tf::Vector3 position);
		
		// utility
		tf::Vector3 unitVector(double yaw);
		double distanceToSegment(tf::Vector3 point, tf::Vector3 p1, tf::Vector3 p2);
};

#endif
