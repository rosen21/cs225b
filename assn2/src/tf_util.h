#include <ros/ros.h>
#include <math.h>

#include <geometry_msgs/PoseStamped.h>
#include <tf/transform_listener.h>
#include <tf/transform_broadcaster.h>

// file of tf related utilities

namespace tf_util {

  void printTransform(tf::Transform tform, std::string name) {
    ROS_INFO("[print_transform]\tNAME:\t%s",name.c_str());
    double yaw, pitch, roll;
    tform.getBasis().getRPY(roll, pitch, yaw);
    tf::Quaternion q = tform.getRotation();
    tf::Vector3 v = tform.getOrigin();
    ROS_INFO("\t\tTranslation: [%f %f %f]",v.getX(),v.getY(),v.getZ());
    ROS_INFO("\t\tRotation: in Quaternion [%f %f %f %f]",q.getX(),q.getY(),q.getZ(),q.getW());
    ROS_INFO("\t\t          in RPY        [%f %f %f]",roll,pitch,yaw); 
  }
  
  void initTransform(tf::Transform tranform) {
    tranform.setOrigin(tf::Vector3(0.0, 0.0, 0.0));
    tranform.setRotation(tf::Quaternion(0.0,0.0,0.0,1.0));
  }

  void getTransform(const tf::TransformListener &listener, std::string from_frame, std::string to_frame, tf::StampedTransform transform) {
    try{
      ROS_DEBUG("[getTransform]\tgrabbing transform between /map and /base_link");
      listener.lookupTransform(from_frame,to_frame,ros::Time(0), transform);
    }
    catch (tf::TransformException ex){
      ROS_ERROR("[getTransform]\tTrying to get %s / %s_link transform. Error: %s",from_frame.c_str(),to_frame.c_str(),ex.what());
    }
  }

}

