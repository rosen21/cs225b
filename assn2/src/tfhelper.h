#ifndef TF_HELPER_H
#define TF_HELPER_H

#include <tf/transform_listener.h>
#include <tf/transform_datatypes.h>

class TFHelper {

	public:
		tf::TransformListener *listener;
		void setListener(tf::TransformListener *listener);
		tf::Vector3 transformPoint(tf::Vector3 pt, std::string fixed_frame, std::string target_frame);
		tf::Pose transformPose(tf::Pose pt, std::string fixed_frame, std::string target_frame);
		tf::Pose transformOrigin(std::string fixedFrame, std::string targetFrame);
		tf::Pose extractPose(const geometry_msgs::PoseStamped::ConstPtr &msg);
};

#endif
