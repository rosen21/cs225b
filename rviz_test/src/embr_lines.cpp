/*
 * Copyright (c) 2010, Willow Garage, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Willow Garage, Inc. nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <ros/ros.h>
#include <visualization_msgs/Marker.h>

#include <string>
using std::string;

#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>
#include <cmath>

int main( int argc, char** argv )
{
  ros::init(argc, argv, "points_and_lines");
  ros::NodeHandle nh;
  ros::Publisher marker_pub = nh.advertise<visualization_msgs::Marker>("visualization_marker", 10);
  tf::TransformListener listener(nh);

  string from_frame_id = "new_frame";
  string to_frame_id = "base_link";
  
  ros::Rate r(30);
  while (ros::ok())
    {
      visualization_msgs::Marker line_strip;
      line_strip.header.frame_id = to_frame_id;
      // this is the frame in w/r/t which the line will be drawn
      line_strip.header.stamp = ros::Time::now();
      line_strip.ns = "points_and_lines";
      line_strip.action = visualization_msgs::Marker::ADD;
      line_strip.pose.orientation.w = 1.0;

      line_strip.id = 1;
      line_strip.type = visualization_msgs::Marker::LINE_STRIP;
      line_strip.scale.x = 0.1; // adjusts line width

      // Line strip is blue
      line_strip.color.b = 1.0;
      line_strip.color.a = 1.0;

      // add base_link point
      geometry_msgs::Point from_point;
      from_point.x = 0;
      from_point.y = 0;
      from_point.z = 0;
      line_strip.points.push_back(from_point);

      // do transform
      geometry_msgs::PointStamped stamped_from_point;
      //stamped_from_point.header.stamp = ros::Time::now();
      stamped_from_point.header.frame_id = from_frame_id;
      stamped_from_point.point.x = 0;
      stamped_from_point.point.y = 0;
      stamped_from_point.point.z = 0;

      geometry_msgs::PointStamped stamped_to_point;
      //stamped_to_point.header.stamp = ros::Time::now();
      stamped_to_point.header.frame_id = to_frame_id;

      try{
	listener.transformPoint(to_frame_id, stamped_from_point, stamped_to_point);
      }
      catch (tf::TransformException ex){
	ROS_ERROR("Attempting Transform.  Error:\t%s",ex.what());
      }

      // construct new geometry point for rviz
      geometry_msgs::Point new_frame_p;
      ROS_INFO("old x=%f, new x=%f", stamped_from_point.point.x,stamped_to_point.point.x);
      ROS_INFO("old y=%f, new y=%f", stamped_from_point.point.y,stamped_to_point.point.y);
      ROS_INFO("old z=%f, new z=%f", stamped_from_point.point.z,stamped_to_point.point.z);

      // add new_frame point
      geometry_msgs::Point to_point = stamped_to_point.point;
      line_strip.points.push_back(to_point);

      marker_pub.publish(line_strip);

      r.sleep();
    }
}
