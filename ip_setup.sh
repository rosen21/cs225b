# source this file in your .bashrc and it should grab your ip
ip=`ip addr show wlan0 | perl -n -e '/inet ([\d\.]+)/ && print $1'`
echo $ip
export ROS_HOSTNAME=$ip
#export $ROS_HOSTNAME
