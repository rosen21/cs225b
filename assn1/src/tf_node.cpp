// team name: zen masters
// team members: Joe Marrama, Karl Uhlig, Evan Rosen
// project 1.2

#include <ros/ros.h>
#include <tf/transform_listener.h>

#include <ros/ros.h>
#include <tf/transform_broadcaster.h>

#include <signal.h>
#include <termios.h>
#include <stdio.h>
#include "boost/thread/mutex.hpp"
#include "boost/thread/thread.hpp"

#include <visualization_msgs/Marker.h>
#include <string>
using std::string;
#include <tf/transform_listener.h>

#define KEYCODE_M 0x6D
#define KEYCODE_T 0x74
#define KEYCODE_Q 0x71

class keyboard_watcher {
public:
  void keywatcher();
};

int kfd = 0;
struct termios cooked, raw;
boost::mutex update_mutex;
bool update = true;

std::string turtle_name;

void draw_line(tf::TransformListener& listener,ros::Publisher& marker_pub,std::string& from_frame_id,std::string& to_frame_id) {
  visualization_msgs::Marker line_strip;
  line_strip.header.frame_id = to_frame_id;
  // this is the frame in w/r/t which the line will be drawn
  line_strip.header.stamp = ros::Time::now();
  line_strip.ns = "points_and_lines";
  line_strip.action = visualization_msgs::Marker::ADD;
  line_strip.pose.orientation.w = 1.0;
	line_strip.lifetime = ros::Duration(0.1);
  
  line_strip.id = 1;
  line_strip.type = visualization_msgs::Marker::LINE_STRIP;
  line_strip.scale.x = 0.1; // adjusts line width
  
  // Line strip is blue
  line_strip.color.b = 1.0;
  line_strip.color.a = 1.0;
  
  // add base_link point
  geometry_msgs::Point from_point;
  from_point.x = 0;
  from_point.y = 0;
  from_point.z = 0;
  line_strip.points.push_back(from_point);
  
  // do transform
  geometry_msgs::PointStamped stamped_from_point;
  //stamped_from_point.header.stamp = ros::Time::now();
  stamped_from_point.header.frame_id = from_frame_id;
  stamped_from_point.point.x = 0;
  stamped_from_point.point.y = 0;
  stamped_from_point.point.z = 0;
  
  geometry_msgs::PointStamped stamped_to_point;
  //stamped_to_point.header.stamp = ros::Time::now();
  stamped_to_point.header.frame_id = to_frame_id;
  
  try{
    listener.transformPoint(to_frame_id, stamped_from_point, stamped_to_point);
  }
  catch (tf::TransformException ex){
    ROS_ERROR("Attempting Transform.  Error:\t%s",ex.what());
  }
  
  // construct new geometry point for rviz
  geometry_msgs::Point new_frame_p;
  //ROS_INFO("old x=%f, new x=%f", stamped_from_point.point.x,stamped_to_point.point.x);
  //ROS_INFO("old y=%f, new y=%f", stamped_from_point.point.y,stamped_to_point.point.y);
  //ROS_INFO("old z=%f, new z=%f", stamped_from_point.point.z,stamped_to_point.point.z);
  
  // add new_frame point
  geometry_msgs::Point to_point = stamped_to_point.point;
  line_strip.points.push_back(to_point);
  
  marker_pub.publish(line_strip);
}

int main(int argc, char** argv){
  // initialize node
  ros::init(argc, argv, "assn1_pt1_broadcaster");
  ros::NodeHandle nh;
  keyboard_watcher kw;
  
  // set up listener/broadcaster/transform
  tf::TransformListener listener;
  tf::StampedTransform current_transform;
  static tf::TransformBroadcaster br;  
  tf::StampedTransform saved_transform;
  ros::Publisher marker_pub = nh.advertise<visualization_msgs::Marker>("visualization_marker", 10);

  //set up keyboard listener
  boost::thread keyboard_thread(boost::bind(&keyboard_watcher::keywatcher, &kw));
  
  ros::Rate rate(30.0);

	//set this to true to update the first time
	bool localup = true;

  while(nh.ok()) {
    //if we should update the current transform
		//this will only be true on the first iteration or after 't' has been pushed
    if (localup) {
			// get current transform
		  try{
		    listener.lookupTransform("/odom","/base_link",ros::Time(0), current_transform);
		  }
		  catch (tf::TransformException ex){
		    ROS_ERROR("%s",ex.what());
		  }    

      ROS_INFO("updating frame.....");
      saved_transform.setData(current_transform);
      saved_transform.frame_id_ = "/odom";
      saved_transform.child_frame_id_ = "/new_frame";
    }
    
    // update timestamp and broadcast transform
    saved_transform.stamp_ = ros::Time::now();
    br.sendTransform(saved_transform);

    // broadcast message to draw line
    string from_frame_id = "/new_frame";
    string to_frame_id = "/base_link";
    draw_line(listener,marker_pub,from_frame_id,to_frame_id);

		rate.sleep();
    
    // figure out whether to update on the next iteration or not
    boost::mutex::scoped_lock lock(update_mutex);
    localup = update;
    if(update) {update = false;} 
  }
  
  //stop keyboard thread
  keyboard_thread.interrupt();
  keyboard_thread.join();
  
  return 0;
}

//thread that watches keystrokes
void keyboard_watcher::keywatcher() {
  //set up console IO
  char c;
  
  // get the console in raw mode                           
  tcgetattr(kfd, &cooked);
  memcpy(&raw, &cooked, sizeof(struct termios));
  raw.c_lflag &=~ (ICANON | ECHO);
  // Setting a new line, then end of file                         
  raw.c_cc[VEOL] = 1;
  raw.c_cc[VEOF] = 2;
  tcsetattr(kfd, TCSANOW, &raw);
  
  ROS_INFO("Reading from keyboard");
  ROS_INFO("---------------------------");
  ROS_INFO("press t to update the frame");
  ROS_INFO("Press q and then ctrl+c to quit.");
  
  bool quit = false;
  
  while (ros::ok() && !quit)
    {
      // get the next event from the keyboard
      // (the call to read blocks)  
      if(read(kfd, &c, 1) < 0)
	{
	  perror("read():");
	  exit(-1);
	}
      
      ROS_DEBUG("value: 0x%02X\n", c);
      
      if(c == KEYCODE_T){
	ROS_DEBUG("t was pressed\n");
	
	//get the publishing lock
	boost::mutex::scoped_lock lock(update_mutex);
	//now, set the update variable to be true
	update = true;
	ROS_DEBUG("update was set to true");
      }
      else if(c == KEYCODE_Q){
	ROS_INFO("stopping the keyboard thread");
	quit = true;
      }		
    }
  
  //if we quit give up the terminal io so we don't mess it up
  tcsetattr(kfd, TCSANOW, &cooked);
  
  return;
}

