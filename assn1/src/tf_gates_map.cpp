// team name: zen masters
// team members: Joe Marrama, Karl Uhlig, Evan Rosen
// project 1.4

#include <ros/ros.h>
#include <math.h>
#include "boost/thread/mutex.hpp"

#include <sensor_msgs/LaserScan.h>
#include <visualization_msgs/Marker.h>

#include <geometry_msgs/PoseStamped.h>
#include <tf/transform_listener.h>
#include <tf/transform_broadcaster.h>

using namespace std;

// the global message which we publish
boost::mutex update_mutex;
bool update = false;
geometry_msgs::PoseStamped::ConstPtr goal_pose_in_map_frame;

void print_transform(tf::Transform tform, std::string name) {
  ROS_INFO("[print_transform]\tNAME:\t%s",name.c_str());
  double yaw, pitch, roll;
  tform.getBasis().getRPY(roll, pitch, yaw);
  tf::Quaternion q = tform.getRotation();
  tf::Vector3 v = tform.getOrigin();
  ROS_INFO("\t\tTranslation: [%f %f %f]",v.getX(),v.getY(),v.getZ());
  ROS_INFO("\t\tRotation: in Quaternion [%f %f %f %f]",q.getX(),q.getY(),q.getZ(),q.getW());
  ROS_INFO("\t\t          in RPY        [%f %f %f]",roll,pitch,yaw); 
}

tf::StampedTransform covert_pose_msg_to_transform(geometry_msgs::PoseStamped::ConstPtr pose_msg) {
  tf::StampedTransform transform;
  transform.setOrigin( tf::Vector3(pose_msg->pose.position.x, 
				   pose_msg->pose.position.y,
				   pose_msg->pose.position.z) );
  transform.setRotation( tf::Quaternion(
				   pose_msg->pose.orientation.x, 
				   pose_msg->pose.orientation.y, 
				   pose_msg->pose.orientation.z, 
				   pose_msg->pose.orientation.w) );
  return transform;
}

void goalCallback(const geometry_msgs::PoseStamped::ConstPtr &msg) {
  ROS_INFO("[goal_callback]\tmsg.header.frame_id: %s", msg->header.frame_id.c_str());
  boost::mutex::scoped_lock lock(update_mutex);
  goal_pose_in_map_frame = msg;
  update = true;
}

int main(int argc, char** argv) {
  // init node
  ros::init(argc, argv, "tf_node_gates2");
  ros::NodeHandle nh;

  // set up broadcaster/listener
  static tf::TransformBroadcaster br; 
  tf::TransformListener listener;

  // initialize map_odom_transform
  tf::StampedTransform map_odom_transform;
  map_odom_transform.setOrigin(tf::Vector3(0.0, 0.0, 0.0));
  map_odom_transform.setRotation(tf::Quaternion(0.0,0.0,0.0,1.0));

  // setup while loop and callback
  ros::Subscriber sub = nh.subscribe("goal", 1000, goalCallback);
  ros::Rate rate(30.0);
  while (ros::ok()) {    
    // get any publications from the goal topic and store the pose
    ros::spinOnce(); // populates the map_odom_transform

    // check if callback updated goal pose
    boost::mutex::scoped_lock lock(update_mutex);
    if (update) {
      update = false;
      
      // grab lates transform between /odom and /base_link
      tf::StampedTransform odom_base_link_transform;
      try{
	ROS_INFO("[get_transform]\tgrabbing transform between /odom and /base_link");
	listener.lookupTransform("/odom","/base_link",ros::Time(0), odom_base_link_transform);
      }
      catch (tf::TransformException ex){
	ROS_ERROR("[get_transform]\tTrying to get odom/base_link transform. Error: %s",ex.what());
      }

      // create transform from goal pose message
      tf::StampedTransform map_goal_transform = covert_pose_msg_to_transform(goal_pose_in_map_frame);

      // multiply together to get map / odom transform
      tf::StampedTransform new_map_odom_transform;
      map_odom_transform.setData(map_goal_transform * odom_base_link_transform.inverse());
      
      ROS_INFO("[get_transform]\tafter multiplication:");
      print_transform(map_goal_transform, "map_goal_transform");
      print_transform(odom_base_link_transform,"odom_base_link_transform");
      print_transform(map_odom_transform,"map_odom_transform");

      //exit(0);
    }
        
    // publish a new map / odom transform
    map_odom_transform.frame_id_ = "/map";
    map_odom_transform.child_frame_id_ = "/odom";
    
    // update timestamp and broadcast transform
    map_odom_transform.stamp_ = ros::Time::now();
    ROS_INFO("[tf_gates_map2::main]\tbroadcasting map_odom_transform:");
    print_transform(map_odom_transform,"map_odom_transform");
    br.sendTransform(map_odom_transform);

    rate.sleep();
  }
}
