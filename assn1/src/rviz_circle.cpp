// team name: zen masters
// team members: Joe Marrama, Karl Uhlig, Evan Rosen
// project 1.3

#include <ros/ros.h>
#include <math.h>
#include "boost/thread/mutex.hpp"

#include <sensor_msgs/LaserScan.h>
#include <visualization_msgs/Marker.h>

using namespace std;

float gMinRange = 0;
float gMinAngle = 0;
boost::mutex ra_lock;

/* Called when receiving a new laser scan message */
void scanCallback(const sensor_msgs::LaserScan::ConstPtr& msg) {
	bool started = false;
	float minRange = 0;
	float minAngle = 0;
	
	// loop through scan ranges
	for (int i = 0; i < msg->ranges.size(); i++) {
		float range = msg->ranges[i];
		
		// bounds check
		if (range >= msg->range_min && range <= msg->range_max) {
			if (!started || range < minRange) {
				// update min
				minRange = range;
				minAngle = msg->angle_min + i * msg->angle_increment;
				started = true;
			}
		}
	}
	
	// any valid point?
	if (started) {
		//get the lock on gMinRange/Angle
		boost::mutex::scoped_lock lock(ra_lock);
		gMinRange = minRange;
		gMinAngle = minAngle;
	}
}

int main(int argc, char** argv) {
	// initialize ros
	ros::init(argc, argv, "rviz_circle");

	//initialize local vars
	float local_minRange = gMinRange;
	float local_minAngle = gMinAngle;
	
	// create the node/publisher
	ros::NodeHandle n;
	ros::Subscriber sub = n.subscribe("scan", 1000, scanCallback);

	ros::Publisher pub = n.advertise<visualization_msgs::Marker>("test_circle", 1000);
	// setup rate
	ros::Rate loop_rate(10);
	uint32_t shape = visualization_msgs::Marker::SPHERE;
	
	while (ros::ok()) {
		visualization_msgs::Marker marker;
		marker.header.frame_id = "/laser";
		marker.header.stamp = ros::Time::now();
		
		marker.ns = "basic_shapes";
		marker.id = 0;
		marker.type = shape;
		marker.action = visualization_msgs::Marker::ADD;
		
		marker.pose.position.x = local_minRange * cos(local_minAngle);
		marker.pose.position.y = local_minRange * sin(local_minAngle);
		marker.pose.position.z = 0;
		
		marker.pose.orientation.x = 0;
		marker.pose.orientation.y = 0;
		marker.pose.orientation.z = 0;
		marker.pose.orientation.w = 1.0;
		
		marker.scale.x = 0.1;
		marker.scale.y = 0.1;
		marker.scale.z = 0.0;
		
		marker.color.r = 0.0f;
		marker.color.g = 1.0f;
		marker.color.b = 0.0f;
		marker.color.a = 0.5;
		
		marker.lifetime = ros::Duration();
		
		// publish the marker
		pub.publish(marker);
		
		ros::spinOnce();
		
		// sleep
		loop_rate.sleep();

		//get mutex and update the local rangle/angle
		boost::mutex::scoped_lock lock(ra_lock);
		local_minRange = gMinRange;
		local_minAngle = gMinAngle;
		//ROS_INFO("updating stuff..... x:%f y:%f", marker.pose.position.x, marker.pose.position.y);
	}
	return 0;
};
