// team name: zen masters
// team members: Joe Marrama, Karl Uhlig, Evan Rosen
// project 1.1

#include "ros/ros.h"
#include "std_msgs/String.h"
#include <geometry_msgs/Twist.h>
#include <signal.h>
#include <termios.h>
#include <stdio.h>
#include "boost/thread/mutex.hpp"
#include "boost/thread/thread.hpp"
#include <turtlebot_node/TurtlebotSensorState.h>

#define KEYCODE_I 0x69 
#define KEYCODE_J 0x6A
#define KEYCODE_K 0x6B
#define KEYCODE_L 0x6C
#define KEYCODE_M 0x6D
#define KEYCODE_Q 0x71
#define KEYCODE_SPACE 0x20 

class turtlebot_teleop_persist
{
public:
	turtlebot_teleop_persist();
	void keyLoop();
	void watchdog();
	void publish(double, double);
	void avoidCliff(const turtlebot_node::TurtlebotSensorState::ConstPtr& msg);
private:
	ros::NodeHandle nh_,ph_;
	double linear_v, angular_v;
	ros::Time first_publish_;
	ros::Time last_publish_;
	double l_scale_, a_scale_;
	ros::Publisher move_pub;
	//void publish(double, double);
	boost::mutex publish_mutex_;
};

turtlebot_teleop_persist::turtlebot_teleop_persist():
	ph_("~"),
	linear_v(0.0),
	angular_v(0.0),
	l_scale_(0.10), //set the linear velocity scale a little low, 1.0 moves the bot too fast
	a_scale_(0.75)
{
  ph_.param("scale_angular", a_scale_, a_scale_);
  ph_.param("scale_linear", l_scale_, l_scale_);

	//set up move publisher
  move_pub = nh_.advertise<geometry_msgs::Twist>("/turtlebot_node/cmd_vel", 1);	
}

int kfd = 0;
struct termios cooked, raw;

void quit(int sig)
{
	ROS_DEBUG("quit is being called?\n");
	tcsetattr(kfd, TCSANOW, &cooked);
	ros::shutdown();
	exit(0);
}

int main(int argc, char **argv)
{
  //init stuff
  ros::init(argc, argv, "turtlebot_teleop_persist");
  turtlebot_teleop_persist turtle;
  ros::NodeHandle n;

	//set up an interrupted signal to call the quit method
	signal(SIGINT, quit);

	//bind a thread to the keyloop of the turtle object
	boost::thread my_thread(boost::bind(&turtlebot_teleop_persist::keyLoop, &turtle));
  
	//create a timer and bind another thread to the watchdog
	ros::Timer timer = n.createTimer(ros::Duration(0.1), boost::bind(&turtlebot_teleop_persist::watchdog, &turtle));

	//subscribe to the turtlebot's general state
	ros::Subscriber sub = n.subscribe("/turtlebot_node/sensor_state", 10, &turtlebot_teleop_persist::avoidCliff, &turtle); 

	ros::spin();

	my_thread.interrupt() ;
	my_thread.join() ;
	  
	return(0);    
}

void turtlebot_teleop_persist::avoidCliff(const turtlebot_node::TurtlebotSensorState::ConstPtr& msg)
{
	bool is_cliff = (msg->cliff_front_left || msg->cliff_front_right
										|| msg->cliff_left || msg->cliff_right);

	//STOP if its a cliff!!	
	if(is_cliff){
  	ROS_INFO("OMG A CLIFF");
		publish(0.0, 0.0);	

		//set linear and angular v to 0
		boost::mutex::scoped_lock lock(publish_mutex_);
		linear_v = 0;
		angular_v = 0;	
	}
}

//thread to continually publish messages that isn't blocked by reading in keystrokes
void turtlebot_teleop_persist::watchdog()
{
  boost::mutex::scoped_lock lock(publish_mutex_);
  if ((ros::Time::now() > last_publish_ + ros::Duration(0.15)) && 
      (ros::Time::now() > first_publish_ + ros::Duration(0.50)))
    publish(angular_v, linear_v);
}

//thread that watches keystrokes
void turtlebot_teleop_persist::keyLoop()
{
	//set up console IO
	char c;

	// get the console in raw mode                                                              
	tcgetattr(kfd, &cooked);
	memcpy(&raw, &cooked, sizeof(struct termios));
	raw.c_lflag &=~ (ICANON | ECHO);
	// Setting a new line, then end of file                         
	raw.c_cc[VEOL] = 1;
	raw.c_cc[VEOF] = 2;
	tcsetattr(kfd, TCSANOW, &raw);

	puts("Reading from keyboard");
	puts("---------------------------");
	puts("Use the ijklm keys to move the turtlebot.");
	puts("Press ctrl+c twice to quit.");
	ROS_DEBUG("starting the program, sweeet\n");

	while (ros::ok())
	{
		// get the next event from the keyboard
		// (the call to read blocks)  
		if(read(kfd, &c, 1) < 0)
		{
		  perror("read():");
		  exit(-1);
		}

		ROS_DEBUG("value: 0x%02X\n", c);

		//get the publishing lock (we might be modifying linear_v/angular_v)
		boost::mutex::scoped_lock lock(publish_mutex_);

		switch(c)
		{
			case KEYCODE_L:
				ROS_DEBUG("moving left...");
				if (angular_v <= 0) {
				  angular_v -= 1.0;
				} else {
				  angular_v = -1.0;
				}
				break;
			case KEYCODE_J:
				ROS_DEBUG("moving right...");
				if (angular_v >= 0) {
				  angular_v += 1.0;
				} else {
				  angular_v = 1.0;
				}
				break;
			case KEYCODE_I:
				ROS_DEBUG("moving forward...");
				linear_v += 1.0;
				break;
			case KEYCODE_M:
				ROS_DEBUG("moving backwards...");
				linear_v -= 1.0;
				break;
			case KEYCODE_K:
				ROS_DEBUG("straightening out!");
				angular_v = 0.0;
				break;
			case KEYCODE_SPACE:
				ROS_DEBUG("stopping!");
				linear_v = 0.0;
				angular_v = 0.0;
				break;
		}

		
		if (ros::Time::now() > last_publish_ + ros::Duration(1.0)) { 
		  first_publish_ = ros::Time::now();
		}
		last_publish_ = ros::Time::now();
		publish(angular_v, linear_v);
	}

	return;
}

void turtlebot_teleop_persist::publish(double angular, double linear)  
{
	//ROS_DEBUG("publishing the message %f %f \n", angular, linear);

	//setup the message to pass
    geometry_msgs::Twist vel;
    vel.angular.z = a_scale_*angular;
    vel.linear.x = l_scale_*linear;

	//publish it, return
    move_pub.publish(vel);    
  	return;
}
