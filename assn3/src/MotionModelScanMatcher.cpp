#include "MotionModelScanMatcher.h"
#include <visualization_msgs/Marker.h>
#include <sensor_msgs/PointCloud.h>
#include "VizUtil.h"
#include "MathUtil.h"
#include "SensorModel.h"
#include <tf/transform_datatypes.h>
#include <cfloat>

MotionModelScanMatcher::MotionModelScanMatcher(double _alpha1, double _alpha2, double _alpha3)
  : MotionModel(_alpha1, _alpha2, _alpha3) {
  cov = new double[9];

  // set occupancy grid dimensions
  occ_xy_grid_dim = 400;
  occ_x_dim = occ_xy_grid_dim;
  occ_y_dim = occ_xy_grid_dim;

  // set pose grid dimensions
  pose_theta_dim = 20;
  pose_x_dim = 50;
  pose_y_dim = 50;

  theta_step = 2 * M_PI / pose_theta_dim;
  xy_step = 0.04; // meters per grid cell (occupancy grid and pose grid have same resolution)
  x_step = xy_step;
  y_step = xy_step; // 2m divided by 100

}

MotionModelScanMatcher::~MotionModelScanMatcher() {
  delete[] cov;
}

// reference_scan_map has size twice that of scan.range_max with robot in the middle
void MotionModelScanMatcher::laserScanToOccupancyGrid(sensor_msgs::LaserScan reference_scan,
						      nav_msgs::OccupancyGrid& reference_scan_map) {
  ROS_INFO("[MotionModelScanMatcher::laserScanToOccupancyGrid]\tentering");
  ROS_INFO("[MotionModelScanMatcher::laserScanToOccupancyGrid]\tmax_range=%f, num_scans=%d",reference_scan.range_max, reference_scan.ranges.size());
  ROS_INFO("[MotionModelScanMatcher::laserScanToOccupancyGrid]\tocc_xy_grid_dim=%d",occ_xy_grid_dim);

  // this ensures that all of the scan points fall in the grid we have created
  reference_scan_map.info.resolution = xy_step;
  reference_scan_map.info.width = occ_x_dim;
  reference_scan_map.info.height = occ_y_dim;
  // init map to zeros
  for (int i = 0; i < occ_xy_grid_dim*occ_xy_grid_dim; i++) {reference_scan_map.data.push_back(0);}

  int num_pts = reference_scan.ranges.size();
  // loop through scan range
  for (int i = 0; i < num_pts; i++) {

      double distance = reference_scan.ranges[i];
      double angle = reference_scan.angle_min + i * reference_scan.angle_increment;
      // bounds check - disregard points outside of lasers range for now
      if (distance < reference_scan.range_min || distance >= reference_scan.range_max) {
          if (distance != reference_scan.range_max) {
              ROS_WARN("[MotionModelScanMatcher::laserScanToOccupancyGrid]\tfound invalid distance: d=%f, min_range=%f, max_range=%f",
                      distance,reference_scan.range_min,reference_scan.range_max);
          }
          continue;
      }

      // get coordinates of point in laser frame
      double x = distance * cos(angle);	
      double y = distance * sin(angle);   

      //get the closest point in the new map frame
      int x_grid = occ_xy_grid_dim/2 + floor(x/x_step + 0.5);
      int y_grid = occ_xy_grid_dim/2 + floor(y/y_step + 0.5);

      reference_scan_map.data[y_grid*occ_xy_grid_dim + x_grid] = 1;
      //ROS_INFO("[MotionModelScanMatcher::laserScanToOccupancyGrid]\tset map[%d,%d]=1",x_grid,y_grid);
  }
  //ROS_INFO("[MotionModelScanMatcher::laserScanToOccupancyGrid]\texiting");
}

void MotionModelScanMatcher::computeMotionModelGrid(PoseGrid &grid) {
    RobotPose p(0,0,0);
    double px;
    for (int theta_ind = 0; theta_ind < pose_theta_dim; theta_ind++) {
        p.theta = theta_ind*theta_step;
        for (int x_ind = 0; x_ind < pose_x_dim; x_ind++) {
            p.x =  (pose_grid_offset_x + x_ind) * x_step;
            for (int y_ind = 0; y_ind < pose_y_dim; y_ind++) {
                p.y = (pose_grid_offset_y + y_ind) * y_step;
                px = getMotionProb(RobotPose(0,0,0), p);
                grid.setVal(theta_ind,y_ind,x_ind,px);
            }
        }
    }
}

void MotionModelScanMatcher::computeSensorModelGrid(PoseGrid &grid, SensorModel &sensor_model) {
    RobotPose p(0,0,0);
    double pz;
    for (int theta_ind = 0; theta_ind < pose_theta_dim; theta_ind++) {
        p.theta = theta_ind*theta_step;
        for (int x_ind = 0; x_ind < pose_x_dim; x_ind++) {
            p.x =  (pose_grid_offset_x + x_ind) * x_step;
            for (int y_ind = 0; y_ind < pose_y_dim; y_ind++) {
                p.y = (pose_grid_offset_y + y_ind) * y_step;
                pz = sensor_model.getScanProb(p);
                grid.setVal(theta_ind,y_ind,x_ind,pz);
            }
        }
    }  
}

void MotionModelScanMatcher::visualizeOdomOffset() {
  RobotPose pose_grid_center = odom_offset.asPose();  

  // visualize pose grid center
  visualization_msgs::Marker marker;
  marker.header.frame_id = "/base_link"; // publish point in the base_link frame as distance from the 
  marker.header.stamp = ros::Time();
  marker.type = visualization_msgs::Marker::LINE_LIST;
  marker.action = visualization_msgs::Marker::ADD;
  marker.ns = "odom_offset";
  marker.id = 0;
  marker.pose.orientation.w = 1.0;
  marker.scale.x = 0.1;
  marker.scale.y = 0.1;
  marker.scale.z = .1;
  marker.color.a = 1.0;
  marker.color.r = 0.0;
  marker.color.g = 1.0;
  marker.color.b = 0.0;
  marker.lifetime = ros::Duration(0);
  geometry_msgs::Point p;
  p.x = 0.0;
  p.y = 0.0;
  p.z = 0.1;
  marker.points.push_back(p);
  p.x = pose_grid_center.x*x_step;
  p.y = pose_grid_center.y*y_step;
  p.z = 0.1;
  marker.points.push_back(p);
  marker_pub->publish(marker);
}

void MotionModelScanMatcher::visualizeReferenceScanMap(nav_msgs::OccupancyGrid &reference_scan_map) {
  reference_scan_map.header.stamp = ros::Time::now();
  reference_scan_map.header.frame_id = "/base_link";
  reference_scan_map.info.map_load_time = ros::Time::now();
  tf::Point origin(-reference_scan_map.info.resolution * reference_scan_map.info.width/2,
                   -reference_scan_map.info.resolution * reference_scan_map.info.height/2,
                   0.01);
  tf::pointTFToMsg(origin, reference_scan_map.info.origin.position);
  tf::quaternionTFToMsg(tf::createQuaternionFromYaw(0), reference_scan_map.info.origin.orientation);
  occ_pub->publish(reference_scan_map);

}

void MotionModelScanMatcher::visualizePoseGrid(PoseGrid &grid, ros::Time pub_time, int grid_sample_rate) {
  ROS_INFO("[visualizePoseGrid]\tentering");
  sensor_msgs::PointCloud cloud;
  cloud.header.stamp = pub_time;
  cloud.header.frame_id = "/base_link";
  
  geometry_msgs::Point32 point;
  sensor_msgs::ChannelFloat32 channel;
  channel.name = "intensity";
  cloud.channels.push_back(channel);
  
  double grid_val;
  for (int x = 0; x < pose_x_dim; x += grid_sample_rate) {
    for (int y = 0; y < pose_y_dim; y += grid_sample_rate) {

      point.x = ( -occ_x_dim/2 + pose_grid_offset_x + x) * x_step;
      point.y = ( -occ_y_dim/2 + pose_grid_offset_y + y) * y_step;
      point.z = 0.2;

      cloud.points.push_back(point);
      grid_val = grid.grid[0];
      for (int theta = 0; theta < pose_theta_dim; theta += grid_sample_rate) {
          if (grid.getVal(theta,y,x) > grid_val) {grid_val = grid.getVal(theta,y,x);}
      }
      //ROS_INFO("[MotionModelScanMatcher::visualizePoseGrid]\tmax(grid[%d,%d,:] = %f", x, y, grid_val);
      cloud.channels[0].values.push_back(grid_val);
    }
  }
  pose_grid_pub->publish(cloud);
  // TODO also publish to pose_pub to show orientation
}

// we do all the heavy lifting from here, sampleMotion just uses instance varialbes cov and mu
void MotionModelScanMatcher::setScans(const sensor_msgs::LaserScan reference_scan, 
				      const sensor_msgs::LaserScan curr_scan) {
  //  ROS_INFO("[MotionModel::MotionModelScanMatcher]\tentering setScans: reference_scan.ranges.size()=%d",reference_scan.ranges.size());

  // construct a sensor model from reference_scan
  // first turn reference scan into occupancy grid for compatibility
  nav_msgs::OccupancyGrid reference_scan_map;
  ROS_INFO("[MotionModelScanMatcher::setScans]\tocc_xy_grid_dim=%d",occ_xy_grid_dim);
  laserScanToOccupancyGrid(reference_scan, reference_scan_map);
  ros::Time curr_time = ros::Time::now();
  visualizeReferenceScanMap(reference_scan_map);
  SensorModel sensor_model(reference_scan_map);
  
  //sensor_model->visualizeLikelihoodMap(marker_pub,occ_xy_grid_dim, occ_xy_grid_dim,xy_grid_resolution);
  sensor_model.updateScan(curr_scan);

  // translation constants
  // these describe the translation of the pose_grid ORIGIN wrt to the occupancy map ORIGIN
  RobotPose pose_grid_center = odom_offset.asPose();  
  visualizeOdomOffset();

  pose_grid_offset_x = (pose_grid_center.x / x_step) + (occ_x_dim - pose_x_dim) / 2;
  pose_grid_offset_y = (pose_grid_center.y / y_step) + (occ_y_dim - pose_y_dim) / 2;
  ROS_INFO("[MotionModelScanMatcher::setScans]\tpose_grid_offset_x=%f, pose_grid_offset_y=%f",pose_grid_offset_x, pose_grid_offset_y);

  // construct grid datastructure, init cells to 1.0
  PoseGrid sensor_model_grid(pose_theta_dim, pose_y_dim, pose_x_dim);
  for(int i = 0; i < sensor_model_grid.num_cells; i++) sensor_model_grid.grid[i] = -DBL_MAX;

  int grid_sample_rate = 4;
  int scan_sample_rate = 1; // these are actually 1/rate, i.e number of scans/pixels to skip
  sensor_model.calcGridProbs(&sensor_model_grid, pose_grid_offset_x, pose_grid_offset_y, grid_sample_rate, scan_sample_rate);
  //computeSensorModelGrid(sensor_model_grid,sensor_model);
  PoseGrid motion_model_grid(pose_theta_dim, pose_y_dim, pose_x_dim);
  computeMotionModelGrid(motion_model_grid);
  PoseGrid combined_grid(pose_theta_dim, pose_y_dim, pose_x_dim);
  
  double max_val;
  max_val = sensor_model_grid.grid[0] + motion_model_grid.grid[0];

  bool use_motion_model = true;
  int max_ind = -1;
  double curr;
  for (int i = 0; i < combined_grid.num_cells; i++) {
    //ROS_INFO("[MotionModelScanMatcher::setScans]\tsensor_model:%f, motion_model:%f",sensor_model_grid.grid[i], motion_model_grid.grid[i]);

    if(sensor_model_grid.grid[i] == -DBL_MAX){
      combined_grid.grid[i] = -DBL_MAX;
      continue;
    } 
      
    if (use_motion_model) {
      curr = sensor_model_grid.grid[i] + motion_model_grid.grid[i];
    } else {
      curr = sensor_model_grid.grid[i];
    }
    if (curr == 0) {
      ROS_WARN("[MotionModelScanMatcher::setScans]\tsetting grid[%d] = 0.0",i);
    }
    combined_grid.grid[i] = curr;
    if (curr > max_val) {
      max_val = curr;
      max_ind = i;
    }
  }

  visualizePoseGrid(combined_grid, curr_time, grid_sample_rate);

  max_pose = combined_grid.getIndexPose(max_ind);
  max_pose.x = max_pose.x * x_step;
  max_pose.y = max_pose.y * y_step;
  max_pose.theta = max_pose.theta * theta_step;

  //ROS_INFO("[MotionModelScanMatcher::setScans]\tmax_ind : %d, max_pose : (%f,%f,%f)",max_ind, max_pose.x,max_pose.y,max_pose.theta);
  // extract mode (mu) and covariance matrix
  estimateCovariance(max_pose,combined_grid);
}

void MotionModelScanMatcher::estimateCovariance(RobotPose max_pose, PoseGrid& grid){
  // first normalize grid
  double sum = 0;
  double offset = 500;
  double phat;

  double temp_scatter[9];
  double logp;
  RobotPose p, norm_p;
  for (int theta_ind = 0; theta_ind < pose_theta_dim; theta_ind++) {
      p.theta = theta_ind*theta_step;
      for (int x_ind = 0; x_ind < pose_x_dim; x_ind++) {
          p.x = x_ind*x_step;
          for (int y_ind = 0; y_ind < pose_y_dim; y_ind++) {
              p.y = y_ind*y_step;
              norm_p = p - max_pose;
              norm_p.scatter(temp_scatter);
              logp = grid.getVal(theta_ind,x_ind,y_ind);
              
              if(logp == -DBL_MAX){
                continue;
              }

              //ROS_INFO("[MotionModelScanMatcher::estimateCovariance]\tlogp=%f",logp);
              phat = exp((logp / 10) + offset); // fudge
              //phat = exp(logp + offset); // legit fudge but overflows still
              sum += phat;
              for (int i = 0; i < 9; i++) {
                  cov[i] += phat*temp_scatter[i];
              }
          }
      }
  }

  // map back to 
  for (int i=0; i < 9; i++) {
      cov[i] = cov[i] / sum;
      if (cov[i] > 10000) cov[i] = 10000; // keep poor estimate from hosing everything else
  }

  ROS_INFO("[MotionModelScanMatcher::estimateCovariance]\tSigma=[%e\t%e\t%e]",cov[0],cov[1],cov[2]);
  ROS_INFO("                                            \t      [%e\t%e\t%e]",cov[3],cov[4],cov[5]);
  ROS_INFO("                                            \t      [%e\t%e\t%e]",cov[6],cov[7],cov[8]);
}

RobotPose MotionModelScanMatcher::sampleMotion(RobotPose pose) {
  RobotPose sampled_pose = pose;
  sampled_pose.x += getGaussianRand(0.0, cov[0]);
  sampled_pose.y += getGaussianRand(0.0, cov[4]);
  double unnormalized_theta += getGaussianRand(0.0, cov[8]);
  sampled_pose.theta = fmod(unnormalized_theta, 2.0*M_PI); // keep within range
  if (sampled_pose.theta == unnormalized_theta) {
    ROS_INFO("unnormalized_theta=\t%f, sampled_pose.theta=\t%f", unnormalized_theta, sampled_pose.theta);
  }
  return sampled_pose;
}
