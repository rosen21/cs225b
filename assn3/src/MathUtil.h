#ifndef _MATH_UTIL_H
#define _MATH_UTIL_H

#include <math.h>
#include <stdlib.h>
#define _USE_MATH_DEFINES

double absf(double val) {
  return val < 0 ? -val : val;
}

double getUnitRand() {
  return ((double)rand() / (double)RAND_MAX);
}

double getUniformRand(double min, double max) {
  double range = max - min;
  return getUnitRand() * range + min;
}

double getGaussianRand(double mean, double variance) {
  double u1 = getUnitRand();
  double u2 = getUnitRand();
  double gaussian_sample = sqrt(-2*log(u1)) * cos(2*M_PI*u2);
  return gaussian_sample*sqrt(variance) + mean;
  //return partial_res = sqrt(-2*log(u1)) * sin(2*M_PI*u2);
}

double getGaussianDensity(double val, double mu, double std_dev) {
    double var = pow(std_dev, 2);
    return exp(-pow(val - mu,2)/(2 * var)) / (sqrt(2*M_PI*var)); 
}

double getGaussianLogDensity(double val, double mu, double std_dev) {
  //ROS_INFO("[getGaussianDensity]\tval=%f, mu=%f, std_dev=%f",val,mu,std_dev);
  double var = pow(std_dev, 2);
  return -pow(val - mu,2)/(2 * var) - log(sqrt(2*M_PI*var));
}

double logAdd(double log_p1, double log_p2) {
  double max_log = max(log_p1, log_p2);
  double p1_hat = exp(log_p1 - max_log);
  double p2_hat = exp(log_p2 - max_log);
  double log_p3_hat = log(p1_hat + p2_hat);
  return log_p3_hat + max_log;
}

/* returns the difference between two headings normalized to the range [0,PI] */
double headingChange(double theta1, double theta2) {
  // get some 0 <= d <= 2*M_PI
  double d = theta1 - theta2;
  if (d < 0) {
    d = -d;
  }
  
  // smaller change if we rotate in the opposite direction
  if (d > M_PI) {
    return 2 * M_PI - d;
  }
  return d;
}

/* binary searches the array for value, assuming arr[lb] < value && arr[ub] >= value.
 * Returns the upper bound index. */
int binarySearch(double *arr, double value, int lb, int ub) {
  // base case
  if (ub - lb <= 1) {
    return ub;
  }
  
  // middle index
  int mid = (lb + ub) / 2;
  
  // mid is too small
  if (arr[mid] < value) {
    return binarySearch(arr, value, mid, ub);
  }
  
  // mid is at least big enough
  return binarySearch(arr, value, lb, mid);
}

#endif

