#include "MotionModel.h"
#include <ros/ros.h>
#include "MathUtil.h"


void MotionModel::getPoseOffset(RobotPose start, RobotPose end, PoseOffset &offset) {

  double dx = end.x - start.x;
  double dy = end.y - start.y;
  double dtheta = end.theta - start.theta;
  
  // handle cases that cross the -pi/pi border
  if (dtheta < -M_PI) {
    dtheta = dtheta + 2 * M_PI;
  } else if (dtheta > M_PI) {
    dtheta = dtheta - 2 * M_PI;
  }

  // set translation
  offset.trans = sqrt(dx * dx + dy * dy);
  
  // handle case where offset.trans is too small
  if (offset.trans < trans_epsilon) {
    offset.rot1 = 0;
  } else {
    offset.rot1 = atan2(dy, dx) - start.theta;
    if (offset.rot1 > M_PI) offset.rot1 -= 2 * M_PI;
    else if (offset.rot1 < -M_PI) offset.rot1 += 2 * M_PI;
  }
  
  offset.rot2 = dtheta - offset.rot1;
}

void MotionModel::setOdometry(RobotPose odom_start, RobotPose odom_end) { 
  getPoseOffset(odom_start, odom_end, odom_offset);
  
  rot1_var = alpha1 * absf(odom_offset.rot1) + alpha2 * odom_offset.trans;
  trans_var = alpha3 * odom_offset.trans;
  rot2_var = alpha1 * absf(odom_offset.rot2) + alpha2 * odom_offset.trans;
  //ROS_INFO("setting odometry: dx=%f, dy=%f, dt=%f", dx, dy, dtheta);
  //ROS_INFO("setting odometry: r1=%f, r2=%f, t=%f", rot1, rot2, trans);
  //ROS_INFO("setting odometry: r1v=%f, r2v=%f, tv=%f", rot1_var, rot2_var, trans_var);
}

RobotPose MotionModel::sampleMotion(RobotPose pose) {
  // sample from motion model
  double rot1_hat = getGaussianRand(odom_offset.rot1, rot1_var);
  double trans_hat = getGaussianRand(odom_offset.trans, trans_var);
  double rot2_hat = getGaussianRand(odom_offset.rot2, rot2_var);
 
  RobotPose result;
  result.x = pose.x + trans_hat * cos(pose.theta + rot1_hat);
  result.y = pose.y + trans_hat * sin(pose.theta + rot1_hat);
  result.theta = pose.theta + rot1_hat + rot2_hat;
  if (result.theta > M_PI) {
    result.theta -= 2 * M_PI;
  } else if (result.theta < -M_PI) {
    result.theta += 2 * M_PI;
  }
  return result;
}

// returns probability of moving from origin (0,0,0) to pose p
// given odometry information
double MotionModel::getMotionProb(RobotPose pose_origin, RobotPose p) {

  PoseOffset motion_offset;
  getPoseOffset(pose_origin,p,motion_offset);

  if (rot1_var == 0 || trans_var == 0 || rot2_var == 0) {
    // ROS_WARN("[MotionModel::getMotionProb]\tvariance found equal to zero");
    // ROS_WARN("[MotionModel::getMotionProb]\tro1_var=%f, trans_var=%f, rot2_var=%f",rot1_var,trans_var,rot2_var);
    return -10000000.0;
  }
  double lp_rot1_hat = getGaussianLogDensity(odom_offset.rot1 - motion_offset.rot1, 0.0, rot1_var);
  double lp_trans_hat = getGaussianLogDensity(odom_offset.trans - motion_offset.trans, 0.0, trans_var);
  double lp_rot2_hat = getGaussianLogDensity(odom_offset.rot2 - motion_offset.rot2, 0.0, rot2_var);
  //ROS_INFO("[MotionModelScanMatcher::getMotionProb]\tlp_rot1_hat=%f, lp_trans_hat=%f, lp_rot2_hat=%f",lp_rot1_hat,lp_trans_hat,lp_rot2_hat);

  double lp = lp_rot1_hat + lp_trans_hat + lp_rot2_hat;
  
  //ROS_INFO("[MotionModelScanMatcher::getMotionProb]\tlp=%f",lp);
  return lp;
}
