#ifndef _PARTICLE_FILTER_H
#define _PARTICLE_FILTER_H

#include <ros/ros.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <nav_msgs/OccupancyGrid.h>
#include <sensor_msgs/LaserScan.h>
#include <tf/transform_listener.h>
#include <tf/transform_datatypes.h>

#include "MotionModel.h"
#include "MotionModelScanMatcher.h"
#include "RobotPose.h"
#include "SensorModel.h"

using namespace std;

/* basic particle data structure */
struct Particle {
  RobotPose pose;
  double weight;
};

class ParticleFilter {
  
 public:
  ParticleFilter();
  
  /* inline getters/setters */
  void setSensorModel(SensorModel *_sensor_model) { sensor_model = _sensor_model; }
  void setMotionModel(MotionModel *_motion_model) { motion_model = _motion_model; }
  
  void setTransformListener(tf::TransformListener *_listener) { listener = _listener; prev_odom = lookupRobotOdomPose(); }
  tf::Transform getMapOdomTransform() { return map_odom_transform; }
    
  /* initializes the particle filter, using a gaussian distribution centered at a given pose */
  void initGaussian(int num_particles, double x_mean, double x_var, double y_mean, double y_var, double theta_mean, double theta_var);
  
  /* initializes the particle filter, using a uniform distribution over a given pose range */
  void initUniform(int num_particles, double x_min, double x_max, double y_min, double y_max, double theta_min, double theta_max);
    
  /* called when a new laser scan is received */
  void processLaserReading(const sensor_msgs::LaserScan::ConstPtr &msg);
  
  /* called when a new laser scan is received */
  void processPoseEstimate(const geometry_msgs::PoseWithCovarianceStamped::ConstPtr &msg);
  
    /* Visualizes the particle cloud */
  void visualizeParticles(ros::Publisher &pub);
  
  /* sets the global map */
  void setMap(nav_msgs::OccupancyGrid &global_map);
  
 private:
  /* config */
  string map_frame, odom_frame, robot_frame;
  SensorModel *sensor_model;
  MotionModel *motion_model;
  tf::TransformListener *listener;
  
  /* === configurable parameters === */
  double min_distance, min_rotation;
  
  int initialpose_num_particles;
  double initialpose_x_var;
  double initialpose_y_var;
  double initialpose_theta_var;
  
  double kld_bin_x;
  double kld_bin_y;
  double kld_bin_theta;
  double kld_z_quantile;
  double kld_epsilon;
  int kld_min_samples;
  int kld_max_samples;
  
  double random_resample_prob;
  double random_noise_x;
  double random_noise_y;
  
  /* global map */
  nav_msgs::OccupancyGrid global_map;
  int map_size_x;
  int map_size_y;
  double map_resolution;
  
  /* stores last computed transform */
  tf::Transform map_odom_transform;
  RobotPose prev_odom, curr_odom;
  sensor_msgs::LaserScan curr_scan, reference_scan ;
  RobotPose pose_estimate;
  
  // particle set
  vector<Particle> particles;
  
  // private methods
  RobotPose randomPose(double x_min, double x_max, double y_min, double y_max, double theta_min, double theta_max);
  RobotPose randomPose();
  bool onMap(double x, double y);
  void performUpdate();
  bool robotHasMoved();
  RobotPose lookupRobotOdomPose();
  void updateMapOdomTransform();
};

#endif

