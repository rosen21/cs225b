#ifndef _ROBOT_POSE_H
#define _ROBOT_POSE_H

using namespace std;

/* Pose class represting a single (x,y,theta) triple. */
class RobotPose {
  public:
    double x;
    double y;
    double theta;

    RobotPose() {};
    RobotPose(double _x, double _y, double _theta) {x=_x; y=_y; theta=_theta;}

    RobotPose operator-(RobotPose other_pose) {
      RobotPose new_pose;
      new_pose.x = x - other_pose.x;
      new_pose.y = y - other_pose.y;
      new_pose.theta = theta - other_pose.theta;
      return new_pose;
    };

    // sets res to an outer product (scatter) matrix in row major order
    void scatter(double* res) {
      res[0] = x * x;
      res[1] = x * y;
      res[2] = x * theta;
      
      res[3] = y * x;
      res[4] = y * y;
      res[5] = y * theta;
      
      res[6] = theta * x;
      res[7] = theta * y;
      res[8] = theta * theta;
    };
};

#endif
