#include "KDHistogram.h"
#include <ros/ros.h>

KDHistogram::KDHistogram() {
  bins = NULL;
}

KDHistogram::~KDHistogram() {
  if (bins != NULL) {
    delete[] bins;
  }
}

void KDHistogram::setXRange(double min, double max, double step) {
  x_min = min;
  x_max = max;
  x_step = step;
}

void KDHistogram::setYRange(double min, double max, double step) {
  y_min = min;
  y_max = max;
  y_step = step;
}

void KDHistogram::setThetaRange(double min, double max, double step) {
  theta_min = min;
  theta_max = max;
  theta_step = step;
}

void KDHistogram::resetHistogram() {
  // delete existing bins
  if (bins != NULL) {
    delete[] bins;
    bins = NULL;
  }
  
  // compute bin dimensions
  x_dim = 1 + (int)((x_max - x_min) / x_step);
  y_dim = 1 + (int)((y_max - y_min) / y_step);
  theta_dim = 1 + (int)((theta_max - theta_min) / theta_step);
    
  // allocate the bins
  int total_bins = x_dim * y_dim * theta_dim;
  bins = new int[total_bins];
  bins_with_support = 0;

  // initialize to zero
  memset(bins, 0, sizeof(int) * total_bins);
}


void KDHistogram::addPose(RobotPose pose) {
  int x_idx = (int)((pose.x - x_min) / x_step);
  if (x_idx < 0 || x_idx >= x_dim) {
    return;
  }
  
  int y_idx = (int)((pose.y - y_min) / y_step);
  if (y_idx < 0 || y_idx >= y_dim) {
    return;
  }
  
  int theta_idx = (int)((pose.theta - theta_min) / theta_step);
  if (theta_idx < 0 || theta_idx >= theta_dim) {
    ROS_INFO("skipping cuz theta is %f idx=%d", pose.theta, theta_idx); 
    return;
  }
  
  int idx = x_idx * y_dim * theta_dim + y_idx * theta_dim + theta_idx;
  if (bins[idx] == 0) {
    bins_with_support++;
  }
  bins[idx]++;
}
