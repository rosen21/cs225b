#ifndef POSE_GRID_H
#define POSE_GRID_H

class PoseGrid{

 public:
  double* grid;
  int num_cells;
  int theta_dim;
  int x_dim;
  int y_dim;

  PoseGrid() {};
  PoseGrid(int _theta_dim, int _y_dim, int _x_dim) {
    theta_dim = _theta_dim;
    y_dim = _y_dim;
    x_dim = _x_dim;
    num_cells = theta_dim * y_dim * x_dim;
    grid = new double[num_cells];
  };
  
  ~PoseGrid() {
    delete[] grid;
  };

  
  bool inBounds(int theta_ind, int x_ind, int y_ind) {
    return (theta_ind  >= 0 &&
	    theta_ind  < theta_dim &&
	    y_ind >= 0 &&
	    y_ind < y_dim &&
	    x_ind >= 0 &&
	    x_ind < x_dim);
  };

  double getVal(int theta_ind, int y_ind, int x_ind) {
    if (!inBounds(theta_ind,y_ind,x_ind)) ROS_ERROR("[PoseGrid::getVal]\tout of bounds index: (%d,%d,%d)",theta_ind,y_ind,x_ind);
    int i = (theta_ind * y_dim * x_dim) + (y_ind * x_dim) + x_ind;
    return grid[i];
  };

  void setVal(int theta_ind, int y_ind, int x_ind, double val) {
    if (!inBounds(theta_ind,y_ind,x_ind)) ROS_ERROR("[PoseGrid::setVal]\tout of bounds index: (%d,%d,%d)",theta_ind,y_ind,x_ind);
    int i = (theta_ind * y_dim * x_dim) + (y_ind * x_dim) + x_ind;
    grid[i] = val;
  };

  RobotPose getIndexPose(int i) {
    RobotPose p;
    p.theta = i / (x_dim * y_dim);
    int theta_rem = i % (x_dim * y_dim);
    p.y = theta_rem / x_dim;
    p.x = theta_rem % x_dim;
    return p;
  }
};

#endif
