#ifndef _KDHISTOGRAM_H
#define _KDHISTOGRAM_H

#include "RobotPose.h"

class KDHistogram {

  public:
    KDHistogram();
    ~KDHistogram();
    void setXRange(double min, double max, double step);
    void setYRange(double min, double max, double step);
    void setThetaRange(double min, double max, double step);
    void resetHistogram();
    void addPose(RobotPose pose);
    int numBinsWithSupport() { return bins_with_support; }
    
  private:
    double x_min, x_max, x_step;
    double y_min, y_max, y_step;
    double theta_min, theta_max, theta_step;
    
    int x_dim, y_dim, theta_dim;
    int bins_with_support;
    int *bins;
};

#endif

