#ifndef POINT_H
#define POINT_H

class Point {
public:
  int x;
  int y;
  Point() {}
  Point(int _x, int _y) {x=_x; y=_y;}
  Point(int i,int map_size_x, int map_size_y) {x=i%map_size_x; y=i/map_size_x;}
  
  bool isValid(int map_size_x, int map_size_y) {
    return (x >= 0) && (x < map_size_x) && (y >= 0) && (y < map_size_y);
  }
  
  int get4Nbrs(Point* nbrs, int map_size_x, int map_size_y) {
    int num_nbrs = 0;
    Point new_pt;
    for (int i = -1; i <= 1; i++) {
      for (int j = -1; j <= 1; j++) {
	      if (i*j == 0 && i != j) {
	        new_pt.x = x + i;
	        new_pt.y = y + j;
	        if (new_pt.isValid(map_size_x,map_size_y)) {
	          //ROS_INFO("[get4Nbrs]\tadding nbr: (%d,%d)",new_x,new_y);
	          nbrs[num_nbrs] = new_pt;
	          num_nbrs++;
	        }
	      }
      }
    }
    return num_nbrs;
  }
};

#endif
