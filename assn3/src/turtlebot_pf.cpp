// TEST TEST
#include <ros/ros.h>
#include <visualization_msgs/Marker.h>
#include <geometry_msgs/PoseArray.h>
#include <nav_msgs/OccupancyGrid.h>
#include <nav_msgs/GetMap.h>
#include <sensor_msgs/LaserScan.h>
#include <sensor_msgs/PointCloud.h>
#include <tf/transform_listener.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_datatypes.h>

#include "ParticleFilter.h"
#include "MathUtil.h"
using namespace std;

nav_msgs::OccupancyGrid global_map;

int main(int argc, char** argv) {
  // initialize ros node
  ros::init(argc, argv, "turtlebot_pf");    
  ros::NodeHandle nh;
  
  tf::TransformListener listener;
  tf::TransformBroadcaster br;

  // wait for odom-robot transform
  listener.waitForTransform("/odom", "/base_link", ros::Time(), ros::Duration(1.5));
  
  // setup sensor model
  ROS_INFO("[ParticleFilter::main]\twaiting for initial map message");
  global_map = *ros::topic::waitForMessage<nav_msgs::OccupancyGrid>("map");
  
  SensorModel sensor_model(global_map);

  // setup motion model
  ROS_INFO("[ParticleFilter::main]\tinit motion model");
  //MotionModel motion_model(0.001,0.001,0.01);
  //MotionModel motion_model(0.01, 0.01, 0.01);
  //MotionModelScanMatcher motion_model(0.01, 0.01, 0.01);
  MotionModelScanMatcher motion_model(1, 1, 2);
  
  // create the particle filter
  ROS_INFO("[ParticleFilter::main]\tinit particle filter");
  ParticleFilter pf;
  pf.setMotionModel(&motion_model);
  pf.setSensorModel(&sensor_model);
  pf.setTransformListener(&listener);
  pf.setMap(global_map);
  ros::Subscriber scan_sub = nh.subscribe("scan", 2, &ParticleFilter::processLaserReading, &pf);
  ros::Subscriber pose_sub = nh.subscribe("initialpose", 2, &ParticleFilter::processPoseEstimate, &pf);
  
  // initialize particles
  ROS_INFO("[ParticleFilter::main]\tinit particle cloud");
  pf.initUniform(10000, 0, 53.6, 0, 53.0, -M_PI, M_PI);
  //pf.initGaussian(1000, 0.0, 0.05, 0.0, 0.05, 0.0, 0.05);
  
  // visualization publishers
  ROS_INFO("[ParticleFilter::main]\tinit viz pubs");
  ros::Publisher marker_pub = nh.advertise<visualization_msgs::Marker>("visualization_marker", 0);
  ros::Publisher pose_pub = nh.advertise<geometry_msgs::PoseArray>("pose_cloud", 0);
  ros::Publisher likelihood_pub = nh.advertise<sensor_msgs::PointCloud>("likelihood_field", 0);
  ros::Publisher pose_grid_pub = nh.advertise<sensor_msgs::PointCloud>("sm_pose_grid", 0);
  ros::Publisher occ_pub = nh.advertise<nav_msgs::OccupancyGrid>("sm_reference_scan", 0);


  motion_model.setMarkerPublisher(&marker_pub);
  motion_model.setPoseGridPublisher(&pose_grid_pub);
  motion_model.setOccPublisher(&occ_pub);
  
  //visualize initial scan likelihood for all points- its VERY VERY slow, only do once
  sensor_model.updateScan(ros::topic::waitForMessage<sensor_msgs::LaserScan>("scan"));
  //sensor_model.visualizeScanLikelihood(marker_pub);
  
  sensor_model.visualizeLikelihoodMap(likelihood_pub);
  
  int loop_rate = 10;
  ros::Rate rate(loop_rate);
  ROS_INFO("entering main loop...");
  while (ros::ok()) {
    // listen for incoming messages. particle filter updates happen here
    ros::spinOnce();

    // publish map-odom transform
    tf::Transform mapOdomTransform = pf.getMapOdomTransform();
    br.sendTransform(tf::StampedTransform(mapOdomTransform, ros::Time::now(), "odom", "map"));
	
    // update visualization
    pf.visualizeParticles(pose_pub);
    rate.sleep();
  }

  return 0;
}
