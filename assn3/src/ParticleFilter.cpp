#include "ParticleFilter.h"

#include <geometry_msgs/Pose.h>
#include <geometry_msgs/PoseArray.h>
#include "MathUtil.h"
#include "KDHistogram.h"

ParticleFilter::ParticleFilter() {
  // config defaults
  map_frame = "/map";
  odom_frame = "/odom";
  robot_frame = "/base_link";

  // minimum odom change before update
  min_distance = 0.10; // meters
  min_rotation = 0.3; // radians
  
  // values to use when setting an rviz pose estimate
  initialpose_num_particles = 1000;
  initialpose_x_var = 0.05;
  initialpose_y_var = 0.05;
  initialpose_theta_var = 0.1;
  
  // probability of using a random pose
  random_resample_prob = 0.015;
  
  // random noise
  random_noise_x = 0.0004;
  random_noise_y = 0.0004;
  
  kld_bin_x = 0.2; // meters
  kld_bin_y = 0.2; // meters
  kld_bin_theta = 0.1; // radians
  kld_epsilon = 0.02;
  kld_min_samples = 500;
  kld_max_samples = 10000;
  kld_z_quantile = 3;
  
  map_odom_transform.setIdentity();
}

/* sets the global map */
void ParticleFilter::setMap(nav_msgs::OccupancyGrid &msg) {
  map_size_x = msg.info.width;
  map_size_y = msg.info.height;
  map_resolution = msg.info.resolution;
  global_map = msg;
}

/* initializes the particle filter, using a gaussian distribution centered at a given pose */
void ParticleFilter::initGaussian(int num_particles, double x_mean, double x_var, double y_mean, double y_var, double theta_mean, double theta_var) {
  prev_odom = curr_odom = lookupRobotOdomPose();
  particles.clear();
  for (int i = 0; i < num_particles; i++) {
    // sample point
    Particle particle;
    particle.pose.x = getGaussianRand(x_mean, x_var);
    particle.pose.y = getGaussianRand(y_mean, y_var);
    particle.pose.theta = getGaussianRand(theta_mean, theta_var);
    particle.weight = 1.0 / num_particles;
    particles.push_back(particle);
  }
}

/* returns true if (x,y) lies on a valid (unoccupied) map cell */
bool ParticleFilter::onMap(double x, double y) {
  int x_idx = (int)(x / map_resolution);
  int y_idx = (int)(y / map_resolution);
  
  if (x_idx < 0 || x_idx >= map_size_x) {
    return false;
  }
  if (y_idx < 0 || y_idx >= map_size_y) {
    return false;
  }
  
  int map_idx = y_idx * map_size_x + x_idx;
  int val = global_map.data[map_idx];
  return val == 0;
}

/* initializes the particle filter, using a uniform distribution over a given pose range */
void ParticleFilter::initUniform(int num_particles, double x_min, double x_max, double y_min, double y_max, double theta_min, double theta_max) {
  prev_odom = curr_odom = lookupRobotOdomPose();
  particles.clear();
  for (int i = 0; i < num_particles; i++) {
    // sample point
    Particle particle;
    particle.pose = randomPose(x_min, x_max, y_min, y_max, theta_min, theta_max);
    particle.weight = 1.0 / num_particles;
    particles.push_back(particle);
  }
}

/* returns a (uniformly) random pose *ON THE MAP* in the given window) */
RobotPose ParticleFilter::randomPose(double x_min, double x_max, double y_min, double y_max, double theta_min, double theta_max) {
  RobotPose pose;
  pose.theta = getUniformRand(theta_min, theta_max);
  pose.x = getUniformRand(x_min,x_max);
  pose.y = getUniformRand(y_min,y_max);
  
  while (!onMap(pose.x, pose.y)) {
    pose.x = getUniformRand(x_min,x_max);
    pose.y = getUniformRand(y_min,y_max);
  }
  
  return pose;
}

/* returns a pose at a random (valid) location on the map */
RobotPose ParticleFilter::randomPose() {
  return randomPose(0, map_size_x * map_resolution, 0, map_size_y * map_resolution, -M_PI, M_PI);
}

/* performs one full iteration of the particle filter algorithm */
void ParticleFilter::performUpdate() {
  ROS_INFO("======= UPDATING PARTICLE FILTER =======");
  vector<Particle> new_particles;

  // update motion model with new odometry readings for sampling

  ROS_INFO("[performUpdate]\tprev_odom: (%f,%f,%f), curr_odom: (%f,%f,%f)",
	   prev_odom.x,prev_odom.y,prev_odom.theta,curr_odom.x,curr_odom.y,curr_odom.theta);
  motion_model->setOdometry(prev_odom, curr_odom);
  motion_model->setScans(reference_scan, curr_scan);

  // create cumulative weight array to sample particles
  double *cumulative_weight = new double[particles.size() + 1];
  cumulative_weight[0] = 0;
  for (size_t i = 0; i < particles.size(); i++) {
    cumulative_weight[i + 1] = cumulative_weight[i] + particles[i].weight;
    if (particles[i].weight < 0) {
      ROS_WARN("particle %d has negative weight: %f", i, particles[i].weight);
    }
  }
  
  // perform KLD update  
  KDHistogram hist;
  hist.setXRange(0, map_size_x * map_resolution, kld_bin_x);
  hist.setYRange(0, map_size_y * map_resolution, kld_bin_y);
  hist.setThetaRange(-M_PI, M_PI, kld_bin_theta);
  hist.resetHistogram();
  

  double max_weight = 0;
  double min_weight = 0;
  int n = 0;
  double nx = 0;
  int k = 0;
  
  while (n < nx || n < kld_min_samples) {
    Particle new_particle;

    if (getUnitRand() <= random_resample_prob) {
      // get a random pose on the map
      new_particle.pose = randomPose();
    } else {
      // sample a particle from the existing set
      double rand = getUnitRand();
      int idx = binarySearch(cumulative_weight, rand, 0, particles.size()) - 1;
      // sample from motion model
      new_particle.pose = motion_model->sampleMotion(particles[idx].pose);
      
      // perturb the pose
      new_particle.pose.x += getGaussianRand(0, random_noise_x);
      new_particle.pose.y += getGaussianRand(0, random_noise_y);
      
      // skip a particle that's off the map (good idea?)
      if (!onMap(new_particle.pose.x, new_particle.pose.y)) {
        continue;
      }
    }
    
    // update particle weight
    // note: this is currently in log space!
    new_particle.weight = sensor_model->getScanProb(new_particle.pose);
    if (n == 0 || new_particle.weight < min_weight) {
      min_weight = new_particle.weight;
    }
    if (n == 0 || new_particle.weight > max_weight) {
      max_weight = new_particle.weight;
    }
    
    // add to new set
    new_particles.push_back(new_particle);
    n++;
    
    // add to kld histogram
    hist.addPose(new_particle.pose);
    k = hist.numBinsWithSupport();
    
    // update samples needed
    if (n >= kld_min_samples && k > 1) {
      double a = 2/(9 * (k - 1));
      nx = (k - 1)/(2 * kld_epsilon) * pow(1 - a + sqrt(a) * kld_z_quantile, 3);
      
      // limit max samples
      if (nx > kld_max_samples) {
        nx = kld_max_samples;
      }
    }
  }
  
  ROS_INFO("[ParticleFilter::performUpdate]\tResampled %d points", n);
  ROS_INFO("[ParticleFilter::performUpdate]\tweight range (log): %f", (max_weight - min_weight));

  double reinit_thresh = 0;
  if (max_weight - min_weight < reinit_thresh) {
    ROS_INFO("[ParticleFilter::performUpdate]\tpoorly localized, (weight_range=%f < reinit_thresh=%f) reinitializing as uniform", max_weight - min_weight,reinit_thresh);
    initUniform(10000, 0, 53.6, 0, 53.0, -M_PI, M_PI);
    return;
  }

  double total_weight = 0;
  double max_scaled_weight = 0;
  int max_idx = 0;
  
  for (size_t i = 0; i < new_particles.size(); i++) {
    double scaled_log_weight = new_particles[i].weight - min_weight;
    double scaled_weight = exp(scaled_log_weight);
    total_weight += scaled_weight;
    new_particles[i].weight = scaled_weight;
    if (scaled_weight > max_scaled_weight) {
      max_scaled_weight = scaled_weight;
      max_idx = i;
    } 
  }
  
  // normalize weights
  for (size_t i = 0; i < new_particles.size(); i++) {
    new_particles[i].weight /= total_weight;
  }
  
  // clean up
  delete[] cumulative_weight;
  
  // update state
  particles = new_particles;
  pose_estimate = new_particles[max_idx].pose;
  prev_odom = curr_odom;
  reference_scan = curr_scan;
  updateMapOdomTransform();
  
  ROS_INFO("======= END OF UPDATE =======\n");
}

/* updates the map-odom transform based on the current odometry reading and map pose estimate */
void ParticleFilter::updateMapOdomTransform() {
  // robot-odom transform
  tf::Transform robot_odom;
  robot_odom.setOrigin(tf::Vector3(curr_odom.x, curr_odom.y, 0));
  robot_odom.setRotation(tf::createQuaternionFromYaw(curr_odom.theta));

  // robot-map transform
  tf::Transform robot_map;
  robot_map.setOrigin(tf::Vector3(pose_estimate.x, pose_estimate.y, 0));
  robot_map.setRotation(tf::createQuaternionFromYaw(pose_estimate.theta));
  
  // update map-odom
  map_odom_transform = robot_odom * robot_map.inverse();
}

/* returns true if the robot has moved at least the min amount needed for update */
bool ParticleFilter::robotHasMoved() {
  double dx = curr_odom.x - prev_odom.x;
  double dy = curr_odom.y - prev_odom.y;
  double rotation = headingChange(curr_odom.theta, prev_odom.theta);
  double distance = sqrt(dx * dx + dy * dy);
  return distance >= min_distance || rotation >= min_rotation;
}

/* called when a new laser scan is received */
void ParticleFilter::processLaserReading(const sensor_msgs::LaserScan::ConstPtr &msg) {
  // nothing to do
  if (particles.size() == 0) return;
  
  // check if the robot has moved enough to update
  curr_odom = lookupRobotOdomPose();
  curr_scan = *msg;
  if (reference_scan.ranges.size() == 0 ) {reference_scan = curr_scan;}
  if (!robotHasMoved()) {
    return;
  }
  
  ROS_INFO("== laser scan & robot moved! ==");
  sensor_model->updateScan(msg);
  //motion_model->setScans(reference_scan, curr_scan);
  performUpdate();
}

/* called when a new pose estimate is set in rviz */
void ParticleFilter::processPoseEstimate(const geometry_msgs::PoseWithCovarianceStamped::ConstPtr &msg) {
  curr_odom = lookupRobotOdomPose();
  
  // get pose estimate
  RobotPose pose;
  pose.x = msg->pose.pose.position.x;
  pose.y = msg->pose.pose.position.y;
  tf::Quaternion rotation;
  tf::quaternionMsgToTF(msg->pose.pose.orientation, rotation);
  pose.theta = tf::getYaw(rotation);
  
  // initialize cloud
  initGaussian(initialpose_num_particles, pose.x, initialpose_x_var, pose.y, initialpose_y_var, pose.theta, initialpose_theta_var);
  
  pose_estimate = pose;
  prev_odom = curr_odom;
  updateMapOdomTransform();
}

/* returns the current robot pose, relative to odom */
RobotPose ParticleFilter::lookupRobotOdomPose() {
  // setup point message
	geometry_msgs::PoseStamped pose_stamped;
	pose_stamped.header.frame_id = robot_frame;
	pose_stamped.header.stamp = ros::Time();

  tf::Pose pose;
	pose.setOrigin(tf::Vector3(0, 0, 0));
	pose.setRotation(tf::createQuaternionFromYaw(0));
	tf::poseTFToMsg(pose, pose_stamped.pose);
  
  geometry_msgs::PoseStamped result_msg;
  listener->transformPose(odom_frame, pose_stamped, result_msg);
  
  tf::Stamped<tf::Pose> result_pose;
	tf::poseStampedMsgToTF(result_msg, result_pose);
  
  RobotPose robot_pose;
  robot_pose.x = result_pose.getOrigin().getX();
  robot_pose.y = result_pose.getOrigin().getY();
  robot_pose.theta = tf::getYaw(result_pose.getRotation());
  return robot_pose;
}

/* Visualizes the particle cloud */
void ParticleFilter::visualizeParticles(ros::Publisher &pub) {
  geometry_msgs::PoseArray cloud;
  cloud.header.frame_id = map_frame;
  cloud.header.stamp = ros::Time();
  
  geometry_msgs::Pose pose;
  for (size_t i = 0; i < particles.size(); i++) {
    pose.position.x = particles[i].pose.x;
    pose.position.y = particles[i].pose.y;
    pose.position.z = 0;
    tf::quaternionTFToMsg(tf::createQuaternionFromYaw(particles[i].pose.theta), pose.orientation);
    cloud.poses.push_back(pose);
  }
  pub.publish(cloud);
}
