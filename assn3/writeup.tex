\documentclass{report}
\usepackage[latin1]{inputenc}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{cancel}
\usepackage{units}
\usepackage[top=3.5cm,left=3cm,right=3cm,bottom=2cm,foot=1cm]{geometry}
\renewcommand{\thesection}{\arabic{section}}

\title{\textbf{CS 225b} \\ Project 3 Summary}
\author{\textbf{Evan Rosen, Karl Uhlig, Joseph Marrama}}
\date{November 17, 2011}

\begin{document}
\maketitle

\subsection*{Particle Filtering}

Our implementation of the particle filter relies primarily on three modular components: a motion model, which is responsible for sampling new poses using odometry information; a sensor model, which uses laser scan readings to compute the likelihood of poses; and the particle filter itself, which manages the particle set and evolves the system over time.

In the resampling stage, we use KLD sampling to bound the uncertainty of the pose distribution and determine the necessary number of particles. We use a relatively small histogram bin size and enforce a minimum/maximum number of particles to ensure that the distribution is well represented (and the update computationally feasible) at all points in time. We also insert random robot poses into the set with some fixed probability, as we found this to be useful in re-localizing the robot when the robot is moved or the particle set is poorly localized.

In terms of parameters to the various error models, we found that the algorithm works best when there is a moderate amount of random noise in the motion and sensor models. When the variances are set too low, small errors are penalized harshly and the same few particles tend to be resampled again and again in the update step. When the error is too large, the algorithm does a poor job of differentiating between good and bad poses. In this case, the distribution begins to spread out and the robot remains poorly localized.

One technical issue that we faced in the sensor model was the accumulation of extremely small probabilities when using all (or even many) of the laser scan points. When we used every laser reading the weight discrepancy between good and bad poses was extreme, and so the same few particles would be resampled every time (similar to the issue of have too little error built into the motion/sensor models). Although it is not probabilistically sound, we found that the algorithm performed best when some of the scan probabilities (in log space) were averaged to give a result that is the geometric mean of the individual scan probabilities.

As a simple heuristic for the robot kidnapping problem, we measured the weight ratio between the particles with highest and lowest weight at each update step. Since we always have some number of random poses in the particle set, the minimum weight is generally pretty consistent (i.e. consistently bad). The maximum weight, on the other hand, is much higher when the robot is well localized. Thus, when the ratio drops below some threshold, we assume that the robot is poorly localized and re-initialize the particle set to a uniform distribution over the entire map. Despite the simplicity of this approach, this technique is surprisingly effective at detecting kidnapped robots and/or misplaced particle sets.

\subsection*{Scan Matching}

We implemented scan matching in a subclass of our original motion model (found in MotionModelScanMatcher.cpp) so that it could be seamlessly integrated with our particle filter. When we update our scan, we use our old reference scan to construct a occupancy grid, which we then feed into our sensor model along with our new scan so that we may use it to calculate $P(z_i|x_i, z_{i-1})$ for various poses $x_i$. We do this in an efficient manner by processing all poses with the same angle $\Theta$ at the same time, much like described in class. We generally follow the methods outlined in the Olson paper for computing the mean and covariance of our new motion model.

We differ from the Olson model in that we first included our old general motion model (which computes $P(x_i|x_{i-1}, u)$) in our computation of our new motion model. We initially included this in our computation of $P(x_i|x_{i-1}, u, z, m)$, but removing our old motion model actually has no noticable impact on the performance of our scan matching implementation. Also, we found that scan matching is really slow, so we decreased the grid size over which we calculate the probability of possible poses. We compute the probabilities of poses for a grid of $x_dim*y_dim*theta_dim$ points, where theta is uniformily distributed between $0$ and $2\pi$, and the x-y grids are centered on the mean of $P(x_i|x_{i-1}, u)$ given by our old motion model. We usually set $theta_dim$ to 100, and $x_dim$ and $y_dim$ equal to 20. The x and y scale was 4cm (i.e., moving point to point in the x-y plane corresponds to moving 4cm). Even at this size, calculating the probability for all these poses still took too long to stay 'caught up' to the robot, especially if it was moving fast. We didn't want to diminish the granularity nor the range of our scan matching, so we left it at that size. 

\end{document}
